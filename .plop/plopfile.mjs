import { moduleGenerator } from "./module-generator/index.mjs";

export default function generate(plop) {
  plop.setGenerator("module", moduleGenerator);
}
