import path from "path";
import { camelCase, kebabCase } from "../utils.mjs";

const LAZYLOAD_ENTRY = "// PLOP LAZYLOAD ENTRYPOINT (Do not remove)";
const ROUTER_ENTRY = "{/* PLOP ROUTER ENTRYPOINT (Do not remove) */}";
const IMPORT_ENTRY = "} from \"./models";

export const updateAppRouter = () => {
    return ({
      type: "modify",
      path: path.join(
        process.cwd(),
        `/src/AppRouter.tsx`
      ),
      transform: (content, data) => {
        const NamePascal = data.module_name 
        const nameCamel = camelCase(NamePascal);
        const NAME_UPPER = NamePascal.toUpperCase()
        const nameKebab = kebabCase(NamePascal)
  
        //Prevent multiple adds
        const existingIndex = content.indexOf(`import("~/modules/${nameKebab}/${NamePascal}Index")`);
        if(existingIndex > 0){
          return content
        }
        
        let updatedContent = content.replace(LAZYLOAD_ENTRY, `
        // ${NamePascal} Modules
        const ${NamePascal}Index = React.lazy(() => import("~/modules/${nameKebab}/${NamePascal}Index"));
        const ${NamePascal}Detail = React.lazy(() => import("~/modules/${nameKebab}/${NamePascal}Detail"));\n
        ${LAZYLOAD_ENTRY}`);

        updatedContent = updatedContent.replace(ROUTER_ENTRY, `
        {/* ${NAME_UPPER} MODULES */}
          <Route path={routes.${nameCamel}Index.path} element={<${NamePascal}Index />} />
          <Route path={routes.${nameCamel}Detail.path} element={<${NamePascal}Detail />} />
          <Route
            path={routes.${nameCamel}Import.path}
            element={<Importer model={${NamePascal}} />}
          />\n
        ${ROUTER_ENTRY}`);

        updatedContent = updatedContent.replace(IMPORT_ENTRY, `, ${NamePascal} ${IMPORT_ENTRY}`)

        return updatedContent;
      }
    });
  };