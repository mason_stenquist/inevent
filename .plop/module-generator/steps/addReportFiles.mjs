import path from "path";

export const addReportFiles = () => ({
    type: "addMany",
    destination: path.join(
        process.cwd(),
        `/src/reports/{{kebabCase module_name}}`
    ),
    base: "module-generator/plop-templates/reports/",
    templateFiles: ["module-generator/plop-templates/reports/**/*.hbs"],
    force: true, //Overwrite during testing
});
