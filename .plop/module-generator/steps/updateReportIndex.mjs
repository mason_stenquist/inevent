import path from "path";
import { getAlternateCasingsFromPascal } from "../utils.mjs";

const IMPORT_ENTRY = "// PLOP IMPORT ENTRY";
const CATEGORY_ENTRY = "category: ";
const EXPORT_ENTRY = "export const reports: Report[] = [";

export const updateReportIndex = () => {
    return ({
      type: "modify",
      path: path.join(
        process.cwd(),
        `/src/reports/index.ts`
      ),
      transform: (content, data) => {
        const {pascal, camel} = getAlternateCasingsFromPascal(data.module_name);
  
        //Prevent multiple adds
        const existingIndex = content.indexOf(`import ${pascal}Reports from "./${camel}`);
        if(existingIndex > 0){
          return content
        }
        
        // Update Imports
        let updatedContent = content.replace(IMPORT_ENTRY, `
        import ${pascal}Reports from "./${camel}";
        ${IMPORT_ENTRY}`);

        // Update categories
        updatedContent = updatedContent.replace(CATEGORY_ENTRY, `${CATEGORY_ENTRY} "${pascal}" | `);
        
        // Update exports
        updatedContent = updatedContent.replace(EXPORT_ENTRY, `${EXPORT_ENTRY}...${pascal}Reports,`);

        return updatedContent;
      }
    });
  };