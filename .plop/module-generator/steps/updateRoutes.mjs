import path from "path";
import { camelCase, getModulePluralName } from "../utils.mjs";

const ROUTES_ENTRY = "// PLOP ROUTES ENTRYPOINT (Do not remove)";

export const updateRoutes = () => {
    return ({
      type: "modify",
      path: path.join(
        process.cwd(),
        `/src/hooks/useRoutes.ts`
      ),
      transform: (content, data) => {
        const formattedName = camelCase(data.module_name);
        const formattedPluralName = camelCase(getModulePluralName(data));
  
        //Prevent multiple adds
        const existingIndex = content.indexOf(`/event/${formattedPluralName}`);
        if(existingIndex > 0){
          return content
        }
        
        const updatedContent = content.replace(ROUTES_ENTRY, `
        // ${data.module_name} Routes
        ${formattedName}Index: route(t\`${formattedPluralName}\`, "/event/${formattedPluralName}"),
        ${formattedName}Import: route(t\`${formattedName}Import\`, "/event/${formattedPluralName}/import"),
        ${formattedName}Detail: route<{ id: string }>(t\`${formattedName}Detail\`, "/event/${formattedPluralName}/:id"),\n
        ${ROUTES_ENTRY}`);
        return updatedContent;
      }
    });
  };