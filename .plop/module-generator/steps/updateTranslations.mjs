import path from "path";
import { camelCase, getAlternateCasingsFromPascal, getModulePluralName } from "../utils.mjs";

const IMPORT_ENTRY = "// PLOP IMPORT ENTRYPOINT (Do not remove)";
const VOCAB_ENTRY = "// PLOP VOCAB ENTRYPOINT (Do not remove)";

export const updateTranslations = () => {
    return ({
      type: "modify",
      path: path.join(
        process.cwd(),
        `/src/hooks/useI18n.ts`
      ),
      transform: (content, data) => {
        const {camel} = getAlternateCasingsFromPascal(data.module_name)
  
        //Prevent multiple adds
        const existingIndex = content.indexOf(`I18n.putVocabularies(${camel}Dict)`);
        if(existingIndex > 0){
          return content
        }
        
        let updatedContent = content.replace(IMPORT_ENTRY, `
        import ${camel}Dict from "~/modules/${camel}/translations";
        ${IMPORT_ENTRY}`);

        updatedContent = updatedContent.replace(VOCAB_ENTRY, `
        I18n.putVocabularies(${camel}Dict);
        ${VOCAB_ENTRY}`);

        return updatedContent;
      }
    });
  };