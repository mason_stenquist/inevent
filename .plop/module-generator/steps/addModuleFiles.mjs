import path from "path";

export const addModuleFiles = () => ({
    type: "addMany",
    destination: path.join(
        process.cwd(),
        `/src/modules/{{kebabCase module_name}}`
    ),
    base: "module-generator/plop-templates/module/",
    templateFiles: ["module-generator/plop-templates/module/**/*.hbs"],
    force: true, //Overwrite during testing
});
