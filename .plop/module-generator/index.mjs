import { addModuleFiles } from "./steps/addModuleFiles.mjs";
import { addReportFiles } from "./steps/addReportFiles.mjs";
import { updateAppRouter } from "./steps/updateAppRouter.mjs";
import { updateReportIndex } from "./steps/updateReportIndex.mjs";
import { updateRoutes } from "./steps/updateRoutes.mjs";
import { updateTranslations } from "./steps/updateTranslations.mjs";
import { getModuleFieldNames } from "./utils.mjs";
import { hasNumbers, hasSpaces, isNotPascalCase } from "./validators.mjs";

export const moduleGenerator = {
  description: "Creates new module which includes pages/helpers/routes etc",
  prompts: [
    {
      name: "module_name",
      type: "input",
      message:
        "What is the amplify model name? Should be singular and PascalCase",
      validate: (value) => {
        if (hasSpaces(value)) return "value cannot include spaces.";
        if (isNotPascalCase(value)) return "value must be in PascalCase";
        if (hasNumbers(value)) return "value must not have numbers";
        return true;
      },
    },
    {
      name: "field_names",
      type: "checkbox",
      choices: (data) => getModuleFieldNames(data),
      message:
        "What fields do you want to see in the list view? Select them in the order you want them to appear.",
    },
    {
      name: "confirm",
      type: "confirm",
      message: (
        data
      ) => `You are about to create a new module (${data.module_name}). This will:
      - Create a new module directory folder
      - Create a pages for list/detail
      - Create importer page
      - Add new routes
      
      Proceed?`,
    },
  ],
  actions: (data) => {
    console.log(data);
    if (!data.confirm) {
      return ["Module generation cancelled"];
    }

    return [
      addModuleFiles(),
      addReportFiles(),
      updateRoutes(),
      updateAppRouter(),
      updateReportIndex(),
      updateTranslations()
    ];
  },
};
