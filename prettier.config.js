module.exports = {
  tailwindConfig: "./tailwind.config.js",
  importOrder: [
    "^(react|@chakra-ui|@heroicons|aws-amplify|jotai|formik)(.*)$",
    "^aws-amplify/(.*)$",
    "^~/models/(.*)$",
    "^~/modules/(.*)$",
    "^~/components/(.*)$",
    "^~/hooks/(.*)$",
    ".",
  ],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,
};
