const path = require("path");
const { startDevServer } = require("@cypress/vite-dev-server");

const awsConfig = require(path.join(__dirname, "../../src/aws-exports.js"));

module.exports = (on, config) => {
  console.log(awsConfig);
  config.env.awsConfig = awsConfig.default;

  on("dev-server:start", (options) => {
    return startDevServer({
      options,
      viteConfig: {
        configFile: path.resolve(__dirname, "../../vite.config.ts"),
      },
    });
  });

  return config;
};
