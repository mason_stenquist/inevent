describe("GuestIndex", () => {
  beforeEach(() => {
    console.log(Cypress.env("AWS_COGNITO_USERNAME"));
    cy.loginByCognitoApi(
      Cypress.env("AWS_COGNITO_USERNAME"),
      Cypress.env("AWS_COGNITO_PASSWORD")
    );
    cy.visit("/");
  });

  it("displays test link", () => {
    cy.get("a").contains("Test");
  });

  it("requires first name", () => {
    cy.visit("/event/guests");
    cy.get(".hidden > .chakra-button").click();
    cy.get("#nameLast").type("Hello");
    cy.get("#chakra-modal--body-20 > form").submit();
    cy.get(".text-input-error").contains("First Name is a required field");
  });

  it("requires last name", () => {
    cy.visit("/event/guests");
    cy.get(".hidden > .chakra-button").click();
    cy.get("#nameFirst").type("Hello");
    cy.get("#chakra-modal--body-20 > form").submit();
    cy.get(".text-input-error").contains("Last Name is a required field");
  });
});
