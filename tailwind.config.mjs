import { breakpoints } from "./src/theme/breakpoints";

function withOpacity(variableName) {
  return ({ opacityValue }) =>
    `rgba(var(${variableName}), ${opacityValue ?? 1})`;
}

export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  important: true,
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        page: withOpacity("--color-page"),
        card: withOpacity("--color-card"),
        modal: withOpacity("--color-modal"),
        accent: withOpacity("--color-accent"),
        "accent-hover": withOpacity("--color-accent-hover"),
        warning: withOpacity("--color-warning"),
        danger: withOpacity("--color-danger"),
        "danger-hover": withOpacity("--color-danger-hover"),
        "default-text": withOpacity("--color-default-text"),
        "muted-text": withOpacity("--color-muted-text"),
        "inverted-text": withOpacity("--color-inverted-text"),
        "input-border": withOpacity("--color-input-border"),
        "input-background": withOpacity("--color-input-background"),
        "input-error": withOpacity("--color-input-error"),
        button: {
          default: withOpacity("--color-button-default"),
          "default-hover": withOpacity("--color-button-default-hover"),
        },
        table: {
          row: {
            hover: withOpacity("--color-table-row-hover"),
          },
        },
        "border-default": withOpacity("--color-border-default"),
      },
    },
    screens: breakpoints,
  },
  plugins: [require("@tailwindcss/forms")],
};
