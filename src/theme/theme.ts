import { extendTheme } from "@chakra-ui/react";
import { chakraBreakpoints } from "./breakpoints";

export const theme = extendTheme({
  initialColorMode: "dark",
  useSystemColorMode: false,
  breakpoints: chakraBreakpoints,
});
