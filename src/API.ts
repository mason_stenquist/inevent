/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateActivityInput = {
  id?: string | null,
  group: string,
  name: string,
  category?: string | null,
  description?: string | null,
  date?: string | null,
  start?: string | null,
  end?: string | null,
  _version?: number | null,
};

export type ModelActivityConditionInput = {
  group?: ModelStringInput | null,
  name?: ModelStringInput | null,
  category?: ModelStringInput | null,
  description?: ModelStringInput | null,
  date?: ModelStringInput | null,
  start?: ModelStringInput | null,
  end?: ModelStringInput | null,
  and?: Array< ModelActivityConditionInput | null > | null,
  or?: Array< ModelActivityConditionInput | null > | null,
  not?: ModelActivityConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type Activity = {
  __typename: "Activity",
  id: string,
  group: string,
  name: string,
  category?: string | null,
  description?: string | null,
  date?: string | null,
  start?: string | null,
  end?: string | null,
  agenda?: ModelAgendaConnection | null,
  createdAt: string,
  updatedAt: string,
  _version: number,
  _deleted?: boolean | null,
  _lastChangedAt: number,
  owner?: string | null,
};

export type ModelAgendaConnection = {
  __typename: "ModelAgendaConnection",
  items:  Array<Agenda | null >,
  nextToken?: string | null,
  startedAt?: number | null,
};

export type Agenda = {
  __typename: "Agenda",
  id: string,
  group: string,
  guestID?: string | null,
  guest?: Guest | null,
  activityID?: string | null,
  activity?: Activity | null,
  createdAt: string,
  updatedAt: string,
  _version: number,
  _deleted?: boolean | null,
  _lastChangedAt: number,
  owner?: string | null,
};

export type Guest = {
  __typename: "Guest",
  id: string,
  group: string,
  nameFirst: string,
  _nameFirst?: string | null,
  nameLast: string,
  _nameLast?: string | null,
  company?: string | null,
  _company?: string | null,
  position?: string | null,
  _position?: string | null,
  dateOfBirth?: string | null,
  _keyPhoto?: string | null,
  agenda?: ModelAgendaConnection | null,
  createdAt: string,
  updatedAt: string,
  _version: number,
  _deleted?: boolean | null,
  _lastChangedAt: number,
  owner?: string | null,
};

export type UpdateActivityInput = {
  id: string,
  group?: string | null,
  name?: string | null,
  category?: string | null,
  description?: string | null,
  date?: string | null,
  start?: string | null,
  end?: string | null,
  _version?: number | null,
};

export type DeleteActivityInput = {
  id: string,
  _version?: number | null,
};

export type CreateAgendaInput = {
  id?: string | null,
  group: string,
  guestID?: string | null,
  activityID?: string | null,
  _version?: number | null,
};

export type ModelAgendaConditionInput = {
  group?: ModelStringInput | null,
  guestID?: ModelIDInput | null,
  activityID?: ModelIDInput | null,
  and?: Array< ModelAgendaConditionInput | null > | null,
  or?: Array< ModelAgendaConditionInput | null > | null,
  not?: ModelAgendaConditionInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type UpdateAgendaInput = {
  id: string,
  group?: string | null,
  guestID?: string | null,
  activityID?: string | null,
  _version?: number | null,
};

export type DeleteAgendaInput = {
  id: string,
  _version?: number | null,
};

export type CreateEventInput = {
  id?: string | null,
  group: string,
  eventName: string,
  _eventName?: string | null,
  startDate?: string | null,
  endDate?: string | null,
  _logo?: string | null,
  _version?: number | null,
};

export type ModelEventConditionInput = {
  group?: ModelStringInput | null,
  eventName?: ModelStringInput | null,
  _eventName?: ModelStringInput | null,
  startDate?: ModelStringInput | null,
  endDate?: ModelStringInput | null,
  _logo?: ModelStringInput | null,
  and?: Array< ModelEventConditionInput | null > | null,
  or?: Array< ModelEventConditionInput | null > | null,
  not?: ModelEventConditionInput | null,
};

export type Event = {
  __typename: "Event",
  id: string,
  group: string,
  eventName: string,
  _eventName?: string | null,
  startDate?: string | null,
  endDate?: string | null,
  _logo?: string | null,
  createdAt: string,
  updatedAt: string,
  _version: number,
  _deleted?: boolean | null,
  _lastChangedAt: number,
  owner?: string | null,
};

export type UpdateEventInput = {
  id: string,
  group?: string | null,
  eventName?: string | null,
  _eventName?: string | null,
  startDate?: string | null,
  endDate?: string | null,
  _logo?: string | null,
  _version?: number | null,
};

export type DeleteEventInput = {
  id: string,
  _version?: number | null,
};

export type CreateFilterInput = {
  id?: string | null,
  name: string,
  modelName: string,
  filter: string,
  pinned?: boolean | null,
  _version?: number | null,
};

export type ModelFilterConditionInput = {
  name?: ModelStringInput | null,
  modelName?: ModelStringInput | null,
  filter?: ModelStringInput | null,
  pinned?: ModelBooleanInput | null,
  and?: Array< ModelFilterConditionInput | null > | null,
  or?: Array< ModelFilterConditionInput | null > | null,
  not?: ModelFilterConditionInput | null,
};

export type ModelBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type Filter = {
  __typename: "Filter",
  id: string,
  name: string,
  modelName: string,
  filter: string,
  pinned?: boolean | null,
  createdAt: string,
  updatedAt: string,
  _version: number,
  _deleted?: boolean | null,
  _lastChangedAt: number,
  owner?: string | null,
};

export type UpdateFilterInput = {
  id: string,
  name?: string | null,
  modelName?: string | null,
  filter?: string | null,
  pinned?: boolean | null,
  _version?: number | null,
};

export type DeleteFilterInput = {
  id: string,
  _version?: number | null,
};

export type CreateGuestInput = {
  id?: string | null,
  group: string,
  nameFirst: string,
  _nameFirst?: string | null,
  nameLast: string,
  _nameLast?: string | null,
  company?: string | null,
  _company?: string | null,
  position?: string | null,
  _position?: string | null,
  dateOfBirth?: string | null,
  _keyPhoto?: string | null,
  _version?: number | null,
};

export type ModelGuestConditionInput = {
  group?: ModelStringInput | null,
  nameFirst?: ModelStringInput | null,
  _nameFirst?: ModelStringInput | null,
  nameLast?: ModelStringInput | null,
  _nameLast?: ModelStringInput | null,
  company?: ModelStringInput | null,
  _company?: ModelStringInput | null,
  position?: ModelStringInput | null,
  _position?: ModelStringInput | null,
  dateOfBirth?: ModelStringInput | null,
  _keyPhoto?: ModelStringInput | null,
  and?: Array< ModelGuestConditionInput | null > | null,
  or?: Array< ModelGuestConditionInput | null > | null,
  not?: ModelGuestConditionInput | null,
};

export type UpdateGuestInput = {
  id: string,
  group?: string | null,
  nameFirst?: string | null,
  _nameFirst?: string | null,
  nameLast?: string | null,
  _nameLast?: string | null,
  company?: string | null,
  _company?: string | null,
  position?: string | null,
  _position?: string | null,
  dateOfBirth?: string | null,
  _keyPhoto?: string | null,
  _version?: number | null,
};

export type DeleteGuestInput = {
  id: string,
  _version?: number | null,
};

export type ModelActivityFilterInput = {
  id?: ModelIDInput | null,
  group?: ModelStringInput | null,
  name?: ModelStringInput | null,
  category?: ModelStringInput | null,
  description?: ModelStringInput | null,
  date?: ModelStringInput | null,
  start?: ModelStringInput | null,
  end?: ModelStringInput | null,
  and?: Array< ModelActivityFilterInput | null > | null,
  or?: Array< ModelActivityFilterInput | null > | null,
  not?: ModelActivityFilterInput | null,
};

export type ModelActivityConnection = {
  __typename: "ModelActivityConnection",
  items:  Array<Activity | null >,
  nextToken?: string | null,
  startedAt?: number | null,
};

export type ModelAgendaFilterInput = {
  id?: ModelIDInput | null,
  group?: ModelStringInput | null,
  guestID?: ModelIDInput | null,
  activityID?: ModelIDInput | null,
  and?: Array< ModelAgendaFilterInput | null > | null,
  or?: Array< ModelAgendaFilterInput | null > | null,
  not?: ModelAgendaFilterInput | null,
};

export type ModelEventFilterInput = {
  id?: ModelIDInput | null,
  group?: ModelStringInput | null,
  eventName?: ModelStringInput | null,
  _eventName?: ModelStringInput | null,
  startDate?: ModelStringInput | null,
  endDate?: ModelStringInput | null,
  _logo?: ModelStringInput | null,
  and?: Array< ModelEventFilterInput | null > | null,
  or?: Array< ModelEventFilterInput | null > | null,
  not?: ModelEventFilterInput | null,
};

export type ModelEventConnection = {
  __typename: "ModelEventConnection",
  items:  Array<Event | null >,
  nextToken?: string | null,
  startedAt?: number | null,
};

export type ModelFilterFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  modelName?: ModelStringInput | null,
  filter?: ModelStringInput | null,
  pinned?: ModelBooleanInput | null,
  and?: Array< ModelFilterFilterInput | null > | null,
  or?: Array< ModelFilterFilterInput | null > | null,
  not?: ModelFilterFilterInput | null,
};

export type ModelFilterConnection = {
  __typename: "ModelFilterConnection",
  items:  Array<Filter | null >,
  nextToken?: string | null,
  startedAt?: number | null,
};

export type ModelGuestFilterInput = {
  id?: ModelIDInput | null,
  group?: ModelStringInput | null,
  nameFirst?: ModelStringInput | null,
  _nameFirst?: ModelStringInput | null,
  nameLast?: ModelStringInput | null,
  _nameLast?: ModelStringInput | null,
  company?: ModelStringInput | null,
  _company?: ModelStringInput | null,
  position?: ModelStringInput | null,
  _position?: ModelStringInput | null,
  dateOfBirth?: ModelStringInput | null,
  _keyPhoto?: ModelStringInput | null,
  and?: Array< ModelGuestFilterInput | null > | null,
  or?: Array< ModelGuestFilterInput | null > | null,
  not?: ModelGuestFilterInput | null,
};

export type ModelGuestConnection = {
  __typename: "ModelGuestConnection",
  items:  Array<Guest | null >,
  nextToken?: string | null,
  startedAt?: number | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type CreateActivityMutationVariables = {
  input: CreateActivityInput,
  condition?: ModelActivityConditionInput | null,
};

export type CreateActivityMutation = {
  createActivity?:  {
    __typename: "Activity",
    id: string,
    group: string,
    name: string,
    category?: string | null,
    description?: string | null,
    date?: string | null,
    start?: string | null,
    end?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type UpdateActivityMutationVariables = {
  input: UpdateActivityInput,
  condition?: ModelActivityConditionInput | null,
};

export type UpdateActivityMutation = {
  updateActivity?:  {
    __typename: "Activity",
    id: string,
    group: string,
    name: string,
    category?: string | null,
    description?: string | null,
    date?: string | null,
    start?: string | null,
    end?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type DeleteActivityMutationVariables = {
  input: DeleteActivityInput,
  condition?: ModelActivityConditionInput | null,
};

export type DeleteActivityMutation = {
  deleteActivity?:  {
    __typename: "Activity",
    id: string,
    group: string,
    name: string,
    category?: string | null,
    description?: string | null,
    date?: string | null,
    start?: string | null,
    end?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type CreateAgendaMutationVariables = {
  input: CreateAgendaInput,
  condition?: ModelAgendaConditionInput | null,
};

export type CreateAgendaMutation = {
  createAgenda?:  {
    __typename: "Agenda",
    id: string,
    group: string,
    guestID?: string | null,
    guest?:  {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    activityID?: string | null,
    activity?:  {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type UpdateAgendaMutationVariables = {
  input: UpdateAgendaInput,
  condition?: ModelAgendaConditionInput | null,
};

export type UpdateAgendaMutation = {
  updateAgenda?:  {
    __typename: "Agenda",
    id: string,
    group: string,
    guestID?: string | null,
    guest?:  {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    activityID?: string | null,
    activity?:  {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type DeleteAgendaMutationVariables = {
  input: DeleteAgendaInput,
  condition?: ModelAgendaConditionInput | null,
};

export type DeleteAgendaMutation = {
  deleteAgenda?:  {
    __typename: "Agenda",
    id: string,
    group: string,
    guestID?: string | null,
    guest?:  {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    activityID?: string | null,
    activity?:  {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type CreateEventMutationVariables = {
  input: CreateEventInput,
  condition?: ModelEventConditionInput | null,
};

export type CreateEventMutation = {
  createEvent?:  {
    __typename: "Event",
    id: string,
    group: string,
    eventName: string,
    _eventName?: string | null,
    startDate?: string | null,
    endDate?: string | null,
    _logo?: string | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type UpdateEventMutationVariables = {
  input: UpdateEventInput,
  condition?: ModelEventConditionInput | null,
};

export type UpdateEventMutation = {
  updateEvent?:  {
    __typename: "Event",
    id: string,
    group: string,
    eventName: string,
    _eventName?: string | null,
    startDate?: string | null,
    endDate?: string | null,
    _logo?: string | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type DeleteEventMutationVariables = {
  input: DeleteEventInput,
  condition?: ModelEventConditionInput | null,
};

export type DeleteEventMutation = {
  deleteEvent?:  {
    __typename: "Event",
    id: string,
    group: string,
    eventName: string,
    _eventName?: string | null,
    startDate?: string | null,
    endDate?: string | null,
    _logo?: string | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type CreateFilterMutationVariables = {
  input: CreateFilterInput,
  condition?: ModelFilterConditionInput | null,
};

export type CreateFilterMutation = {
  createFilter?:  {
    __typename: "Filter",
    id: string,
    name: string,
    modelName: string,
    filter: string,
    pinned?: boolean | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type UpdateFilterMutationVariables = {
  input: UpdateFilterInput,
  condition?: ModelFilterConditionInput | null,
};

export type UpdateFilterMutation = {
  updateFilter?:  {
    __typename: "Filter",
    id: string,
    name: string,
    modelName: string,
    filter: string,
    pinned?: boolean | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type DeleteFilterMutationVariables = {
  input: DeleteFilterInput,
  condition?: ModelFilterConditionInput | null,
};

export type DeleteFilterMutation = {
  deleteFilter?:  {
    __typename: "Filter",
    id: string,
    name: string,
    modelName: string,
    filter: string,
    pinned?: boolean | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type CreateGuestMutationVariables = {
  input: CreateGuestInput,
  condition?: ModelGuestConditionInput | null,
};

export type CreateGuestMutation = {
  createGuest?:  {
    __typename: "Guest",
    id: string,
    group: string,
    nameFirst: string,
    _nameFirst?: string | null,
    nameLast: string,
    _nameLast?: string | null,
    company?: string | null,
    _company?: string | null,
    position?: string | null,
    _position?: string | null,
    dateOfBirth?: string | null,
    _keyPhoto?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type UpdateGuestMutationVariables = {
  input: UpdateGuestInput,
  condition?: ModelGuestConditionInput | null,
};

export type UpdateGuestMutation = {
  updateGuest?:  {
    __typename: "Guest",
    id: string,
    group: string,
    nameFirst: string,
    _nameFirst?: string | null,
    nameLast: string,
    _nameLast?: string | null,
    company?: string | null,
    _company?: string | null,
    position?: string | null,
    _position?: string | null,
    dateOfBirth?: string | null,
    _keyPhoto?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type DeleteGuestMutationVariables = {
  input: DeleteGuestInput,
  condition?: ModelGuestConditionInput | null,
};

export type DeleteGuestMutation = {
  deleteGuest?:  {
    __typename: "Guest",
    id: string,
    group: string,
    nameFirst: string,
    _nameFirst?: string | null,
    nameLast: string,
    _nameLast?: string | null,
    company?: string | null,
    _company?: string | null,
    position?: string | null,
    _position?: string | null,
    dateOfBirth?: string | null,
    _keyPhoto?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type GetActivityQueryVariables = {
  id: string,
};

export type GetActivityQuery = {
  getActivity?:  {
    __typename: "Activity",
    id: string,
    group: string,
    name: string,
    category?: string | null,
    description?: string | null,
    date?: string | null,
    start?: string | null,
    end?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type ListActivitiesQueryVariables = {
  filter?: ModelActivityFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListActivitiesQuery = {
  listActivities?:  {
    __typename: "ModelActivityConnection",
    items:  Array< {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type SyncActivitiesQueryVariables = {
  filter?: ModelActivityFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncActivitiesQuery = {
  syncActivities?:  {
    __typename: "ModelActivityConnection",
    items:  Array< {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type GetAgendaQueryVariables = {
  id: string,
};

export type GetAgendaQuery = {
  getAgenda?:  {
    __typename: "Agenda",
    id: string,
    group: string,
    guestID?: string | null,
    guest?:  {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    activityID?: string | null,
    activity?:  {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type ListAgendaQueryVariables = {
  filter?: ModelAgendaFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListAgendaQuery = {
  listAgenda?:  {
    __typename: "ModelAgendaConnection",
    items:  Array< {
      __typename: "Agenda",
      id: string,
      group: string,
      guestID?: string | null,
      activityID?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type SyncAgendaQueryVariables = {
  filter?: ModelAgendaFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncAgendaQuery = {
  syncAgenda?:  {
    __typename: "ModelAgendaConnection",
    items:  Array< {
      __typename: "Agenda",
      id: string,
      group: string,
      guestID?: string | null,
      activityID?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type GetEventQueryVariables = {
  id: string,
};

export type GetEventQuery = {
  getEvent?:  {
    __typename: "Event",
    id: string,
    group: string,
    eventName: string,
    _eventName?: string | null,
    startDate?: string | null,
    endDate?: string | null,
    _logo?: string | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type ListEventsQueryVariables = {
  filter?: ModelEventFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListEventsQuery = {
  listEvents?:  {
    __typename: "ModelEventConnection",
    items:  Array< {
      __typename: "Event",
      id: string,
      group: string,
      eventName: string,
      _eventName?: string | null,
      startDate?: string | null,
      endDate?: string | null,
      _logo?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type SyncEventsQueryVariables = {
  filter?: ModelEventFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncEventsQuery = {
  syncEvents?:  {
    __typename: "ModelEventConnection",
    items:  Array< {
      __typename: "Event",
      id: string,
      group: string,
      eventName: string,
      _eventName?: string | null,
      startDate?: string | null,
      endDate?: string | null,
      _logo?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type GetFilterQueryVariables = {
  id: string,
};

export type GetFilterQuery = {
  getFilter?:  {
    __typename: "Filter",
    id: string,
    name: string,
    modelName: string,
    filter: string,
    pinned?: boolean | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type ListFiltersQueryVariables = {
  filter?: ModelFilterFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListFiltersQuery = {
  listFilters?:  {
    __typename: "ModelFilterConnection",
    items:  Array< {
      __typename: "Filter",
      id: string,
      name: string,
      modelName: string,
      filter: string,
      pinned?: boolean | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type SyncFiltersQueryVariables = {
  filter?: ModelFilterFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncFiltersQuery = {
  syncFilters?:  {
    __typename: "ModelFilterConnection",
    items:  Array< {
      __typename: "Filter",
      id: string,
      name: string,
      modelName: string,
      filter: string,
      pinned?: boolean | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type GetGuestQueryVariables = {
  id: string,
};

export type GetGuestQuery = {
  getGuest?:  {
    __typename: "Guest",
    id: string,
    group: string,
    nameFirst: string,
    _nameFirst?: string | null,
    nameLast: string,
    _nameLast?: string | null,
    company?: string | null,
    _company?: string | null,
    position?: string | null,
    _position?: string | null,
    dateOfBirth?: string | null,
    _keyPhoto?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type ListGuestsQueryVariables = {
  filter?: ModelGuestFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListGuestsQuery = {
  listGuests?:  {
    __typename: "ModelGuestConnection",
    items:  Array< {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type SyncGuestsQueryVariables = {
  filter?: ModelGuestFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncGuestsQuery = {
  syncGuests?:  {
    __typename: "ModelGuestConnection",
    items:  Array< {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type AgendaByGroupQueryVariables = {
  group: string,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelAgendaFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type AgendaByGroupQuery = {
  agendaByGroup?:  {
    __typename: "ModelAgendaConnection",
    items:  Array< {
      __typename: "Agenda",
      id: string,
      group: string,
      guestID?: string | null,
      activityID?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
    startedAt?: number | null,
  } | null,
};

export type OnCreateActivitySubscriptionVariables = {
  owner?: string | null,
};

export type OnCreateActivitySubscription = {
  onCreateActivity?:  {
    __typename: "Activity",
    id: string,
    group: string,
    name: string,
    category?: string | null,
    description?: string | null,
    date?: string | null,
    start?: string | null,
    end?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnUpdateActivitySubscriptionVariables = {
  owner?: string | null,
};

export type OnUpdateActivitySubscription = {
  onUpdateActivity?:  {
    __typename: "Activity",
    id: string,
    group: string,
    name: string,
    category?: string | null,
    description?: string | null,
    date?: string | null,
    start?: string | null,
    end?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnDeleteActivitySubscriptionVariables = {
  owner?: string | null,
};

export type OnDeleteActivitySubscription = {
  onDeleteActivity?:  {
    __typename: "Activity",
    id: string,
    group: string,
    name: string,
    category?: string | null,
    description?: string | null,
    date?: string | null,
    start?: string | null,
    end?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnCreateAgendaSubscriptionVariables = {
  owner?: string | null,
};

export type OnCreateAgendaSubscription = {
  onCreateAgenda?:  {
    __typename: "Agenda",
    id: string,
    group: string,
    guestID?: string | null,
    guest?:  {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    activityID?: string | null,
    activity?:  {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnUpdateAgendaSubscriptionVariables = {
  owner?: string | null,
};

export type OnUpdateAgendaSubscription = {
  onUpdateAgenda?:  {
    __typename: "Agenda",
    id: string,
    group: string,
    guestID?: string | null,
    guest?:  {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    activityID?: string | null,
    activity?:  {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnDeleteAgendaSubscriptionVariables = {
  owner?: string | null,
};

export type OnDeleteAgendaSubscription = {
  onDeleteAgenda?:  {
    __typename: "Agenda",
    id: string,
    group: string,
    guestID?: string | null,
    guest?:  {
      __typename: "Guest",
      id: string,
      group: string,
      nameFirst: string,
      _nameFirst?: string | null,
      nameLast: string,
      _nameLast?: string | null,
      company?: string | null,
      _company?: string | null,
      position?: string | null,
      _position?: string | null,
      dateOfBirth?: string | null,
      _keyPhoto?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    activityID?: string | null,
    activity?:  {
      __typename: "Activity",
      id: string,
      group: string,
      name: string,
      category?: string | null,
      description?: string | null,
      date?: string | null,
      start?: string | null,
      end?: string | null,
      createdAt: string,
      updatedAt: string,
      _version: number,
      _deleted?: boolean | null,
      _lastChangedAt: number,
      owner?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnCreateEventSubscriptionVariables = {
  owner?: string | null,
};

export type OnCreateEventSubscription = {
  onCreateEvent?:  {
    __typename: "Event",
    id: string,
    group: string,
    eventName: string,
    _eventName?: string | null,
    startDate?: string | null,
    endDate?: string | null,
    _logo?: string | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnUpdateEventSubscriptionVariables = {
  owner?: string | null,
};

export type OnUpdateEventSubscription = {
  onUpdateEvent?:  {
    __typename: "Event",
    id: string,
    group: string,
    eventName: string,
    _eventName?: string | null,
    startDate?: string | null,
    endDate?: string | null,
    _logo?: string | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnDeleteEventSubscriptionVariables = {
  owner?: string | null,
};

export type OnDeleteEventSubscription = {
  onDeleteEvent?:  {
    __typename: "Event",
    id: string,
    group: string,
    eventName: string,
    _eventName?: string | null,
    startDate?: string | null,
    endDate?: string | null,
    _logo?: string | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnCreateFilterSubscriptionVariables = {
  owner?: string | null,
};

export type OnCreateFilterSubscription = {
  onCreateFilter?:  {
    __typename: "Filter",
    id: string,
    name: string,
    modelName: string,
    filter: string,
    pinned?: boolean | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnUpdateFilterSubscriptionVariables = {
  owner?: string | null,
};

export type OnUpdateFilterSubscription = {
  onUpdateFilter?:  {
    __typename: "Filter",
    id: string,
    name: string,
    modelName: string,
    filter: string,
    pinned?: boolean | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnDeleteFilterSubscriptionVariables = {
  owner?: string | null,
};

export type OnDeleteFilterSubscription = {
  onDeleteFilter?:  {
    __typename: "Filter",
    id: string,
    name: string,
    modelName: string,
    filter: string,
    pinned?: boolean | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnCreateGuestSubscriptionVariables = {
  owner?: string | null,
};

export type OnCreateGuestSubscription = {
  onCreateGuest?:  {
    __typename: "Guest",
    id: string,
    group: string,
    nameFirst: string,
    _nameFirst?: string | null,
    nameLast: string,
    _nameLast?: string | null,
    company?: string | null,
    _company?: string | null,
    position?: string | null,
    _position?: string | null,
    dateOfBirth?: string | null,
    _keyPhoto?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnUpdateGuestSubscriptionVariables = {
  owner?: string | null,
};

export type OnUpdateGuestSubscription = {
  onUpdateGuest?:  {
    __typename: "Guest",
    id: string,
    group: string,
    nameFirst: string,
    _nameFirst?: string | null,
    nameLast: string,
    _nameLast?: string | null,
    company?: string | null,
    _company?: string | null,
    position?: string | null,
    _position?: string | null,
    dateOfBirth?: string | null,
    _keyPhoto?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};

export type OnDeleteGuestSubscriptionVariables = {
  owner?: string | null,
};

export type OnDeleteGuestSubscription = {
  onDeleteGuest?:  {
    __typename: "Guest",
    id: string,
    group: string,
    nameFirst: string,
    _nameFirst?: string | null,
    nameLast: string,
    _nameLast?: string | null,
    company?: string | null,
    _company?: string | null,
    position?: string | null,
    _position?: string | null,
    dateOfBirth?: string | null,
    _keyPhoto?: string | null,
    agenda?:  {
      __typename: "ModelAgendaConnection",
      nextToken?: string | null,
      startedAt?: number | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    _version: number,
    _deleted?: boolean | null,
    _lastChangedAt: number,
    owner?: string | null,
  } | null,
};
