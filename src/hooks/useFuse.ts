import { useState } from "react";

import Fuse from "fuse.js";

export type useFuseOptions<T> = Fuse.IFuseOptions<T> & { allOnNull?: boolean };

type Return<T> = [
  search: string,
  setSearch: (value: string) => void,
  items: T[]
];

const defaultOptions: useFuseOptions<any> = {
  allOnNull: true,
  isCaseSensitive: false,
  findAllMatches: true,
  shouldSort: true,
  threshold: 0.2,
  // includeScore: false,
  // includeMatches: false,
  // minMatchCharLength: 1,
  // location: 0,
  // distance: 100,
  // useExtendedSearch: false,
  // ignoreLocation: false,
  // ignoreFieldNorm: false,
  // fieldNormWeight: 1,
};

export const useFuse = <T>(
  list: T[],
  fuseOptions: useFuseOptions<T>
): Return<T> => {
  const [search, setSearch] = useState("");

  const options = { ...defaultOptions, ...fuseOptions };

  let results;
  if (search === "" && options.allOnNull) {
    results = list.map((item) => ({ item: item }));
  } else {
    const fuse = new Fuse(list, options);
    results = fuse.search(search);
  }

  const items = results.map(({ item }) => item);

  return [search, setSearch, items];
};
