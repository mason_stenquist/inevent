import { useEffect } from "react";

import { Recent, useRecents } from "./useRecents";
import { PersistentModel } from "@aws-amplify/datastore";
import { Model } from "~/@types/aws/model";
import { Guest } from "~/models";

const getRecent = <T extends PersistentModel>(
  model: Model,
  data: T | undefined
) => {
  if (!data) return;
  let recentData;
  switch (model.name) {
    case Guest.name:
      recentData = {
        name: data.nameFirst + " " + data.nameLast,
        photoKey: data.keyPhoto,
      };
      break;
    default:
      break;
  }
  return {
    modelName: model.name,
    id: data.id,
    path: window.location.pathname,
    ...recentData,
  } as Recent;
};

export const useTrackRecent = <T extends PersistentModel>(
  model: Model,
  data: T | undefined,
  loading: boolean = false
) => {
  const { getRecents, deleteExistingMatches, addCurrentRecent, saveRecents } =
    useRecents();
  const currentRecent = getRecent(model, data);

  useEffect(() => {
    if (loading) return;
    const recents = getRecents(model.name);
    const first = recents?.[0];
    if (
      currentRecent?.modelName &&
      currentRecent?.id &&
      currentRecent?.name &&
      currentRecent?.path &&
      first?.path !== currentRecent?.path
    ) {
      const cleanedRecents = deleteExistingMatches(recents, currentRecent.id);
      const recentsWithNew = addCurrentRecent(cleanedRecents, currentRecent);
      const newest = recentsWithNew.slice(0, 9);
      saveRecents(model.name, newest);
    }
  }, [
    currentRecent?.path,
    currentRecent?.photoKey,
    currentRecent?.name,
    loading,
  ]);
};
