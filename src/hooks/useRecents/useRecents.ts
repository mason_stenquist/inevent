import { Model } from "~/@types/aws/model";

export interface Recent {
  modelName: string;
  id: string;
  name: string;
  path: string;
  photoKey?: string;
}

export const useRecents = () => {
  const getStorageKey = (name: string) => `${name}Recents`;

  const getRecents = (modelName: string): Recent[] =>
    JSON.parse(localStorage.getItem(getStorageKey(modelName)) ?? "[]");

  const deleteExistingMatches = (recents: Recent[], id: string) => {
    return recents.filter((recent) => recent.id !== id);
  };

  const addCurrentRecent = (recents: Recent[], currentRecent: Recent) => [
    currentRecent,
    ...recents,
  ];

  const saveRecents = (modelName: string, recents: any[]) => {
    localStorage.setItem(getStorageKey(modelName), JSON.stringify(recents));
  };

  return {
    getRecents,
    deleteExistingMatches,
    addCurrentRecent,
    saveRecents,
  };
};
