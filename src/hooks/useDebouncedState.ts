import { useEffect, useState } from "react";

export const useDebouncedState = (
  initialValue: string,
  timeout: number
): [
  state: string,
  setState: (value: string) => void,
  debouncedState: string
] => {
  const [state, setState] = useState(initialValue);
  const [debouncedState, setDebouncedState] = useState(initialValue);

  useEffect(() => {
    const wait = state === "" ? 0 : timeout;
    const timer = setTimeout(() => setDebouncedState(state), wait);
    return () => clearTimeout(timer);
  }, [state]);

  return [state, setState, debouncedState];
};
