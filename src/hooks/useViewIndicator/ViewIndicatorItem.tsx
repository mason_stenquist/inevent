import { Tooltip } from "@chakra-ui/react";

import { View, useViewIndicatorContext } from "./useViewIndicator";

interface Props {
  view: View;
}

export const ViewIndicatorItem = ({ view }: Props) => {
  const { active, setActive } = useViewIndicatorContext();

  return (
    <Tooltip key={view.label} label={view.label} hasArrow>
      <button
        className={`p-2.5 rounded-lg ${
          active === view.label
            ? "bg-card shadow dark:bg-button-default"
            : "hover:bg-button-default-hover"
        }`}
        onClick={() => {
          setActive(view.label);
        }}
      >
        {view.icon}
      </button>
    </Tooltip>
  );
};
