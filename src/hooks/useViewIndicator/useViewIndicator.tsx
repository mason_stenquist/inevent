import { createContext, useContext, useMemo, useState } from "react";

import { ViewIndicator } from "./ViewIndicator";

export interface View {
  label: string;
  icon: JSX.Element;
}

const defaultState = {
  active: "",
  setActive: (label: string) => {},
};

const ViewIndicatorContext = createContext(defaultState);
export const useViewIndicatorContext = () => useContext(ViewIndicatorContext);

export const useViewIndicator = (views: View[], defaultView?: string) => {
  const [active, setActive] = useState(defaultView ?? views?.[0].label);

  const getPanelProps = (label: string) => ({
    style: { display: active === label ? "inherit" : "none" },
  });

  const contextValue = useMemo(
    () => ({ active, setActive }),
    [active, setActive]
  );

  const WiredUpIndicator = () => (
    <ViewIndicatorContext.Provider value={contextValue}>
      <ViewIndicator key={active} views={views} />
    </ViewIndicatorContext.Provider>
  );

  return { ViewIndicator: WiredUpIndicator, getPanelProps };
};
