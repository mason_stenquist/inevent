import { View, ViewIndicatorItem } from ".";

interface Props {
  views: View[];
}

export const ViewIndicator = ({ views }: Props) => {
  return (
    <div className="rounded-lg bg-button-default flex space-x-1 p-1 dark:bg-card shadow-inner">
      {views.map((view) => (
        <ViewIndicatorItem key={view.label} view={view} />
      ))}
    </div>
  );
};
