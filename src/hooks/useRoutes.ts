import { I18n } from "aws-amplify";
import {
  Params,
  generatePath,
  matchPath,
  useLocation,
  useNavigate,
} from "react-router-dom";

export const useRoutes = () => {
  const nav = useNavigate();
  const t = I18n.get;
  const { pathname } = useLocation();

  const route = <RouteParams extends Params>(label: string, path: string) => {
    const routeObj = {
      path,
      label,
      url: (params?: RouteParams) => {
        try {
          return generatePath(path, params);
        } catch (e) {
          console.log(e);
          return window.location.pathname + window.location.search;
        }
      },
      go: (params?: RouteParams) => nav(routeObj.url(params)),
    };
    return routeObj;
  };

  const routes = {
    home: route(t`home`, "/event/"),

    guestIndex: route(t`guests`, "/event/guests"),
    guestImport: route(t`guestImport`, "/event/guests/import"),
    guestDetail: route<{ id: string }>(t`guestDetail`, "/event/guests/:id"),

    flightIndex: route(t`flights`, "/event/flights"),
    housingIndex: route(t`housing`, "/event/housing"),
    reportIndex: route(t`reports`, "/event/reports"),
    reportPreview: route<{ path: string }>(
      t`reportPreview`,
      "/event/reports/:path"
    ),
    settingsIndex: route(t`settings`, "/settings"),
    settingsMyAccount: route(t`myAccount`, "/settings/my-account"),
    settingsBilling: route(t`billing`, "/settings/billing"),
    settingsEvents: route(t`eventManagement`, "/settings/events"),
    settingsEventsNew: route(t`newEvent`, "/settings/events/new"),
    settingsEventsDetail: route<{ id: string }>(
      t`newEvent`,
      "/settings/events/:id"
    ),
    settingsUsers: route(t`userManagement`, "/settings/users"),

    // Activity Routes
    activityIndex: route(t`activities`, "/event/activities"),
    activityImport: route(t`activityImport`, "/event/activities/import"),
    activityDetail: route<{ id: string }>(
      t`activityDetail`,
      "/event/activities/:id"
    ),

    // PLOP ROUTES ENTRYPOINT (Do not remove)
  };

  const findRouteByPathname = (url: string) => {
    const routeArray = Object.values(routes);
    return routeArray.find(({ path }) => matchPath(path, url));
  };

  const currentRoute = () => {
    return findRouteByPathname(pathname);
  };

  const breadcrumbs = () => {
    const pathArray = pathname.split("/").filter(Boolean);
    return pathArray.map((_, index) => {
      const url = `/${pathArray.slice(0, index + 1).join("/")}`;
      return findRouteByPathname(url);
    });
  };

  return { routes, currentRoute: currentRoute(), breadcrumbs: breadcrumbs() };
};
