import { useEffect, useState } from "react";

export interface UseRecordSelection<T> {
  selected: T[];
  hasAllSelected: boolean;
  hasAnySelected: boolean;
  hasSomeSelected: boolean;
  isSelected: (record: T) => boolean;
  setSelected: React.Dispatch<React.SetStateAction<T[]>>;
  toggleSelected: (record: T, e: any) => void;
  toggleAllSelected: () => void;
  clearAll: () => void;
}

export const useRecordSelection = <T>(
  fullDataSet: T[] = [],
  equalityKey: keyof T,
  initSelected: T[] = []
): UseRecordSelection<T> => {
  const [selected, setSelected] = useState<T[]>(initSelected);
  const [lastIndex, setLastIndex] = useState<number>();

  const hasAllSelected =
    selected.length === fullDataSet.length && selected.length > 0;
  const hasAnySelected = selected.length > 0;
  const hasSomeSelected = !hasAllSelected && selected.length > 0;

  useEffect(() => {
    if (selected.length === 0 && lastIndex !== undefined) {
      setLastIndex(undefined);
    }
  }, [selected, lastIndex]);

  const isSelected = (record: T) =>
    selected.some((s) => s[equalityKey] === record[equalityKey]);

  const clearAll = () => setSelected([]);
  const toggleAllSelected = () => {
    setSelected(hasAllSelected ? [] : fullDataSet);
  };

  const toggleSelected = (record: T, e: any) => {
    const selectedIndex = fullDataSet.findIndex(
      (s) => s[equalityKey] === record[equalityKey]
    );
    setLastIndex(selectedIndex);

    if (
      lastIndex !== undefined &&
      selectedIndex !== undefined &&
      lastIndex !== selectedIndex &&
      e.shiftKey
    ) {
      const start = Math.min(lastIndex, selectedIndex);
      const end = Math.max(lastIndex, selectedIndex) + 1;
      const records = fullDataSet.slice(start, end);
      modifySelectionGroup(records, !isSelected(record));
    } else {
      toggleOneSelected(record);
    }
  };

  const modifySelectionGroup = (records: T[], select: boolean) => {
    records.forEach((record) => {
      if (isSelected(record) !== select) {
        toggleOneSelected(record);
      }
    });
  };

  const toggleOneSelected = (record: T) => {
    if (isSelected(record)) {
      setSelected((s) =>
        s.filter((s) => s[equalityKey] !== record[equalityKey])
      );
    } else {
      setSelected((s) => [...s, record]);
    }
  };

  return {
    selected,
    hasAllSelected,
    hasAnySelected,
    hasSomeSelected,
    isSelected,
    setSelected,
    toggleSelected,
    toggleAllSelected,
    clearAll,
  };
};
