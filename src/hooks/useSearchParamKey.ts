import { useSearchParams } from "react-router-dom";

import { serializeData } from "~/utils";

export const useSearchParamKey = (
  key: string,
  startingValue: string = ""
): [searchParam: string, setSearchParam: (value: string) => void] => {
  const [searchParams, setSearchParams] = useSearchParams();

  const searchParam = searchParams.get(key) ?? startingValue;

  const setSearchParam = (value: string) => {
    const params = serializeData(searchParams);
    setSearchParams({ ...params, [key]: value });
  };

  return [searchParam, setSearchParam];
};
