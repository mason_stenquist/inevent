import { atom, useAtom } from "jotai";

interface DrawerProps {
  isOpen?: boolean;
  title?: string;
  body: JSX.Element;
  footer?: JSX.Element;
  closeButton?: boolean;
  autoFocus?: boolean;
  closeOnEsc?: boolean;
  closeOnOverlayClick?: boolean;
  placement?: "left" | "right" | "top" | "bottom";
  size?: "xs" | "sm" | "md" | "lg" | "xl" | "full";
}

export const drawerDefaults: DrawerProps = {
  isOpen: false,
  body: <div />,
  title: undefined,
  footer: <div />,
  closeButton: true,
  autoFocus: true,
  closeOnEsc: true,
  closeOnOverlayClick: true,
  placement: "right",
  size: "md",
};

export const drawerAtom = atom<DrawerProps>(drawerDefaults);

export const useDrawer = () => {
  const [_state, setState] = useAtom(drawerAtom);

  const closeDrawer = () => {
    setState(drawerDefaults);
  };

  const openDrawer = (props: DrawerProps) => {
    setState({ ...drawerDefaults, ...props, isOpen: true });
  };

  return { openDrawer, closeDrawer };
};
