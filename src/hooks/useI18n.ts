import { I18n } from "aws-amplify";
import { useAtomValue } from "jotai/utils";

import globalDict from "~/modules/global/translations";
import guestDict from "~/modules/guest/translations";
import reportDict from "~/modules/report/translations";

import importerDict from "~/components/importer/translations";

import { langAtom } from "~/state";


        import activityDict from "~/modules/activity/translations";
        // PLOP IMPORT ENTRYPOINT (Do not remove)

declare global {
  var t: (key: any, defVal?: any) => any;
}

export const useI18n = () => {
  const lang = useAtomValue(langAtom);

  //Init dictionaries
  I18n.putVocabularies(globalDict);
  I18n.putVocabularies(importerDict);
  I18n.putVocabularies(reportDict);
  I18n.putVocabularies(guestDict);

  
        I18n.putVocabularies(activityDict);
        // PLOP VOCAB ENTRYPOINT (Do not remove)

  //Bind translation to global space
  t = I18n.get;

  //Set default language from locale storage
  I18n.setLanguage(lang);

  return lang;
};
