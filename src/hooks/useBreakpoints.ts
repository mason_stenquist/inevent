import { useMedia } from "react-use";
import { breakpoints } from "~/theme";

const minusOne = (size: string) => `${parseInt(size) - 1}px`;
export const isSmallOrAbove = () =>
  useMedia(`(max-width: ${minusOne(breakpoints.sm)})`);
export const isMediumOrAbove = () =>
  useMedia(`(max-width: ${minusOne(breakpoints.md)})`);
export const isLargeOrAbove = () =>
  useMedia(`(max-width: ${minusOne(breakpoints.lg)})`);
export const isExtraLargeOrAbove = () =>
  useMedia(`(max-width: ${minusOne(breakpoints.xl)})`);
export const is2ExtraLargeOrAbove = () =>
  useMedia(`(max-width: ${minusOne(breakpoints["2xl"])})`);

export const isSmallOrBelow = () => useMedia(`(min-width: ${breakpoints.sm})`);
export const isMediumOrBelow = () => useMedia(`(min-width: ${breakpoints.md})`);
export const isLargeOrBelow = () => useMedia(`(min-width: ${breakpoints.lg})`);
export const isExtraLargeOrBelow = () =>
  useMedia(`(min-width: ${breakpoints.xl})`);
export const is2ExtraLargeOrBelow = () =>
  useMedia(`(min-width: ${breakpoints["2xl"]})`);
