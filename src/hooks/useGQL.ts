import { QueryKey, UseQueryOptions, useQuery } from "react-query";

interface QueryObject<ApiResponse> {
  key: (string | undefined)[];
  fetch: () => Promise<ApiResponse>;
}

type Options<ApiResponse> = Omit<
  UseQueryOptions<ApiResponse, Error, ApiResponse, QueryKey>,
  "queryKey" | "queryFn"
>;

export function useGQL<ApiResponse>(
  queryObject: QueryObject<ApiResponse>,
  options?: Options<ApiResponse>
) {
  const query = useQuery<ApiResponse, Error>(
    queryObject.key,
    async () => queryObject.fetch(),
    options
  );
  return query;
}
