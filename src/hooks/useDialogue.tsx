import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogCloseButton,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
} from "@chakra-ui/react";
import { atom, useAtom } from "jotai";
import { useRef } from "react";

interface Button {
  label: string;
  value: string;
}

interface DialogueState {
  isOpen: boolean;
  title: string;
  body: string;
  showCancel: boolean;
  buttons: Button[];
}
export const DialogueAtom = atom<DialogueState>({
  isOpen: false,
  title: "",
  body: "",
  showCancel: true,
  buttons: [],
});

declare global {
  interface Window {
    resolveDialogue: (value: string | PromiseLike<string>) => void;
  }
}

export const useDialogue = () => {
  const [dialogueState, setDialogueState] = useAtom(DialogueAtom);
  const cancelRef = useRef(null);

  const cancel = () => {
    setDialogueState((s) => ({ ...s, isOpen: false }));
    window.resolveDialogue("cancel");
  };

  const handleClick = (value: string) => {
    setDialogueState((s) => ({ ...s, isOpen: false }));
    window.resolveDialogue(value);
  };

  const showDialogue = async (props: Omit<DialogueState, "isOpen">) => {
    setDialogueState((s) => ({ ...props, isOpen: true }));

    return new Promise<string>((resolve, reject) => {
      window.resolveDialogue = resolve;
      setTimeout(() => {
        reject("1 Hour Timeout Reached");
      }, 1000 * 60 * 60);
    });
  };

  const Dialogue = () => (
    <AlertDialog
      motionPreset="slideInBottom"
      leastDestructiveRef={cancelRef}
      onClose={cancel}
      isOpen={dialogueState.isOpen}
      isCentered
    >
      <AlertDialogOverlay />

      <AlertDialogContent>
        <AlertDialogHeader>{dialogueState.title}</AlertDialogHeader>
        {dialogueState.showCancel && <AlertDialogCloseButton />}
        <AlertDialogBody>{dialogueState.body}</AlertDialogBody>
        <AlertDialogFooter className="space-x-3">
          {dialogueState.showCancel && (
            <Button type="button" ref={cancelRef} onClick={cancel}>
              {t`cancel`}
            </Button>
          )}
          {dialogueState.buttons.map((button, index) => (
            <Button
              key={button.value}
              onClick={() => handleClick(button.value)}
            >
              {button.label}
            </Button>
          ))}
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );

  return { showDialogue, Dialogue };
};
