import {
  Modal as ChakraModal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  useColorMode,
} from "@chakra-ui/react";
import { atom, useAtom } from "jotai";

import { PromiseResult, ResolvePromise } from "~/@types";

export interface ModalState {
  isOpen: boolean;
  title: string;
  body: JSX.Element;
  showCancel?: boolean;
  size?:
    | "xs"
    | "sm"
    | "md"
    | "lg"
    | "xl"
    | "2xl"
    | "3xl"
    | "4xl"
    | "5xl"
    | "6xl"
    | "full";
}
export const ModalAtom = atom<ModalState>({
  isOpen: false,
  title: "",
  body: <div />,
  showCancel: true,
  size: undefined,
});

declare global {
  interface Window {
    resolveModal: ResolvePromise;
  }
}

export const useModal = () => {
  const { colorMode } = useColorMode();
  const [modalState, setModalState] = useAtom(ModalAtom);

  const showModal = async (props: Omit<ModalState, "isOpen">) => {
    setModalState((s) => ({ ...props, isOpen: true }));

    return new Promise<PromiseResult>((resolve, reject) => {
      window.resolveModal = resolve;
      setTimeout(() => {
        reject("1 Hour Timeout Reached");
      }, 1000 * 60 * 60);
    });
  };

  const closeModal = () => {
    setModalState((s) => ({ ...s, isOpen: false }));
    window.resolveModal({ cancel: true });
  };

  const Modal = () => (
    <ChakraModal
      blockScrollOnMount={false}
      isOpen={modalState.isOpen}
      onClose={closeModal}
      scrollBehavior="inside"
      size={modalState.size}
    >
      <ModalOverlay />
      <ModalContent className={colorMode}>
        <ModalHeader>{modalState.title}</ModalHeader>
        <ModalCloseButton />
        {modalState.body}
      </ModalContent>
    </ChakraModal>
  );

  return { showModal, closeModal, Modal };
};
