import { atomWithStorage } from "jotai/utils";

const initLang = new Intl.Locale(window.navigator.language ?? "en").language;
export const langAtom = atomWithStorage("lang", initLang);
