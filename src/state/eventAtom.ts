import { useAtom } from "jotai";
import { atomWithStorage } from "jotai/utils";

import { Event } from "~/models";

const eventAtom = atomWithStorage<Event | null>("event", null);
export const useEvent = () => useAtom(eventAtom);
