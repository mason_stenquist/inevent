import {
  NonModelTypeConstructor,
  PersistentModelConstructor,
} from "@aws-amplify/datastore";

export type Model =
  | NonModelTypeConstructor<any>
  | PersistentModelConstructor<
      any,
      {
        readOnlyFields: "createdAt" | "updatedAt";
      }
    >;
