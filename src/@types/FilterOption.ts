export interface FilterOption<T> {
  field: keyof T & string;
  queryField: keyof T & string;
  noGlobalSearch?: true;
  predicate: "contains" | "lt" | "gt";
}
