interface CancelResult {
  cancel: true;
}

interface ConfirmResult {
  cancel: false;
  results: any;
}

export type PromiseResult = CancelResult | ConfirmResult;

export type ResolvePromise = (
  value: PromiseResult | PromiseLike<PromiseResult>
) => void;
