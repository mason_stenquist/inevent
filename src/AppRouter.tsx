import React from "react";
import { Link, Route, Routes } from "react-router-dom";

import App from "./App";
import DefaultLayout from "./layouts/DefaultLayout";
import { Activity, Guest } from "./models";
import { Overlays } from "~/components";
import { useI18n, useRoutes } from "~/hooks";
import { Home } from "~/modules";
import { reports } from "~/reports";

//Lazy loaded components

const Importer = React.lazy(() => import("~/components/importer"));
const ReportIndex = React.lazy(() => import("~/modules/report/ReportIndex"));
const SettingsIndex = React.lazy(
  () => import("~/modules/settings/SettingsIndex")
);
const MyAccount = React.lazy(() => import("~/modules/settings/MyAccount"));
const Billing = React.lazy(() => import("~/modules/settings/Billing"));
const EventIndex = React.lazy(
  () => import("~/modules/settings/event-management/EventIndex")
);
const EventNew = React.lazy(
  () => import("~/modules/settings/event-management/EventNew")
);
const EventDetail = React.lazy(
  () => import("~/modules/settings/event-management/EventDetail")
);

// Guest Modules
const GuestIndex = React.lazy(() => import("~/modules/guest/GuestIndex"));
const GuestDetail = React.lazy(() => import("~/modules/guest/GuestDetail"));

// Activity Modules
const ActivityIndex = React.lazy(
  () => import("~/modules/activity/ActivityIndex")
);
const ActivityDetail = React.lazy(
  () => import("~/modules/activity/ActivityDetail")
);

// PLOP LAZYLOAD ENTRYPOINT (Do not remove)

const AppRouter = () => {
  const { routes } = useRoutes();
  const lang = useI18n();

  return (
    <div key={lang}>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<Link to="/event">Test</Link>} />
        </Route>

        <Route path={routes.home.path} element={<DefaultLayout />}>
          <Route index element={<Home />} />

          {/* GUEST MODULES */}
          <Route path={routes.guestIndex.path} element={<GuestIndex />} />
          <Route path={routes.guestDetail.path} element={<GuestDetail />} />
          <Route
            path={routes.guestImport.path}
            element={<Importer model={Guest} />}
          />

          {/* ACTIVITY MODULES */}
          <Route path={routes.activityIndex.path} element={<ActivityIndex />} />
          <Route
            path={routes.activityDetail.path}
            element={<ActivityDetail />}
          />
          <Route
            path={routes.activityImport.path}
            element={<Importer model={Activity} />}
          />

          {/* PLOP ROUTER ENTRYPOINT (Do not remove) */}

          {/* REPORT PREVIEWS */}
          <Route path="reports" element={<ReportIndex />}>
            {reports.map(({ path, reportOptions }) => (
              <Route key={path} path={path} element={reportOptions} />
            ))}
          </Route>

          {/* REPORTS */}
        </Route>
        {reports.map(({ path, report }) => (
          <Route key={path} path={`/r/${path}`} element={report} />
        ))}

        {/* SETTINGS */}
        <Route
          path={routes.settingsIndex.path}
          element={
            <DefaultLayout>
              <SettingsIndex />
            </DefaultLayout>
          }
        >
          <Route index element={<div>Loading...</div>} />
          <Route path={routes.settingsMyAccount.path} element={<MyAccount />} />
          <Route path={routes.settingsBilling.path} element={<Billing />} />
          <Route path={routes.settingsEvents.path} element={<EventIndex />} />
          <Route path={routes.settingsEventsNew.path} element={<EventNew />} />
          <Route
            path={routes.settingsEventsDetail.path}
            element={<EventDetail />}
          />
        </Route>
      </Routes>
      <Overlays />
    </div>
  );
};

export default AppRouter;
