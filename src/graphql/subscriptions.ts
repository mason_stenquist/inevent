/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateActivity = /* GraphQL */ `
  subscription OnCreateActivity($owner: String) {
    onCreateActivity(owner: $owner) {
      id
      group
      name
      category
      description
      date
      start
      end
      agenda {
        nextToken
        startedAt
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onUpdateActivity = /* GraphQL */ `
  subscription OnUpdateActivity($owner: String) {
    onUpdateActivity(owner: $owner) {
      id
      group
      name
      category
      description
      date
      start
      end
      agenda {
        nextToken
        startedAt
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onDeleteActivity = /* GraphQL */ `
  subscription OnDeleteActivity($owner: String) {
    onDeleteActivity(owner: $owner) {
      id
      group
      name
      category
      description
      date
      start
      end
      agenda {
        nextToken
        startedAt
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onCreateAgenda = /* GraphQL */ `
  subscription OnCreateAgenda($owner: String) {
    onCreateAgenda(owner: $owner) {
      id
      group
      guestID
      guest {
        id
        group
        nameFirst
        _nameFirst
        nameLast
        _nameLast
        company
        _company
        position
        _position
        dateOfBirth
        _keyPhoto
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
      }
      activityID
      activity {
        id
        group
        name
        category
        description
        date
        start
        end
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onUpdateAgenda = /* GraphQL */ `
  subscription OnUpdateAgenda($owner: String) {
    onUpdateAgenda(owner: $owner) {
      id
      group
      guestID
      guest {
        id
        group
        nameFirst
        _nameFirst
        nameLast
        _nameLast
        company
        _company
        position
        _position
        dateOfBirth
        _keyPhoto
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
      }
      activityID
      activity {
        id
        group
        name
        category
        description
        date
        start
        end
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onDeleteAgenda = /* GraphQL */ `
  subscription OnDeleteAgenda($owner: String) {
    onDeleteAgenda(owner: $owner) {
      id
      group
      guestID
      guest {
        id
        group
        nameFirst
        _nameFirst
        nameLast
        _nameLast
        company
        _company
        position
        _position
        dateOfBirth
        _keyPhoto
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
      }
      activityID
      activity {
        id
        group
        name
        category
        description
        date
        start
        end
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onCreateEvent = /* GraphQL */ `
  subscription OnCreateEvent($owner: String) {
    onCreateEvent(owner: $owner) {
      id
      group
      eventName
      _eventName
      startDate
      endDate
      _logo
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onUpdateEvent = /* GraphQL */ `
  subscription OnUpdateEvent($owner: String) {
    onUpdateEvent(owner: $owner) {
      id
      group
      eventName
      _eventName
      startDate
      endDate
      _logo
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onDeleteEvent = /* GraphQL */ `
  subscription OnDeleteEvent($owner: String) {
    onDeleteEvent(owner: $owner) {
      id
      group
      eventName
      _eventName
      startDate
      endDate
      _logo
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onCreateFilter = /* GraphQL */ `
  subscription OnCreateFilter($owner: String) {
    onCreateFilter(owner: $owner) {
      id
      name
      modelName
      filter
      pinned
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onUpdateFilter = /* GraphQL */ `
  subscription OnUpdateFilter($owner: String) {
    onUpdateFilter(owner: $owner) {
      id
      name
      modelName
      filter
      pinned
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onDeleteFilter = /* GraphQL */ `
  subscription OnDeleteFilter($owner: String) {
    onDeleteFilter(owner: $owner) {
      id
      name
      modelName
      filter
      pinned
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onCreateGuest = /* GraphQL */ `
  subscription OnCreateGuest($owner: String) {
    onCreateGuest(owner: $owner) {
      id
      group
      nameFirst
      _nameFirst
      nameLast
      _nameLast
      company
      _company
      position
      _position
      dateOfBirth
      _keyPhoto
      agenda {
        nextToken
        startedAt
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onUpdateGuest = /* GraphQL */ `
  subscription OnUpdateGuest($owner: String) {
    onUpdateGuest(owner: $owner) {
      id
      group
      nameFirst
      _nameFirst
      nameLast
      _nameLast
      company
      _company
      position
      _position
      dateOfBirth
      _keyPhoto
      agenda {
        nextToken
        startedAt
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
export const onDeleteGuest = /* GraphQL */ `
  subscription OnDeleteGuest($owner: String) {
    onDeleteGuest(owner: $owner) {
      id
      group
      nameFirst
      _nameFirst
      nameLast
      _nameLast
      company
      _company
      position
      _position
      dateOfBirth
      _keyPhoto
      agenda {
        nextToken
        startedAt
      }
      createdAt
      updatedAt
      _version
      _deleted
      _lastChangedAt
      owner
    }
  }
`;
