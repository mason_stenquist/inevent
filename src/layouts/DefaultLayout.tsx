import { Spinner, useColorMode } from "@chakra-ui/react";
import {
  DotsHorizontalIcon,
  HomeIcon,
  PaperAirplaneIcon,
  PuzzleIcon,
  UserIcon,
} from "@heroicons/react/outline";
import { Suspense, useEffect } from "react";
import { NavLink, Outlet, useLocation } from "react-router-dom";

import { Header } from "~/components";
import { useRoutes } from "~/hooks";

export default function DefaultLayout({
  children,
}: {
  children?: JSX.Element;
}) {
  const { colorMode } = useColorMode();
  const { pathname } = useLocation();
  const { routes } = useRoutes();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <div className={`app bg-page pt-10 sm:pt-0 ${colorMode}`}>
      <div className="min-h-screen w-full  transition-all duration-200">
        <div className=" mx-auto max-w-screen-2xl px-4">
          <Header />
          <div className="pb-20 sm:pb-4 2xl:pb-8">
            <Suspense
              fallback={
                <div className="flex-center py-20">
                  <Spinner />
                </div>
              }
            >
              {!children && <Outlet key={pathname} />}
              {children}
            </Suspense>
          </div>
        </div>
      </div>

      <div className="fixed bottom-0 flex w-full justify-evenly border-t bg-card  sm:hidden">
        <NavLink to={routes.home.url()} className="mobile-nav">
          <HomeIcon className="mx-auto w-5" />
          <span>{routes.home.label}</span>
        </NavLink>
        <NavLink to={routes.guestIndex.url()} className="mobile-nav">
          <UserIcon className="mx-auto w-5" />
          <span>{routes.guestIndex.label}</span>
        </NavLink>
        <NavLink to={routes.flightIndex.url()} className="mobile-nav">
          <PaperAirplaneIcon className="mx-auto w-5" />
          <span>{routes.flightIndex.label}</span>
        </NavLink>
        <NavLink to={routes.activityIndex.url()} className="mobile-nav">
          <PuzzleIcon className="mx-auto w-5" />
          <span>{routes.activityIndex.label}</span>
        </NavLink>
        <NavLink to={routes.reportIndex.url()} className="mobile-nav">
          <DotsHorizontalIcon className="mx-auto w-5" />
          <span>{t`more`}</span>
        </NavLink>
      </div>
    </div>
  );
}
