import {
  ChevronDownIcon,
  ChevronUpIcon,
  SelectorIcon,
} from "@heroicons/react/outline";
import { useSearchParams } from "react-router-dom";

import { useSearchParamKey } from "~/hooks/useSearchParamKey";

import { SearchForm, SearchInput } from "../search-form";
import { Column } from "./TableList";
import { serializeData } from "~/utils";

interface Props {
  column: Column<any>;
  globalSearchKey?: string;
}

const alignMap = {
  left: "text-left",
  center: "text-center",
  right: "text-right",
};

export const SortHeader: React.FC<Props> = ({ column, globalSearchKey }) => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [globalSearch, setGlobalSearch] = useSearchParamKey(
    globalSearchKey ?? ""
  );
  const [sortField] = useSearchParamKey("sortField");
  const [sortDirection] = useSearchParamKey("sortDirection");

  const handleSort = () => {
    const params = serializeData(searchParams);
    const sortParams = {
      sortField: column.key,
      sortDirection:
        sortDirection === "ascending" && column.key === sortField
          ? "descending"
          : "ascending",
    };

    setSearchParams({ ...params, ...sortParams });
  };

  const textAlign = alignMap[column.align ?? "left"];

  const handleChange = () => {
    if (globalSearch) {
      setGlobalSearch("");
    }
  };

  return (
    <div className="hidden md:block pb-2 md:-mx-[1px] md:border-b md:pb-4">
      <button
        className="hidden items-center space-x-2 pb-2 text-sm font-bold  hover:text-accent focus:text-accent focus:outline-none md:flex"
        onClick={handleSort}
      >
        <span>{t(column.key, column.label)}</span>
        {sortField !== column.key && <SelectorIcon className="w-4" />}
        {sortField === column.key && sortDirection === "ascending" && (
          <ChevronDownIcon className="w-4" />
        )}
        {sortField === column.key && sortDirection === "descending" && (
          <ChevronUpIcon className="w-4" />
        )}
      </button>
      <div className="md:pr-2">
        <SearchForm>
          <SearchInput
            className={`default-input rounded ${textAlign}`}
            placeholder={`Filter ${t(column.key)}`}
            name={column.key}
            autoComplete={`${column.key}Nope`}
            onChange={handleChange}
          />
        </SearchForm>
      </div>
    </div>
  );
};
