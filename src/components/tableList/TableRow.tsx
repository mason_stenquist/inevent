import { Checkbox } from "@chakra-ui/react";
import { memo, useRef } from "react";
import { generatePath, useNavigate } from "react-router-dom";

import { Column } from "./TableList";

interface Props<T> {
  item: T;
  index: number;
  onFocus: (index: number) => void;
  columns: Column<T>[];
  rowTo: string;
  toggleSelected: (item: T, e: any) => void;
  isSelected: boolean;
}
export const TableRow = <T extends { id: string }>({
  item,
  index,
  onFocus,
  columns,
  rowTo,
  toggleSelected,
  isSelected,
}: Props<T>) => {
  const ref = useRef<HTMLTableRowElement>(null);
  const navigate = useNavigate();
  const handleClick = () => {
    navigate(generatePath(rowTo, item));
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLTableRowElement>) => {
    if (e.key === "Enter") {
      handleClick();
    }
    if (!ref.current) return;
    if (e.key === "ArrowDown" && ref?.current?.nextSibling) {
      // @ts-ignore
      ref?.current?.nextSibling.focus();
    }
    if (e.key === "ArrowUp" && ref?.current?.previousSibling) {
      // @ts-ignore
      ref?.current?.previousSibling.focus();
    }
  };

  return (
    <tr
      ref={ref}
      onKeyDown={handleKeyDown}
      tabIndex={0}
      onFocus={() => onFocus(index)}
      className="odd:bg-page/25 block cursor-pointer hover:bg-table-row-hover focus:bg-table-row-hover focus:outline-none focus:ring md:table-row md:border-none"
    >
      <td>
        <div className="">
          <Checkbox
            isChecked={isSelected}
            onChange={(e) => toggleSelected(item, e.nativeEvent)}
            className="p-1"
            size="lg"
            value={"true"}
          />
        </div>
      </td>
      {columns.map((column) => (
        <td
          key={column.key}
          onClick={handleClick}
          align={column.align ?? "left"}
          className="flex justify-between py-2 text-xs md:table-cell md:text-sm lg:text-base max-w-0"
        >
          <p className="inline md:hidden">{column.label}</p>
          <p className="truncate">
            <>{item[column.key]}</>
          </p>
        </td>
      ))}
    </tr>
  );
};
