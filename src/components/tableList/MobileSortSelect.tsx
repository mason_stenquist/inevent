import { Button, InputGroup, InputLeftAddon, Select } from "@chakra-ui/react";
import { ArrowDownIcon } from "@heroicons/react/outline";
import { ChangeEvent, FormEvent, FormEventHandler } from "react";

import { useSearchParamKey } from "~/hooks/useSearchParamKey";

import { SearchSelect } from "../search-form/SearchSelect";
import { Column } from "./TableList";
import { sortBy } from "cypress/types/lodash";

interface Props {
  columns: Column<any>[];
}
export const MobileSortSelect: React.FC<Props> = ({ columns }) => {
  const [sortField] = useSearchParamKey("sortField");
  const [sortDirection, setSortDirection] = useSearchParamKey("sortDirection");

  const toggleSortDirection = () => {
    setSortDirection(
      sortDirection === "ascending" ? "descending" : "ascending"
    );
  };

  const rotate = sortDirection === "descending";

  return (
    <div className="flex flex-grow">
      <SearchSelect
        roundedRight="none"
        variant="filled"
        name="sortField"
        placeholder={t`sortBy`}
      >
        {columns.map((column) => (
          <option value={column.key} key={column.key}>
            {t(column.key, column.label)}
          </option>
        ))}
      </SearchSelect>
      <Button
        roundedLeft="none"
        type="button"
        onClick={toggleSortDirection}
        isDisabled={!sortField}
      >
        <ArrowDownIcon
          className={`w-5 transition-all duration-200 ${
            rotate ? "rotate-180" : ""
          }`}
        />
      </Button>
    </div>
  );
};
