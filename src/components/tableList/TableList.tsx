import { Button, Checkbox, Collapse, Show } from "@chakra-ui/react";
import { FilterIcon } from "@heroicons/react/outline";
import {
  Children,
  ReactChild,
  ReactElement,
  cloneElement,
  memo,
  useState,
} from "react";
import { useMedia } from "react-use";

import { MobileSortSelect, SortHeader, TableRow } from ".";
import { useRecordSelection } from "~/hooks";

export interface Column<T> {
  key: keyof T & string;
  label?: string;
  minWidth?: number;
  align?: "left" | "right" | "center";
}

interface Props<T> {
  columns: Column<T>[];
  data: T[];
  rowTo: string;
  globalSearchKey?: string;
  RenderMobile?: (item: T, rowTo: string) => JSX.Element;
  children?: ReactChild | ReactChild[];
}

const TableComponent = <T extends { id: string }>({
  columns,
  data,
  rowTo,
  globalSearchKey,
  RenderMobile,
  children,
}: Props<T>) => {
  const [focusIndex, setFocusIndex] = useState(0);
  const checkboxes = useRecordSelection(data, "id");

  const isDesktop = useMedia("(min-width: 768px)");

  return (
    <div className="">
      <Show below="md">
        <div className="flex space-x-4">
          <MobileSortSelect columns={columns} />
        </div>
      </Show>
      <table className="block w-full md:table">
        <thead className={`block pb-4 md:table-header-group md:pb-0`}>
          <tr
            className={`hidden md:table-row overflow-hidden transition-all duration-300`}
          >
            <th className="hidden md:table-cell top-0 bg-card transition-all duration-200 md:sticky">
              <div className="-mx-px border-b h-[85px] flex items-end pb-5 pl-[5px]">
                <Checkbox
                  isChecked={checkboxes.hasAllSelected}
                  isIndeterminate={checkboxes.hasSomeSelected}
                  onChange={checkboxes.toggleAllSelected}
                  size="lg"
                  title={t`selectAll`}
                />
              </div>
            </th>
            {columns.map((column) => (
              <th
                key={column.key}
                align={column.align ?? "left"}
                className="top-0 block bg-card transition-all duration-200 md:sticky md:table-cell"
              >
                <SortHeader globalSearchKey={globalSearchKey} column={column} />
              </th>
            ))}
          </tr>
        </thead>
        <tbody className="block md:table-row-group">
          <Show above="md">
            <tr>
              <td className="top-[86px] sticky" colSpan={42}>
                <Collapse in={checkboxes.hasAnySelected}>
                  <div className="bg-card transition-all duration-300 border-b py-3 space-x-3">
                    <Button onClick={checkboxes.clearAll}>{t`cancel`}</Button>
                    {Children.map(children, (child) => {
                      const component = child as ReactElement;
                      const interceptedClick = () => {
                        component?.props?.onClick(checkboxes.selected);
                        checkboxes.clearAll();
                      };
                      const interceptedChildren = () =>
                        component?.props?.children(checkboxes.selected.length);
                      const newButton = cloneElement(component, {
                        onClick: interceptedClick,
                        children: interceptedChildren,
                      });
                      return newButton;
                    })}
                  </div>
                </Collapse>
              </td>
            </tr>
          </Show>
          {data.map((item, index) =>
            !isDesktop && !!RenderMobile ? (
              <div className="-mx-4" key={`${item.id}-render-mobile`}>
                {RenderMobile(item, rowTo)}
              </div>
            ) : (
              <TableRow
                key={item.id}
                item={item}
                index={index}
                onFocus={setFocusIndex}
                columns={columns}
                rowTo={rowTo}
                toggleSelected={checkboxes.toggleSelected}
                isSelected={checkboxes.isSelected(item)}
              />
            )
          )}
        </tbody>
      </table>
    </div>
  );
};

// eslint-disable-next-line react/display-name
export const TableList = memo(TableComponent, (prev, next) => {
  return prev.data === next.data;
}) as typeof TableComponent;
