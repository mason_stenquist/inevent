import { Button, ButtonProps } from "@chakra-ui/react";
import { ReactElement } from "react";

interface Props<T> extends Omit<ButtonProps, "onClick" | "children"> {
  onClick: (selected: T[]) => void;
  children: (selectedCount: number) => string;
}

export const TableActionButton = <T extends { id: string }>({
  onClick,
  children,
  ...rest
}: Props<T>) => {
  return (
    <Button {...rest} onClick={() => onClick([])}>
      {children(0)}
    </Button>
  );
};
