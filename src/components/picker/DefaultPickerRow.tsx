import { CheckIcon } from "@heroicons/react/outline";

interface Props<T> {
  item: T;
  isSelected: boolean;
  select: (item: T, e: any) => void;
  children: JSX.Element;
}

export const DefaultPickerRow = <T extends { id: string }>({
  item,
  isSelected,
  select,
  children,
}: Props<T>) => (
  <button
    className="flex items-center rounded-none px-1 justify-between w-full pt-1 pb-2 text-left focus:shadow-none focus:outline-none focus:bg-table-row-hover border-b"
    type="button"
    key={item.id}
    onClick={(e) => select(item, e)}
  >
    <div>{children}</div>
    {isSelected && <CheckIcon className="w-6 text-accent" />}
  </button>
);
