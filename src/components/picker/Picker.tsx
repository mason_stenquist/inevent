import {
  Button,
  Input,
  ModalBody,
  ModalFooter,
  Spinner,
} from "@chakra-ui/react";

import { useFuse, useFuseOptions, useRecordSelection } from "~/hooks";

interface RowProps<T> {
  item: T;
  select: (item: T, e: any) => void;
  isSelected: boolean;
}

interface Props<T> {
  closeModal: () => void;
  multiple?: boolean;
  data: T[];
  loading: boolean;
  fuseOptions: useFuseOptions<T>;
  children: (row: RowProps<T>) => JSX.Element;
  initSelected?: T[];
}

export const Picker = <T extends { id: string }>({
  closeModal,
  multiple = false,
  data = [],
  loading = true,
  fuseOptions,
  children,
  initSelected = [],
}: Props<T>) => {
  const [search, setSearch, items] = useFuse(data, fuseOptions);
  const checkboxes = useRecordSelection(items, "id", initSelected);
  console.log(checkboxes.selected);

  const cancel = () => {
    window.resolveModal({ cancel: true });
    closeModal();
  };

  const select = (item: T, e: any) => {
    if (multiple) {
      checkboxes.toggleSelected(item, e);
    } else {
      window.resolveModal({ cancel: false, results: item });
      closeModal();
    }
  };

  const done = () => {
    window.resolveModal({ cancel: false, results: checkboxes.selected });
    closeModal();
  };

  if (loading) return <Spinner />;

  return (
    <>
      <ModalBody pt="0">
        <div className="relative">
          <div className="sticky top-0 w-full z-10 flex space-x-3 pt-1 pb-2 bg-modal">
            <Input
              autoFocus
              className="bg-page "
              type="text"
              placeholder="Search..."
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            />
            {multiple && (
              <Button onClick={checkboxes.toggleAllSelected}>
                {checkboxes.hasAllSelected ? "Select none" : "Select all"}
              </Button>
            )}
          </div>
          <div className="">
            {items.map((item) => {
              return (
                <div key={item.id}>
                  {children({
                    item,
                    select,
                    isSelected: checkboxes.isSelected(item),
                  })}
                </div>
              );
            })}
          </div>
        </div>
      </ModalBody>
      <ModalFooter>
        <div className="space-x-3">
          <Button type="button" onClick={cancel}>
            Cancel
          </Button>
          {multiple && (
            <Button
              className="btn-primary"
              onClick={done}
              isDisabled={!checkboxes.hasAnySelected}
            >
              {!checkboxes.hasAnySelected
                ? "Select Something"
                : `Select ${checkboxes.selected.length} Items`}
            </Button>
          )}
        </div>
      </ModalFooter>
    </>
  );
};
