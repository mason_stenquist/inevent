import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Button,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
} from "@chakra-ui/react";
import { ReactNode, useMemo } from "react";
import { useMedia } from "react-use";

interface Section {
  label: ReactNode;
  element: JSX.Element;
}

interface Props {
  sections: Section[];
}

export const Sections: React.FC<Props> = ({ sections }) => {
  const isDesktop = useMedia("(min-width: 768px)");

  const tabs = useMemo(
    () => sections.map(({ label }, i) => <Tab key={i}>{label}</Tab>),
    [sections]
  );
  const tabPanels = useMemo(
    () =>
      sections.map(({ element }, i) => (
        <TabPanel key={i} px={0}>
          {element}
        </TabPanel>
      )),
    [sections]
  );

  const accordionPanels = useMemo(
    () =>
      sections.map(({ label, element }, i) => (
        <div key={i}>
          <AccordionItem
            className="card overflow-hidden p-0"
            mb={4}
            style={{ border: "none" }}
          >
            <AccordionButton
              className="relative"
              _expanded={{
                background: "rgb(var(--color-table-row-hover))",
                borderBottom: "1px solid rgb(var(--color-input-border))",
                color: "rgb(var(--color-accent))",
              }}
            >
              <div className="flex w-full">{label}</div>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel p={0}>
              <div className=" rounded-b-lg  bg-card p-4 ">{element}</div>
            </AccordionPanel>
          </AccordionItem>
        </div>
      )),
    [sections]
  );

  if (isDesktop) {
    return (
      <div className="card h-full">
        <Tabs>
          <TabList>{tabs}</TabList>
          {<TabPanels>{tabPanels}</TabPanels>}
        </Tabs>
      </div>
    );
  }

  return (
    <Accordion defaultIndex={[0]} allowMultiple>
      {accordionPanels}
    </Accordion>
  );
};
