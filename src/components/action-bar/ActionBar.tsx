import {
  Button,
  ButtonProps,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import { ChevronDownIcon } from "@heroicons/react/outline";
import React, {
  Children,
  ReactElement,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { useWindowSize } from "react-use";

type BooleanChild = false | Element;

interface Props {
  children: ReactElement | ReactElement[];
}

export const ActionBar: React.FC<Props> = ({ children }) => {
  const { width } = useWindowSize();
  const [breakPointConfig, setBreakPointConfig] = useState<[number, number][]>(
    []
  );
  const [toHide, setToHide] = useState(0);
  const ref = useRef<HTMLDivElement>(null);
  const actions = Children.toArray(children);

  const actionsToShow = useMemo(() => {
    return actions.slice(0, actions.length - toHide);
  }, [toHide]);

  const menuActions = useMemo(() => {
    return actions.slice(actions.length - toHide, actions.length);
  }, [toHide]);

  // Setup Breakpoint config
  useEffect(() => {
    if (!ref.current) return;
    const configArray: [number, number][] = [];
    const childArray = ref.current.childNodes;
    let widthAcc = 160;
    let hideCount = childArray.length;
    childArray.forEach((child) => {
      const el = child as HTMLElement;
      widthAcc += el.clientWidth;
      configArray.push([widthAcc, hideCount]);
      hideCount--;
    });
    setBreakPointConfig(configArray);
  }, [ref]);

  const getItemHideCount = (currentWidth: number) => {
    return (
      breakPointConfig.find(
        ([breakpointWidth]) => breakpointWidth > currentWidth
      )?.[1] ?? 0
    );
  };

  useEffect(() => {
    if (!ref.current) return;
    const amountToHide = getItemHideCount(ref.current.clientWidth);
    if (amountToHide !== toHide) {
      setToHide(amountToHide);
    }
  }, [width, breakPointConfig]);

  return (
    <div
      ref={ref}
      className="flex flex-grow space-x-3"
      style={{ width: breakPointConfig.length === 0 ? "5000px" : "unset" }}
    >
      {actionsToShow.map((action) => action)}
      {menuActions.length > 0 && (
        <Menu isLazy placement="bottom-end" matchWidth>
          <MenuButton
            as={Button}
            rightIcon={<ChevronDownIcon className="w-4" />}
            className="flex-grow sm:mr-5 sm:flex-none"
          >
            {actionsToShow.length === 0 ? t`actions` : t`more`}
          </MenuButton>
          <MenuList>
            {menuActions.map((action, index) => {
              const comp = action as any;
              const { children, leftIcon, className, onClick } = comp?.props;
              if (!children) return null;
              return (
                <MenuItem
                  key={index}
                  icon={leftIcon}
                  className={className}
                  onClick={onClick}
                >
                  {children}
                </MenuItem>
              );
            })}
          </MenuList>
        </Menu>
      )}
    </div>
  );
};
