import {
  IconButton,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuItem,
  MenuList,
  Tooltip,
} from "@chakra-ui/react";
import { DocumentReportIcon } from "@heroicons/react/outline";
import { Link } from "react-router-dom";

import { useRoutes } from "~/hooks";
import { Report } from "~/reports";

interface Props {
  reports: Report[];
}

export const ReportMenu: React.FC<Props> = ({ reports }) => {
  const { routes } = useRoutes();

  return (
    <Menu>
      <Tooltip label="Report Menu" hasArrow>
        <MenuButton
          as={IconButton}
          icon={<DocumentReportIcon className="h-6 w-6" />}
        ></MenuButton>
      </Tooltip>
      <MenuList>
        <MenuGroup title="PDF/Print Reports">
          {reports.map(({ name, path }) => (
            <MenuItem
              key={path}
              as={Link}
              to={routes.reportPreview.url({ path })}
            >
              {name}
            </MenuItem>
          ))}
        </MenuGroup>
        <MenuDivider />
        <MenuGroup title="Excel">
          <MenuItem>Guest List</MenuItem>
        </MenuGroup>
      </MenuList>
    </Menu>
  );
};
