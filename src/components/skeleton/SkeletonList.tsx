import { Spinner } from "@chakra-ui/react";

export const SkeletonList = () => {
  return (
    <div>
      <div className="mb-4 flex w-full animate-pulse justify-between">
        <div className="flex space-x-3">
          <div className="h-10 w-24 animate-pulse rounded-lg bg-card"></div>
          <div className="h-10 w-10 animate-pulse rounded-lg bg-card"></div>
        </div>
        <div className="flex space-x-3">
          <div className="h-10 w-56 animate-pulse rounded-lg bg-card"></div>
          <div className="h-10 w-12 animate-pulse rounded-lg bg-card"></div>
        </div>
      </div>

      <div className="card py-0.5 px-1.5 mb-4 flex h-12 w-full animate-pulse justify-between">
        <div className="hidden md:flex items-center space-x-3">
          <div className="flex items-center space-x-1.5">
            <div className="w-[32px] h-[32px] animate-pulse rounded-full bg-page"></div>
            <div className="h-5 w-32 animate-pulse rounded bg-page"></div>
          </div>
          <div className="flex items-center space-x-1.5">
            <div className="w-[32px] h-[32px] animate-pulse rounded-full bg-page"></div>
            <div className="h-5 w-28 animate-pulse rounded bg-page"></div>
          </div>
          <div className="flex items-center space-x-1.5">
            <div className="w-[32px] h-[32px] animate-pulse rounded-full bg-page"></div>
            <div className="h-5 w-32 animate-pulse rounded bg-page"></div>
          </div>
        </div>
      </div>

      <div className="card h-screen w-full animate-pulse">
        <div className="hidden md:flex justify-between space-x-3">
          <div className="h-10 w-56 flex-grow animate-pulse rounded-lg bg-page"></div>
          <div className="h-10 w-40 flex-grow animate-pulse rounded-lg bg-page"></div>
          <div className="h-10 w-12 flex-grow animate-pulse rounded-lg bg-page"></div>
          <div className="h-10 w-40 flex-grow animate-pulse rounded-lg bg-page"></div>
          <div className="h-10 w-12 flex-grow animate-pulse rounded-lg bg-page"></div>
          <div className="h-10 w-56 flex-grow animate-pulse rounded-lg bg-page"></div>
        </div>
        <div className="flex-center h-[50vh]">
          <Spinner size="xl" />
        </div>
      </div>
    </div>
  );
};
