import { useMemo, useState } from "react";

import { WizardStep, useWizard } from "../wizard";
import { ImportModel } from "./ImporterDropdown";
import { ImporterMapFields } from "./ImporterMapFields";
import { ImporterProcessImport } from "./ImporterProcessImport";
import { ImporterSelectFile } from "./ImporterSelectFile";
import { ImporterSettings } from "./ImporterSettings";
import { ImporterType } from "./ImporterType";
import { SchemaModel } from "@aws-amplify/datastore";
import { ParseError } from "papaparse";
import { Model } from "~/@types/aws/model";
import { MobileTitle } from "~/components";
import { useRecordSelection, useRoutes } from "~/hooks";
import { getSchema } from "~/utils";

interface Props {
  model: Model;
}

export type RowData = Record<string, string | number> & {
  internalIndex: number;
};

export enum ImportType {
  Add = "add",
  Update = "update",
}

export interface ImportSettings {
  type: ImportType | undefined;
  addRemaining: boolean;
}

const initModelFields = (schemaModel: SchemaModel) =>
  Object.values(schemaModel.fields).filter(({ name, isReadOnly, isArray }) => {
    if (isReadOnly) return false;
    if (isArray) return false;
    if (name.startsWith("_")) return false;
    if (name === "group") return false;
    return true;
  });

export const Importer = ({ model }: Props) => {
  const { Wizard, next } = useWizard(5);
  const { routes } = useRoutes();
  const [dataHeaders, setDataHeaders] = useState<string[]>([]);
  const [importData, setImportData] = useState<RowData[]>([]);
  const [errors, setErrors] = useState<ParseError[]>([]);
  const [modelFields, setModelFields] = useState<ImportModel[]>(
    initModelFields(getSchema(model))
  );
  const [importSettings, setImportSettings] = useState<ImportSettings>({
    type: undefined,
    addRemaining: false,
  });

  const checkboxes = useRecordSelection(importData, "internalIndex");

  const allRequiredMapped = useMemo(() => {
    return !modelFields.some(
      ({ isRequired, mappedHeader, type }) =>
        isRequired && !mappedHeader && type !== "ID"
    );
  }, [modelFields]);

  const mappedFields = useMemo(() => {
    return modelFields.filter(({ mappedHeader }) => !!mappedHeader);
  }, [modelFields]);

  return (
    <div className="">
      <Wizard>
        <WizardStep title="Step 1" subtitle="Select an import file">
          <ImporterSelectFile
            modelName={model.name}
            modelFields={modelFields}
            setModelFields={setModelFields}
            setDataHeaders={setDataHeaders}
            setImportData={setImportData}
            setErrors={setErrors}
            checkboxes={checkboxes}
            next={next}
          />
        </WizardStep>
        <WizardStep title="Step 2" subtitle="Import Type">
          <ImporterType
            importSettings={importSettings}
            setImportSettings={setImportSettings}
            next={next}
          />
        </WizardStep>
        <WizardStep title="Step 3" subtitle="Map table headers">
          <ImporterMapFields
            dataHeaders={dataHeaders}
            importData={importData}
            errors={errors}
            modelFields={modelFields}
            setModelFields={setModelFields}
            checkboxes={checkboxes}
            importSettings={importSettings}
            allRequiredMapped={allRequiredMapped}
            mappedFields={mappedFields}
            next={next}
          />
        </WizardStep>
        <WizardStep title="Step 4" subtitle="Import settings and review">
          <ImporterSettings
            modelFields={modelFields}
            setModelFields={setModelFields}
            importSettings={importSettings}
            checkboxes={checkboxes}
            next={next}
          />
        </WizardStep>
        <WizardStep title="Step 5" subtitle="Import summary">
          <ImporterProcessImport
            model={model}
            checkboxes={checkboxes}
            modelFields={modelFields}
            importSettings={importSettings}
          />
        </WizardStep>
      </Wizard>
      <MobileTitle
        leftButtonText={routes.guestIndex.label}
        leftButtonAction={() => routes.guestIndex.go()}
      />
    </div>
  );
};
