import { Button, Checkbox, Radio, RadioGroup } from "@chakra-ui/react";
import { Dispatch, useMemo, useState } from "react";

import { ImportSettings, ImportType, RowData } from "./Importer";
import { ImportModel } from "./ImporterDropdown";
import { ImporterSettingsMappingRow } from "./ImporterSettingsMappingRow";
import { UseRecordSelection, useDialogue } from "~/hooks";

interface Props {
  modelFields: ImportModel[];
  setModelFields: Dispatch<React.SetStateAction<ImportModel[]>>;
  importSettings: ImportSettings;
  checkboxes: UseRecordSelection<RowData>;
  next: () => void;
}

export const ImporterSettings: React.FC<Props> = ({
  modelFields,
  setModelFields,
  importSettings,
  checkboxes,
  next,
}) => {
  const { showDialogue } = useDialogue();
  const mappedFields = modelFields.filter((field) => !!field.mappedHeader);

  const hasAtLeastOneMatchField = useMemo(
    () => modelFields.some(({ isMatchField }) => isMatchField),
    [modelFields]
  );

  const preventNextErrorMessage = useMemo(() => {
    if (importSettings.type === ImportType.Update && !hasAtLeastOneMatchField) {
      return "You must have select at least one match field before proceeding";
    }

    return null;
  }, [importSettings, mappedFields]);

  const handleNextClick = async () => {
    if (preventNextErrorMessage) {
      await showDialogue({
        title: "Import",
        body: preventNextErrorMessage,
        showCancel: false,
        buttons: [{ label: "OK", value: "ok" }],
      });
      return;
    }
    console.log("NEXT");
    next();
  };

  return (
    <div className="card mt-4">
      <div className="md:flex justify-between">
        <div className="flex-grow pb-2">
          <h2 className="font-semibold text-lg">Import settings and review</h2>
        </div>
        <div>
          <Button
            opacity={preventNextErrorMessage ? "0.4" : "1"}
            onClick={handleNextClick}
            className="btn-primary"
          >
            Import {checkboxes.selected.length} Records
          </Button>
        </div>
      </div>

      <div className="">
        <div className="">
          <p className="font-bold mt-2">Mapping Review</p>
          <p className="text-muted-text text-sm mb-2">
            {importSettings.type === ImportType.Update
              ? t`reviewUpdateHint`
              : t`reviewAddHint`}
          </p>
          <div className="rounded-lg overflow-hidden border p-0 bg-modal">
            <table className="text-sm w-full">
              <thead>
                <tr className="bg-modal">
                  <th className="px-2 py-2">Source Field</th>
                  <th className="px-2 py-2"></th>
                  <th className="px-2 py-2">Target Field</th>
                </tr>
              </thead>
              <tbody>
                {mappedFields.map((field) => (
                  <ImporterSettingsMappingRow
                    key={field.name}
                    field={field}
                    setModelFields={setModelFields}
                    importType={importSettings.type}
                  />
                ))}
              </tbody>
            </table>
          </div>
          {importSettings.type === ImportType.Update && (
            <div className="text-sm pt-2 italic text-muted-text">
              {!importSettings.addRemaining && (
                <p>
                  * If an exact match is not found, the record will be ignored
                </p>
              )}
              {importSettings.addRemaining && (
                <p>
                  * If an exact match is not found, the record will be added as
                  a new record
                </p>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
