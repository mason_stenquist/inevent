import { useMemo } from "react";

import { ImportSettings, ImportType } from "./Importer";
import { ModelField } from "@aws-amplify/datastore";
import { useDialogue } from "~/hooks";

export type ImportModel = ModelField & {
  mappedHeader?: string;
  isMatchField?: boolean;
};

interface Props {
  dataHeader: string;
  modelFields: ImportModel[];
  importSettings: ImportSettings;
  setModelFields: (modelFields: ImportModel[]) => void;
}

export const ImportDropdown: React.FC<Props> = ({
  dataHeader,
  modelFields,
  importSettings,
  setModelFields,
}) => {
  const { showDialogue } = useDialogue();

  const shouldShowReqFlag = (field: ImportModel) => {
    if (
      importSettings.type === ImportType.Update &&
      !importSettings.addRemaining
    )
      return false;
    if (field.type === "ID") return false;
    return field.isRequired;
  };

  const currentModelField = modelFields.find(
    (model) => model.mappedHeader === dataHeader
  );

  const filteredModels = modelFields.filter((field) => {
    return field.mappedHeader === dataHeader || !field?.mappedHeader;
  });

  const updateImportModel = async (value: string) => {
    if (value === "id") {
      const add = await showDialogue({
        title: "Import ID",
        body: "You normally shouldn't map the ID field as it will be populated automatically. Only do so if you know what you are doing.",
        showCancel: true,
        buttons: [{ value: "continue", label: "Continue" }],
      });
      if (add === "cancel") return;
    }

    const existingMappedRemoved = modelFields.map((field) => {
      if (field.mappedHeader === dataHeader)
        return { ...field, mappedHeader: undefined };
      return field;
    });

    const newMappedAdded = existingMappedRemoved.map((field) => {
      if (field.name === value) return { ...field, mappedHeader: dataHeader };
      return field;
    });

    setModelFields(newMappedAdded);
  };

  return (
    <select
      value={currentModelField?.name ?? ""}
      onChange={(e) => updateImportModel(e.target.value)}
      className={`border-l-0 border-r-0 border-t-0 border-b-4 static rounded-none pl-2 h-8 text-sm font-normal ${
        currentModelField?.name ? "border-green-500" : "border-warning"
      }`}
    >
      <option>Don't Import</option>
      {filteredModels.map((field) => (
        <option key={field.name} value={field.name}>
          {t(field.name)} {shouldShowReqFlag(field) && " (req)"}
        </option>
      ))}
    </select>
  );
};
