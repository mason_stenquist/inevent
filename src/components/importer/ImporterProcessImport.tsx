import { Spinner } from "@chakra-ui/react";
import { DataStore } from "aws-amplify";
import { useEffect, useState } from "react";

import { ImportSettings, RowData } from "./Importer";
import { ImportModel } from "./ImporterDropdown";
import { Model } from "~/@types/aws/model";
import { UseRecordSelection } from "~/hooks";
import { newModel } from "~/utils";

interface Props {
  model: Model;
  checkboxes: UseRecordSelection<RowData>;
  modelFields: ImportModel[];
  importSettings: ImportSettings;
}
export const ImporterProcessImport: React.FC<Props> = ({
  model,
  checkboxes,
  modelFields,
  importSettings,
}) => {
  const [isProcessing, setIsProcessing] = useState(false);
  const [errors, setErrors] = useState<{ rowNumber: number; error: string }[]>(
    []
  );
  const [successCount, setSuccessCount] = useState(0);

  const mappedFields = modelFields.filter(({ mappedHeader }) =>
    Boolean(mappedHeader)
  );

  const process = async () => {
    setIsProcessing(true);
    setErrors([]);
    let rowNumber = 2;
    let successCounter = 0;
    for (const data of checkboxes.selected) {
      const fieldData = mappedFields.reduce(
        (acc, field) => ({
          ...acc,
          [field.name]: data[field.mappedHeader ?? ""],
        }),
        {}
      );

      let record;
      try {
        record = newModel(model, fieldData as any);
        DataStore.save(record);
        successCounter = successCounter + 1;
      } catch (e: any) {
        const currentRow = rowNumber;
        setErrors((p) => [
          ...p,
          {
            rowNumber: currentRow,
            error: e.message ?? "Unknown error creating record",
          },
        ]);
      }
      rowNumber = rowNumber + 1;
    }
    setSuccessCount(successCounter);
    setIsProcessing(false);
  };

  useEffect(() => {
    process();
  }, []);

  if (isProcessing) {
    return (
      <div>
        <Spinner size="xl" />
        <div>Processing</div>
      </div>
    );
  }

  return (
    <div>
      <button onClick={process}>process</button>
      <div className="card mt-4">
        <h2 className="font-semibold text-lg">Import Summary</h2>
        <div>
          Records successfully created: <strong>{successCount}</strong>
        </div>
        <div>
          Errors: <strong>{errors.length}</strong>
        </div>
        {errors.length > 0 && (
          <table className="w-full mt-4">
            <thead>
              <tr>
                <th className="bg-modal px-2 border text-center w-20">Row</th>
                <th className="bg-modal px-2 border text-left">Error</th>
              </tr>
            </thead>
            <tbody>
              {errors.map(({ rowNumber, error }) => (
                <tr key={rowNumber}>
                  <td className="border px-2 text-sm bg-danger/10 text-center ">
                    {rowNumber}
                  </td>
                  <td className="border px-2 text-sm bg-danger/10">{error}</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    </div>
  );
};
