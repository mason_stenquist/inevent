import {
  Button,
  ButtonGroup,
  Checkbox,
  Radio,
  RadioGroup,
} from "@chakra-ui/react";
import { ArrowRightIcon, SelectorIcon } from "@heroicons/react/outline";
import { Dispatch, useMemo, useState } from "react";

import { ImportType } from "./Importer";
import { ImportModel } from "./ImporterDropdown";

interface Props {
  field: ImportModel;
  setModelFields: Dispatch<React.SetStateAction<ImportModel[]>>;
  importType: ImportType | undefined;
}

export const ImporterSettingsMappingRow: React.FC<Props> = ({
  field,
  setModelFields,
  importType,
}) => {
  const { name, mappedHeader, isMatchField } = field;

  const toggleMatch = () => {
    setModelFields((p) =>
      p.map((modelField) => {
        if (modelField.name === name) {
          return {
            ...modelField,
            isMatchField: Boolean(!modelField.isMatchField),
          };
        }
        return modelField;
      })
    );
  };

  const button = useMemo(() => {
    if (importType === ImportType.Add) {
      return (
        <div className="rounded-none w-full flex justify-center md:space-x-3 px-3 py-1.5">
          <span className="hidden md:block">Import data</span>
          <ArrowRightIcon className="w-4" />
        </div>
      );
    }
    if (isMatchField) {
      return (
        <Button
          onClick={toggleMatch}
          size="sm"
          colorScheme="blue"
          className="rounded-none w-full md:space-x-3 px-3"
          title="Toggle to import data"
        >
          <span className="hidden md:block">Match field</span>
          <SelectorIcon className="w-5 rotate-90" />
        </Button>
      );
    }
    if (!isMatchField) {
      return (
        <Button
          onClick={toggleMatch}
          size="sm"
          colorScheme="green"
          className="rounded-none w-full md:space-x-3 px-3"
          title="Toggle to match field"
        >
          <span className="hidden md:block">Import data</span>
          <ArrowRightIcon className="w-4" />
        </Button>
      );
    }
  }, [isMatchField, importType]);

  return (
    <tr className="">
      <td className="border px-2 bg-page">
        <span>{mappedHeader}</span>
      </td>
      <td className="border p-0 md:w-32">{button}</td>
      <td className="border px-2 bg-page">{t(name)}</td>
    </tr>
  );
};
