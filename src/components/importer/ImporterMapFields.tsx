import { Badge, Button, Checkbox } from "@chakra-ui/react";
import { ArrowRightIcon, ExclamationIcon } from "@heroicons/react/outline";
import { Dispatch, useMemo, useState } from "react";

import { ImportSettings, ImportType, RowData } from "./Importer";
import { ImportDropdown, ImportModel } from "./ImporterDropdown";
import { ModelFields } from "@aws-amplify/datastore";
import { ParseError } from "papaparse";
import { UseRecordSelection, useDialogue, useRecordSelection } from "~/hooks";

interface Props {
  dataHeaders: string[];
  importData: RowData[];
  errors: ParseError[];
  modelFields: ImportModel[];
  setModelFields: Dispatch<React.SetStateAction<ImportModel[]>>;
  checkboxes: UseRecordSelection<RowData>;
  importSettings: ImportSettings;
  allRequiredMapped: boolean;
  mappedFields: ImportModel[];
  next: () => void;
}

export const ImporterMapFields: React.FC<Props> = ({
  dataHeaders,
  importData,
  errors,
  modelFields,
  setModelFields,
  checkboxes,
  importSettings,
  allRequiredMapped,
  mappedFields,
  next,
}) => {
  const { showDialogue } = useDialogue();
  const [showAll, setShowAll] = useState(false);

  const filteredImportData = useMemo(() => {
    return importData.slice(0, showAll ? importData.length : 5);
  }, [showAll, importData]);

  const preventNextErrorMessage = useMemo(() => {
    if (importSettings.type === ImportType.Add && !allRequiredMapped) {
      return 'When doing an "Add" import type, you must map all required fields';
    }
    if (importSettings.type === ImportType.Add && mappedFields.length === 0) {
      return "You must have at least 1 field mapped to do an import";
    }
    if (importSettings.addRemaining && !allRequiredMapped) {
      return 'When "import remaining data" is checked, you must map all required fields';
    }
    if (importSettings.type === ImportType.Update && mappedFields.length < 2) {
      return "You must have at least 2 fields mapped to do an update import";
    }
    return null;
  }, [importSettings, allRequiredMapped, mappedFields]);

  const requiredFields = modelFields.filter(
    ({ isRequired, type }) => isRequired && type !== "ID"
  );

  const handleNextClick = async () => {
    if (preventNextErrorMessage) {
      await showDialogue({
        title: "Import",
        body: preventNextErrorMessage,
        showCancel: false,
        buttons: [{ label: "OK", value: "ok" }],
      });
      return;
    }
    next();
  };

  const getBadge = (field: ImportModel) => {
    if (field.mappedHeader)
      return (
        <Badge className="text-center" colorScheme="green">
          {t(field.name)} <ArrowRightIcon className="inline w-3 pb-0.5" />{" "}
          {field.mappedHeader}
        </Badge>
      );
    return (
      <Badge className="text-center" colorScheme="red">
        <span>
          {t(field.name)} <ArrowRightIcon className="inline w-3 pb-0.5" /> Not
          Mapped
        </span>
      </Badge>
    );
  };

  return (
    <div className="card px-0 pb-0 mt-4">
      <div className="px-4 pb-4 lg:flex justify-between">
        <div className="flex-grow pb-2">
          <h2 className="font-semibold text-lg">
            Use the column headers to match your rows up with the inEvent
            manager fields
          </h2>
          <p className="text-muted-text text-sm">
            Deslect any rows you don't wish to import
          </p>
        </div>

        <div>
          <div className="flex justify-end pb-2">
            <Button
              opacity={preventNextErrorMessage ? "0.4" : "1"}
              className="w-full md:w-auto btn-primary"
              onClick={handleNextClick}
            >
              Next
            </Button>
          </div>

          {(importSettings.type === ImportType.Add ||
            importSettings.addRemaining) && (
            <div className="pb-3 lg:text-right">
              <p className="font-bold">Required field mappings</p>
              <div className="pt-2">
                <div className="">
                  {requiredFields.map((field) => (
                    <div key={field.name}>{getBadge(field)}</div>
                  ))}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>

      <div className="overflow-x-auto flip">
        <table
          className={`border-t w-full flip ${
            importData.length === 0 ? "hidden" : ""
          }`}
        >
          <thead>
            <tr className="bg-modal">
              <th></th>
              {dataHeaders.map((header) => (
                <th key={header} className="text-left p-0 pt-2 ">
                  <p
                    className="pl-2 pr-2 text-sm font-semibold h-8 truncate min-w-[150px] max-w-[200px]"
                    title={header}
                  >
                    {header}
                  </p>
                </th>
              ))}
            </tr>
          </thead>
          <tbody className="">
            <tr className="">
              <td className="border-y p-0 sticky left-0 bg-page">
                <div className="border-x h-8 px-2  flex items-center justify-center ">
                  <Checkbox
                    isChecked={checkboxes.hasAllSelected}
                    isIndeterminate={checkboxes.hasSomeSelected}
                    onChange={checkboxes.toggleAllSelected}
                    size="lg"
                    className="mt-[4px]"
                    title={t`selectAll`}
                  />
                </div>
              </td>

              {dataHeaders.map((dataHeader) => (
                <td className="border text-left text-sm p-0">
                  <ImportDropdown
                    dataHeader={dataHeader}
                    modelFields={modelFields}
                    setModelFields={setModelFields}
                    importSettings={importSettings}
                  />
                </td>
              ))}
            </tr>
            {filteredImportData.map((row, i) => {
              const error = errors.find((e) => e.row === i);
              return (
                <tr
                  className={`hover:bg-table-row-hover ${
                    error ? "bg-red-600/10" : " "
                  }`}
                >
                  <td className="p-0 sticky left-0 bg-card border-b w-[40px]">
                    <div className=" border-x h-7 px-2  flex items-center justify-center">
                      {error && (
                        <div title={error.message}>
                          <ExclamationIcon className="w-4 mt-[2px] text-rose-600" />
                        </div>
                      )}
                      {!error && (
                        <Checkbox
                          className="mt-[3px]"
                          isChecked={checkboxes.isSelected(row)}
                          onChange={(e) =>
                            checkboxes.toggleSelected(row, e.nativeEvent)
                          }
                        />
                      )}
                    </div>
                  </td>
                  {dataHeaders.map((field) => (
                    <td className="px-1 text-xs border">
                      <p
                        className="h-6 truncate max-w-[200px]"
                        title={row[field] as string}
                      >
                        {row[field]}
                      </p>
                    </td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="p-4 space-x-3 flex items-center">
        <Button onClick={() => setShowAll((p) => !p)}>
          {showAll ? "Show less" : "Show all"}
        </Button>
        <span>
          Viewing {filteredImportData.length} of {importData.length}
        </span>
      </div>
    </div>
  );
};
