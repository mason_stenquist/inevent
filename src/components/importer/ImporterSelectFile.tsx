import { Badge, Button, Spinner } from "@chakra-ui/react";
import { DownloadIcon } from "@heroicons/react/outline";
import { ChangeEvent, Dispatch, DragEvent, useState } from "react";
import { Link } from "react-router-dom";

import { RowData } from "./Importer";
import { ImportModel } from "./ImporterDropdown";
import Papa, { ParseError } from "papaparse";
import { UseRecordSelection, useDialogue, useRecordSelection } from "~/hooks";

interface Props {
  modelName: string;
  modelFields: ImportModel[];
  setModelFields: Dispatch<React.SetStateAction<ImportModel[]>>;
  setDataHeaders: Dispatch<React.SetStateAction<string[]>>;
  setImportData: Dispatch<React.SetStateAction<RowData[]>>;
  setErrors: Dispatch<React.SetStateAction<ParseError[]>>;
  checkboxes: UseRecordSelection<RowData>;
  next: () => void;
}

export const ImporterSelectFile: React.FC<Props> = ({
  modelFields,
  setModelFields,
  modelName,
  setDataHeaders,
  setImportData,
  setErrors,
  checkboxes,
  next,
}) => {
  const [isProcessing, setIsProcessing] = useState(false);

  const handleFile = <T extends {}>(file?: File | null) => {
    if (!file) return;
    setIsProcessing(true);

    Papa.parse<T>(file, {
      quoteChar: '"',
      escapeChar: '"',
      header: true,
      transformHeader: undefined,
      dynamicTyping: false,
      preview: 0,
      encoding: "",
      worker: false,
      comments: false,
      skipEmptyLines: true,
      complete: handleParseComplete,
    });
  };

  const handleParseComplete = <T extends {}>(results: Papa.ParseResult<T>) => {
    const headers = results.meta.fields ?? [];
    setDataHeaders(headers);
    setErrors(results.errors);
    const records = results.data.map((row, index) => ({
      ...row,
      internalIndex: index,
    }));

    console.log(records);
    const updatedModelFields = modelFields.map((field) => ({
      ...field,
      mappedHeader: headers.includes(field.name) ? field.name : undefined,
    }));
    setModelFields(updatedModelFields);
    setImportData(records);
    checkboxes.setSelected(records);
    setIsProcessing(false);
    next();
  };

  const csvObject = modelFields
    .filter((field) => field.type !== "ID")
    .reduce((acc, field) => ({ ...acc, [field.name]: "" }), {});
  const csv = Papa.unparse([csvObject]);
  var csvData = new Blob([csv], { type: "text/csv;charset=utf-8;" });
  var csvURL = window.URL.createObjectURL(csvData);

  return (
    <div className="card mt-4">
      <h2 className="font-semibold text-lg">
        Select the file you wish to import
      </h2>

      <div className=" flex-center">
        <div className="py-24 text-center">
          {!isProcessing && (
            <div className="text-center">
              <div className="flex justify-center flex-col lg:flex-row items-center">
                <input
                  accept=".csv"
                  type="file"
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    handleFile(e.target?.files?.[0])
                  }
                  onDrop={(e: DragEvent<HTMLInputElement>) =>
                    handleFile(e.dataTransfer.items?.[0].getAsFile())
                  }
                  className="block text-sm text-muted
                file:mr-4 file:py-2 file:px-4
                file:rounded-lg file:border-0
                file:text-sm file:font-semibold file:text-white
                file:bg-accent"
                />
                <p className="py-4 pr-10">or</p>
                <Button
                  as="a"
                  href={csvURL}
                  download={`${modelName}-upload-template`}
                  size="sm"
                  className=""
                  rightIcon={<DownloadIcon className="w-5" />}
                >
                  Download Template File
                </Button>
              </div>
              <p className="text-muted-text text-sm mt-8">
                Name your column headers the following to have them
                automatically mapped
              </p>
              <div className="flex justify-center pt-2 flex-wrap">
                {modelFields.map((field) => (
                  <div key={field.name} className="p-1">
                    <div className="bg-page px-2 rounded border">
                      {field.name}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          )}

          {isProcessing && (
            <div>
              <Spinner size="xl" />
              <div>Processing</div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
