import { Button, Checkbox, Radio, RadioGroup } from "@chakra-ui/react";
import { Dispatch } from "react";

import { ImportSettings, ImportType, RowData } from "./Importer";
import { ImporterSettingsMappingRow } from "./ImporterSettingsMappingRow";
import { useDialogue } from "~/hooks";

interface Props {
  importSettings: ImportSettings;
  setImportSettings: Dispatch<React.SetStateAction<ImportSettings>>;
  next: () => void;
}

export const ImporterType: React.FC<Props> = ({
  importSettings,
  setImportSettings,
  next,
}) => {
  const { showDialogue } = useDialogue();

  const handleNextClick = async () => {
    if (!importSettings.type) {
      await showDialogue({
        title: "Import",
        body: "Before you can continue please select an import type",
        showCancel: false,
        buttons: [{ label: "OK", value: "ok" }],
      });
      return;
    }
    next();
  };

  return (
    <div className="card mt-4">
      <div className="md:flex justify-between">
        <div className="flex-grow pb-2">
          <h2 className="font-semibold text-lg">Import Type</h2>
          <p className="text-muted-text text-sm">
            Setup what type of import you want to do
          </p>
        </div>
        <div>
          <Button
            opacity={!importSettings.type ? "0.4" : "1"}
            onClick={handleNextClick}
            className="btn-primary"
          >
            Next
          </Button>
        </div>
      </div>

      <div className="">
        <RadioGroup
          value={importSettings.type}
          onChange={(value: ImportType) =>
            setImportSettings((p) => ({ ...p, type: value }))
          }
        >
          <div className="flex flex-col space-y-2">
            <Radio value={ImportType.Add}>
              <div>
                <p className="font-semibold">Add</p>
                <p className="text-muted-text text-sm">
                  Add records from spreadsheet as new records
                </p>
              </div>
            </Radio>
            <Radio value={ImportType.Update}>
              <div>
                <p className="font-semibold">Update</p>
                <p className="text-muted-text text-sm">
                  Update the existing records when a matching record can be
                  found
                </p>
              </div>
            </Radio>
          </div>
        </RadioGroup>
        <Checkbox
          isChecked={importSettings.addRemaining}
          onChange={(e) =>
            setImportSettings((old) => ({
              ...old,
              addRemaining: Boolean(!old.addRemaining),
            }))
          }
          className="ml-6 mt-1"
          size="sm"
          isDisabled={importSettings.type !== ImportType.Update}
        >
          Add remaining data as new records?
        </Checkbox>
      </div>
    </div>
  );
};
