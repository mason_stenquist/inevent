export default {
  en: {
    reviewUpdateHint:
      "Click the middle button to toggle which fields should be matched on when updating records",
    reviewAddHint: "Please ensure that all mappings look correct",
  },
  es: {
    reviewUpdateHint:
      "Haga clic en el botón central para alternar en qué campos se deben coincidir cuando se actualiza los registros",
    reviewAddHint:
      "Por favor, asegúrese de que todas las asignaciones se vean correctas",
  },
};
