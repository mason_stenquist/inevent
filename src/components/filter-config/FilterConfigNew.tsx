import { Button, ModalBody, ModalFooter } from "@chakra-ui/react";
import { DataStore } from "aws-amplify";
import { Form, Formik } from "formik";

import { Input } from "../formik-fields";
import { Filter } from "~/models";

interface Props {
  modelName: string;
  filter: string;
  closeModal: () => void;
}
export const FilterConfigNew: React.FC<Props> = ({
  modelName,
  filter,
  closeModal,
}) => {
  return (
    <Formik
      initialValues={{ name: "", modelName, filter }}
      onSubmit={(values) => {
        DataStore.save(new Filter(values));
        closeModal();
      }}
    >
      <Form>
        <ModalBody>
          <Input label="Quick Filter Name" name="name" />
        </ModalBody>
        <ModalFooter className="space-x-3">
          <Button onClick={closeModal}>Cancel</Button>
          <Button type="submit" className="btn-primary">
            Save
          </Button>
        </ModalFooter>
      </Form>
    </Formik>
  );
};
