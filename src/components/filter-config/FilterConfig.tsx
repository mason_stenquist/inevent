import { Button, IconButton, Spinner } from "@chakra-ui/react";
import { TrashIcon } from "@heroicons/react/outline";
import { DataStore } from "aws-amplify";
import { useSearchParams } from "react-router-dom";

import { FilterConfigNew } from "..";
import { useQuery } from "~/datastore";
import { useDialogue, useModal } from "~/hooks";
import { Filter } from "~/models";
import { serializeData } from "~/utils";

interface Props {
  modelName: string;
}

export const FilterConfig: React.FC<Props> = ({ modelName }) => {
  const { showDialogue } = useDialogue();
  const { data: filters, loading } = useQuery<Filter>({
    model: Filter,
    query: (q) => q.modelName("eq", modelName),
  });
  const { showModal, closeModal } = useModal();
  const [searchParams] = useSearchParams();
  const searchObject = serializeData(searchParams);

  const searchArray = Object.entries(searchObject).filter(
    ([key]) => !key.endsWith("Predicate")
  );

  const addFilterConfig = () => {
    showModal({
      title: "Save Quick Filter",
      body: (
        <FilterConfigNew
          modelName={modelName}
          filter={JSON.stringify(searchObject)}
          closeModal={closeModal}
        />
      ),
    });
  };

  const deleteFilterConfig = async (filter: Filter) => {
    const confirm = await showDialogue({
      title: "Delete Quick Filter",
      body: `Are you sure you want to delete "${filter.name}"?`,
      showCancel: true,
      buttons: [{ label: "Delete", value: "delete" }],
    });
    if (confirm === "delete") {
      DataStore.delete(filter);
    }
  };

  if (loading)
    return (
      <div className="py-2 flex-center">
        <Spinner />
      </div>
    );

  return (
    <div>
      {searchArray.length > 0 && (
        <div>
          <p className="font-bold">Current Filter Criteria</p>
          <div className="card p-2 my-2">
            {searchArray.map(([key, value]) => (
              <div key={key} className="text-sm">
                <span>{t(key)}</span>:{" "}
                <span className="text-muted-text">
                  {searchObject[`${key}Predicate`]}
                </span>{" "}
                {value}
              </div>
            ))}
          </div>
          <div className="py-2">
            <Button onClick={addFilterConfig} className="w-full">
              Save as Quick Filter
            </Button>
          </div>
        </div>
      )}
      <p className="font-bold pt-2 pb-1">Existing Quick Filters</p>
      {filters.map((filter) => (
        <div key={filter.id} className="py-1">
          <Button
            justifyContent="start"
            className="w-full hover:bg-danger/25"
            aria-label="Delete Quick Filter"
            size="md"
            leftIcon={<TrashIcon className="w-4" />}
            onClick={() => deleteFilterConfig(filter)}
          >
            {filter.name}
          </Button>
        </div>
      ))}
    </div>
  );
};
