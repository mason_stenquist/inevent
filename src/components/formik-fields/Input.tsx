import {
  Input as ChakraInput,
  InputGroup,
  InputLeftAddon,
  InputRightAddon,
} from "@chakra-ui/react";
import { BanIcon } from "@heroicons/react/outline";
import { Field, FieldAttributes } from "formik";
import { InputHTMLAttributes, ReactElement } from "react";

interface Props
  extends Omit<InputHTMLAttributes<HTMLInputElement>, "name" | "type"> {
  name: string;
  label?: string;
  type?: string;
  optional?: boolean;
  leftAddon?: ReactElement;
  rightAddon?: ReactElement;
}

export const Input: React.FC<Props> = ({
  name,
  label,
  type,
  optional,
  leftAddon,
  rightAddon,
  ...rest
}) => {
  return (
    <Field name={name}>
      {({ field, form: { touched, errors } }: FieldAttributes<any>) => {
        const displayError = touched[name] && errors[name];
        return (
          <div className="">
            {label && (
              <label htmlFor={name} className="left-0 text-sm">
                {label}{" "}
                {optional && (
                  <span className="text-xs opacity-60">(Optional)</span>
                )}
              </label>
            )}
            <InputGroup>
              {leftAddon && <InputLeftAddon p={0}>{leftAddon}</InputLeftAddon>}
              <ChakraInput
                type={type ?? "text"}
                id={name}
                isInvalid={displayError}
                {...field}
                {...rest}
              />
              {rightAddon && (
                <InputRightAddon p={0}>{rightAddon}</InputRightAddon>
              )}
            </InputGroup>
            <div className="h-5 text-right">
              {displayError && (
                <div className="text-sm text-input-error">{errors[name]}</div>
              )}
            </div>
          </div>
        );
      }}
    </Field>
  );
};
