import {
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuItemOption,
  MenuList,
  MenuOptionGroup,
} from "@chakra-ui/react";
import { useField } from "formik";

interface Props {
  type: "string" | "number" | "list";
  name: string;
}

type Predicate = keyof typeof predicateMap;

const typeMap: Record<string, Array<Predicate>> = {
  string: ["eq", "ne", "contains", "notContains", "beginsWith"],
  number: ["eq", "ne", "le", "lt", "ge", "gt", "between"],
  list: ["contains", "notContains"],
};

const predicateMap = {
  eq: "Equals",
  ne: "Does not equal",
  contains: "Contains",
  notContains: "Does not contain",
  beginsWith: "Begins with",
  le: "Less than or equal",
  lt: "Less than",
  ge: "Greater than or equal",
  gt: "Greater than",
  between: "Between",
};

export const PredicateFilterSelect: React.FC<Props> = ({ type, name }) => {
  const [field, _meta, helpers] = useField<Predicate>(name);

  const predicates = typeMap[type].map((predicate) => ({
    value: predicate,
    label: predicateMap?.[predicate] ?? "",
  }));
  return (
    <Menu strategy="fixed">
      <MenuButton
        as={Button}
        size="sm"
        aria-label="Options"
        variant="none"
        className="rounded-r-none w-[135px] text-left"
      >
        {predicateMap?.[field.value] ?? ""}
      </MenuButton>
      <MenuList>
        <MenuOptionGroup
          type="radio"
          value={field.value}
          onChange={(value) => helpers.setValue(value as Predicate)}
        >
          {predicates.map(({ value, label }) => (
            <MenuItemOption value={value}>{label}</MenuItemOption>
          ))}
        </MenuOptionGroup>
      </MenuList>
    </Menu>
  );
};
