import { Spinner } from "@chakra-ui/react";
export const PageSpinner = () => {
  return (
    <div className="flex-center h-screen">
      <Spinner size="xl" />
    </div>
  );
};
