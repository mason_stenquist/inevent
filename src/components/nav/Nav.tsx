import React from "react";
import { NavLink } from "react-router-dom";

import { useRoutes } from "~/hooks/useRoutes";

const NavButton: React.FC<{ to: string; children?: React.ReactNode }> = ({
  to,
  children,
}) => {
  return (
    <NavLink
      to={to}
      className={({ isActive }) =>
        `text-small flex items-center justify-center rounded-lg py-1 px-2 transition-all duration-300 hover:bg-button-default-hover lg:py-1.5 lg:px-4 lg:text-base dark:hover:bg-white/20  ${
          isActive
            ? "bg-button-default dark:bg-white/10 "
            : "text-muted-text hover:text-default-text"
        }`
      }
    >
      {children}
    </NavLink>
  );
};

export const Nav = () => {
  const { routes } = useRoutes();

  return (
    <div className="flex space-x-1 font-semibold">
      <NavButton to={routes.home.url()}>{routes.home.label}</NavButton>
      <NavButton to={routes.guestIndex.url()}>
        {routes.guestIndex.label}
      </NavButton>
      <NavButton to={routes.flightIndex.url()}>
        {routes.flightIndex.label}
      </NavButton>
      <NavButton to={routes.activityIndex.url()}>
        {routes.activityIndex.label}
      </NavButton>
      <NavButton to={routes.housingIndex.url()}>
        {routes.housingIndex.label}
      </NavButton>
      <NavButton to={routes.reportIndex.url()}>
        {routes.reportIndex.label}
      </NavButton>
    </div>
  );
};
