import { Breadcrumb, BreadcrumbItem, BreadcrumbLink } from "@chakra-ui/react";
import {
  ChevronLeftIcon,
  ChevronRightIcon,
  LocationMarkerIcon,
} from "@heroicons/react/outline";
import { useMemo } from "react";
import { Link, useParams } from "react-router-dom";

import { StatusArea } from "./StatusArea";
import { Nav } from "~/components";
import { useRoutes } from "~/hooks";
import { useEvent } from "~/state/eventAtom";

export const Header = () => {
  const params = useParams();
  const { breadcrumbs } = useRoutes();
  const [event] = useEvent();
  const breadcrumbItems = useMemo(
    () =>
      breadcrumbs.map((route, index) => {
        const isLast = index === breadcrumbs.length - 1;
        return (
          <BreadcrumbItem
            key={index}
            isCurrentPage={isLast}
            className={`hover:text-accent ${isLast ? "text-muted-text" : ""}`}
          >
            <BreadcrumbLink
              as={Link}
              /* @ts-ignore */
              to={route?.url(params) ?? ""}
              fontSize="md"
            >
              <div className="-mt-[3px]">{route?.label}</div>
            </BreadcrumbLink>
          </BreadcrumbItem>
        );
      }),
    [breadcrumbs]
  );

  const backButton = useMemo(() => {
    const backIndex = breadcrumbs.length - 2;
    const backRoute = breadcrumbs[backIndex] ?? null;
    if (!backRoute) return null;
    return (
      <Link
        /* @ts-ignore */
        to={backRoute.url(params)}
        className="flex items-center space-x-1 hover:text-accent"
      >
        <ChevronLeftIcon className="w-4" />
        <span className="-mt-0.5">{backRoute.label}</span>
      </Link>
    );
  }, [breadcrumbs]);

  return (
    <>
      <div className="flex items-center justify-between py-4 2xl:pt-10">
        <div className="flex space-x-3">
          <div className="flex h-8 w-8 items-center justify-center rounded-lg bg-sky-700 ">
            <LocationMarkerIcon className="h-5 w-5 text-white" />
          </div>
          <div className="pt-0.5 text-lg font-extrabold">
            <span className="hidden sm:inline">inEvent Manager - </span>
            {event?.eventName}
          </div>
        </div>
        <StatusArea />
      </div>

      <div className="mb-8 hidden items-center justify-between sm:flex sm:space-x-4">
        <div>
          <div className="hidden lg:block">
            <Breadcrumb separator={<ChevronRightIcon className="w-3" />}>
              {breadcrumbItems}
            </Breadcrumb>
          </div>
          <div className="block lg:hidden">{backButton}</div>
        </div>
        <div className="hidden sm:block">
          <Nav />
        </div>
      </div>
    </>
  );
};
