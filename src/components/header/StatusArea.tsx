import {
  Avatar,
  Button,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuItem,
  MenuList,
  useColorMode,
} from "@chakra-ui/react";
import { MoonIcon, SunIcon } from "@heroicons/react/outline";
import { I18n } from "aws-amplify";
import { useAtom } from "jotai";
import { useEffect } from "react";
import { Link } from "react-router-dom";

import { useChangeEvent } from "~/modules/settings/event-management/useEventPicker";

import { NetworkIndicator } from "~/components/header/NetworkIndicator";

import { useRoutes } from "~/hooks";
import { langAtom } from "~/state";

export const StatusArea = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  const [lang, setLang] = useAtom(langAtom);
  const { routes } = useRoutes();
  const changeEvent = useChangeEvent();

  useEffect(() => {
    const metaThemeColor = document.querySelector("meta[name=theme-color]");
    const appleMobileBar = document.querySelector(
      "meta[name=apple-mobile-web-app-status-bar-style]"
    );
    if (metaThemeColor && appleMobileBar) {
      const color = colorMode === "dark" ? "#1E1E2E" : "#FBF9F6";
      metaThemeColor.setAttribute("content", color);
      appleMobileBar.setAttribute("content", color);
    }
  }, [colorMode]);

  const setLanguage = (lang: string) => {
    I18n.setLanguage(lang);
    setLang(lang);
  };

  return (
    <div className="flex space-x-3">
      <NetworkIndicator />

      <Button onClick={toggleColorMode}>
        {colorMode === "dark" ? (
          <SunIcon className="w-4" />
        ) : (
          <MoonIcon className="w-4" />
        )}
      </Button>

      <Menu>
        <MenuButton className="rounded-full focus:ring-4 h-10 w-10">
          <Avatar
            size="sm"
            name="Mason Stenquist"
            className="w-10 h-10 text-black focus:ring-2"
          />
        </MenuButton>
        <MenuList>
          <MenuGroup title="Profile">
            <MenuItem as={Link} to={routes.settingsMyAccount.url()}>
              My Account
            </MenuItem>
            <MenuItem as={Link} to={routes.settingsBilling.url()}>
              Billing
            </MenuItem>
            <MenuItem as={Link} to={routes.settingsEvents.url()}>
              {routes.settingsEvents.label}
            </MenuItem>
            <MenuItem as={Link} to={routes.settingsUsers.url()}>
              {routes.settingsUsers.label}
            </MenuItem>
          </MenuGroup>
          <MenuDivider />
          <MenuItem onClick={changeEvent}>Change event</MenuItem>
          <MenuDivider />
          <MenuGroup title="Laguage">
            <MenuItem
              onClick={() => setLanguage("en")}
              className={`${
                lang === "en"
                  ? "text-accent border-l-4 border-accent bg-table-row-hover pl-2"
                  : ""
              }`}
            >
              English
            </MenuItem>
            <MenuItem
              onClick={() => setLanguage("es")}
              className={`${
                lang === "es"
                  ? "text-accent  border-l-4 border-accent bg-table-row-hover pl-2"
                  : ""
              }`}
            >
              Spanish
            </MenuItem>
          </MenuGroup>
        </MenuList>
      </Menu>
    </div>
  );
};
