import {
  StatusOfflineIcon,
  SwitchVerticalIcon,
} from "@heroicons/react/outline";
import { useEffect, useState } from "react";

import { Hub } from "@aws-amplify/core";
import { useRecents } from "~/hooks";

export const NetworkIndicator = () => {
  const [networkConnection, setNetworkConnection] = useState(true);
  const [mutationQueue, setMutationQueue] = useState<any[]>([]);
  const { getRecents, deleteExistingMatches, saveRecents } = useRecents();

  const handleRecordDelete = (data: any) => {
    if (data.element._deleted) {
      const id = data.element.id;
      const modelName = data.model.name;
      const recents = getRecents(modelName);
      const filtered = deleteExistingMatches(recents, id);
      saveRecents(modelName, filtered);
    }
  };

  useEffect(() => {
    const listener = Hub.listen("datastore", async (hubData) => {
      const { event, data } = hubData.payload;

      // console.log(hubData.payload);

      switch (event) {
        case "networkStatus":
          setNetworkConnection(data.active);
          break;
        case "outboxMutationEnqueued":
          setMutationQueue((prev) => [...prev, data]);
          break;
        case "outboxMutationProcessed":
          setMutationQueue((prev) =>
            prev.filter((item) => item.element.id !== data.element.id)
          );
          handleRecordDelete(data);
          break;
        case "outboxStatus":
          if (data.isEmpty) {
            setMutationQueue([]);
          }
          break;
        default:
          break;
      }
    });

    return () => listener();
  }, []);

  if (mutationQueue.length === 0) return null;

  return (
    <div className="relative">
      <button
        title={`${mutationQueue.length} records to sync\nNetwork Status: ${
          networkConnection ? "Connected" : "Disconnected"
        }`}
        className={`flex-center h-8  w-8 rounded-lg ${
          networkConnection ? "bg-green-200" : "bg-red-100 "
        }`}
      >
        {networkConnection ? (
          <SwitchVerticalIcon className="w-5 text-green-600" />
        ) : (
          <StatusOfflineIcon className="w-5 text-red-600" />
        )}
      </button>
      {mutationQueue.length > 0 && (
        <p
          className={`flex-center absolute top-[-3px] right-[-5px] h-4 w-4 rounded-full  text-[10px] text-inverted-text ${
            networkConnection ? "bg-green-600" : "bg-red-600"
          }`}
        >
          {mutationQueue.length}
        </p>
      )}
    </div>
  );
};
