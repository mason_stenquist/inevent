import { Avatar } from "@chakra-ui/react";
import { useState } from "react";
import { Link } from "react-router-dom";

import { S3Image } from "../s3-image";
import { Model } from "~/@types/aws/model";
import { useRecents } from "~/hooks";

interface Props {
  model: Model;
}

export const RecentsBar: React.FC<Props> = ({ model }) => {
  const { getRecents } = useRecents();
  const [showRecents, setShowRecents] = useState(false);
  const recents = getRecents(model.name);

  if (recents.length === 0) return null;

  const maxHeight = showRecents ? "max-h-[500px]" : "max-h-0";

  return (
    <div className="rounded w-full overflow-hidden">
      <div className="block card p-0 md:flex md:space-x-1.5 items-center relative overflow-hidden">
        <button
          onClick={() => setShowRecents((p) => !p)}
          className="flex justify-center space-x-1 md:space-x-0 md:block md:rounded-l-lg py-3 md:py-4 md:py-2 px-3 bg-button-default h-full block md:text-xs text-center w-full md:w-auto dark:bg-modal"
        >
          <p>Recently</p>
          <p>Viewed</p>
          <p className="inline md:hidden">({recents.length})</p>
        </button>

        <div
          className={`overflow-hidden block md:flex md:space-x-1.5 items-center transition-all duration-300 md:max-h-[500px] ${maxHeight}`}
        >
          {recents.map((recent) => (
            <Link
              key={recent.path}
              to={recent.path}
              className="m-2 md:m-0 hover:bg-page px-2 py-1 rounded-lg flex space-x-3 text-sm items-center whitespace-nowrap"
            >
              <S3Image
                src={recent.photoKey}
                readOnly
                className="min-w-[32px] w-[32px] h-[32px] object-cover rounded-full block"
              >
                <Avatar name={recent.name} size="sm" />
              </S3Image>
              <div>{recent.name}</div>
            </Link>
          ))}
        </div>

        <div className="hidden md:block absolute right-0 h-full w-96 bg-gradient-to-l from-card rounded-r-lg pointer-events-none"></div>
      </div>
    </div>
  );
};
