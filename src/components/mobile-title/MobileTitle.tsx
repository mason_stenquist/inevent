import { ChevronLeftIcon } from "@heroicons/react/outline";

import { useRoutes } from "~/hooks/useRoutes";

interface Props {
  title?: string;
  subtitle?: string;
  leftButtonText?: string;
  leftButtonAction?: () => void;
  leftButtonIcon?: JSX.Element;
  rightButton?: JSX.Element;
}

export const MobileTitle = ({
  title,
  subtitle,
  leftButtonText,
  leftButtonAction,
  leftButtonIcon = <ChevronLeftIcon className="w-5" />,
  rightButton,
}: Props) => {
  const { currentRoute } = useRoutes();

  return (
    <div className="fixed left-0 top-0 flex h-12 w-full justify-between bg-page/60 py-0  shadow backdrop-blur-lg sm:hidden">
      <div className="flex w-1/3">
        {leftButtonText && (
          <button
            onClick={leftButtonAction}
            className="flex items-center space-x-1 px-4 text-left text-accent"
          >
            {leftButtonIcon}
            <span className="-mt-0.5">{leftButtonText}</span>
          </button>
        )}
      </div>
      <div className="flex w-1/3 flex-col items-center justify-center text-center">
        <p className="text-sm">{title ?? currentRoute?.label}</p>
        {subtitle && <p className=" text-[10px] text-muted-text">{subtitle}</p>}
      </div>
      <div className="flex w-1/3 justify-end">{rightButton && rightButton}</div>
    </div>
  );
};
