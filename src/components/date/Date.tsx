import dayjs from "dayjs";

interface Props {
  children: string | undefined | null;
  format?: string;
}

export const Date = ({ format, children }: Props) => {
  if (!children) return <>mm/dd/yyyy</>;

  //https://day.js.org/docs/en/display/format
  return <>{dayjs(children).format(format ?? "M/D/YYYY")}</>;
};
