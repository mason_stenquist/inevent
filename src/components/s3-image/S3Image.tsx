import { Spinner } from "@chakra-ui/react";
import { Cache, Storage } from "aws-amplify";
import {
  ChangeEvent,
  ImgHTMLAttributes,
  ReactElement,
  cloneElement,
  useEffect,
  useRef,
  useState,
} from "react";

interface Props extends Omit<ImgHTMLAttributes<HTMLImageElement>, "src"> {
  src?: string | null;
  uploadPath?: string;
  readOnly?: boolean;
  onKeyChange?: (key: string) => void;
  children: ReactElement;
}

export const S3Image: React.FC<Props> = ({
  src,
  uploadPath = "",
  onKeyChange,
  readOnly = false,
  children,
  ...rest
}) => {
  const [s3Key, setS3Key] = useState(src);
  const [url, setUrl] = useState<string>("");
  const [uploading, setUploading] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const handleUpload = async (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (!file) return;
    setUploading(true);
    const { key } = await Storage.put(`${uploadPath}/${file.name}`, file, {
      contentType: file.type,
    });

    setS3Key(key);
  };

  const handleUploadClick = () => {
    if (!inputRef.current) return;
    inputRef.current.click();
  };

  const fallbackElement = cloneElement(children, {
    onClick: handleUploadClick,
  });

  const getImageUrl = async (key: string) => {
    const cachedUrl = Cache.getItem(key);
    if (cachedUrl) {
      setUrl(cachedUrl);
      setUploading(false);
      return;
    }

    setUploading(true);
    const url = await Storage.get(key);
    setUrl(url);
    Cache.setItem(key, url, {
      expires: new Date().getTime() + 1000 * 60 * 15,
    }); //Expire in 15 Minutes
    setUploading(false);
  };

  useEffect(() => {
    if (!s3Key) return;
    if (onKeyChange) onKeyChange(s3Key);
    getImageUrl(s3Key);
  }, [s3Key]);

  if (uploading)
    return (
      <div className="h-full w-full flex-center">
        <Spinner />
      </div>
    );

  return (
    <>
      {!url && !readOnly && fallbackElement}
      {!url && readOnly && children}
      {url && (
        <div className="group relative">
          <img src={url} {...rest} />
          {!readOnly && (
            <button
              onClick={handleUploadClick}
              className="opacity-0 absolute inset-0  group-hover:opacity-90 transition-all duration-300"
            >
              {children}
            </button>
          )}
        </div>
      )}
      <input
        className="hidden"
        ref={inputRef}
        type="file"
        onChange={handleUpload}
        capture="environment"
        accept="image/*"
      />
    </>
  );
};
