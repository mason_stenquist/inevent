import {
  IconButton,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuItem,
  MenuItemOption,
  MenuList,
  MenuOptionGroup,
} from "@chakra-ui/react";
import { CogIcon, FilterIcon } from "@heroicons/react/outline";
import React from "react";
import { useSearchParams } from "react-router-dom";

import { FilterConfig } from "../filter-config";
import { useQuery } from "~/datastore";
import { useDrawer } from "~/hooks";
import { Filter } from "~/models";
import { serializeData } from "~/utils";

interface Props {
  modelName: string;
  onAdvancedClick: () => void;
}

export const FilterMenu: React.FC<Props> = ({ modelName, onAdvancedClick }) => {
  const { openDrawer, closeDrawer } = useDrawer();
  const [searchParams, setSearhParams] = useSearchParams();
  const searchObject = serializeData(searchParams);

  const { data: filters, loading } = useQuery({
    model: Filter,
    query: (c) => c.modelName("eq", modelName),
  });

  const showFilterConfig = () => {
    openDrawer({
      title: `${t(modelName)} Quick Filters`,
      body: <FilterConfig modelName={modelName} />,
    });
  };

  const setFilter = (filterString: string | string[]) => {
    if (typeof filterString === "object") return;
    console.log(filterString);
    const filterJSON = JSON.parse(filterString);
    setSearhParams(filterJSON);
  };

  return (
    <Menu>
      <MenuButton
        as={IconButton}
        icon={<FilterIcon className="h-6 w-6" />}
      ></MenuButton>
      <MenuList>
        <MenuOptionGroup
          title="Quick Filters"
          type="radio"
          value={JSON.stringify(searchObject)}
          onChange={(value) => setFilter(value)}
        >
          <IconButton
            size="sm"
            aria-label="Edit quick filters"
            className="absolute top-0 right-0 mr-2.5 mt-2.5"
            onClick={showFilterConfig}
          >
            <CogIcon className="w-5 " />
          </IconButton>
          {filters.map((filter) => (
            <MenuItemOption key={filter.name} value={filter.filter}>
              {filter.name}
            </MenuItemOption>
          ))}
        </MenuOptionGroup>
        <MenuDivider />
        <MenuGroup>
          <MenuItem onClick={onAdvancedClick}>Advanced Filter</MenuItem>
        </MenuGroup>
      </MenuList>
    </Menu>
  );
};
