import { useNavigate, useSearchParams } from "react-router-dom";

import { serializeData } from "~/utils";

interface Props {
  action?: string;
  submitOnEnter?: true;
  children?: React.ReactNode;
}

export const SearchForm: React.FC<Props> = ({
  action,
  submitOnEnter,
  children,
}) => {
  const [searchParams, setSearchParams] = useSearchParams({});
  const navigate = useNavigate();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.target as HTMLFormElement);
    if (action) {
      const urlParams = new URLSearchParams(formData as any).toString();
      const url = `${action}?${urlParams}`;
      console.log(url);
      navigate(url);
      return;
    }
    const params = serializeData(formData);
    const existingParams = serializeData(searchParams);
    setSearchParams({ ...existingParams, ...params });
  };

  return (
    <form action={action} onSubmit={handleSubmit} className="w-full">
      {children}
      {submitOnEnter && (
        <button type="submit" className="hidden">
          Go
        </button>
      )}
    </form>
  );
};
