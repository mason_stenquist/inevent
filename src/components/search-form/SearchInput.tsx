import { Input, InputProps } from "@chakra-ui/react";
import { XIcon } from "@heroicons/react/outline";
import { ChangeEvent, useEffect, useRef, useState } from "react";

import { useSearchParamKey } from "~/hooks/useSearchParamKey";

type Props = InputProps & {
  name: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
};

export const SearchInput: React.FC<Props> = ({ name, onChange, ...rest }) => {
  const [search, setSearch] = useSearchParamKey(name);
  const [searchInput, setSearchInput] = useState("");
  const ref = useRef<HTMLInputElement>(null);

  //On first load, set input to match queryString
  useEffect(() => {
    if (search !== searchInput) {
      setSearchInput(search);
    }
  }, [search]);

  //Update searchquery using 300 debounce
  useEffect(() => {
    const timeout = searchInput ? 300 : 0;
    const timerId = setTimeout(() => {
      if (search !== searchInput) {
        setSearch(searchInput);
      }
    }, timeout);
    return () => clearTimeout(timerId);
  }, [searchInput]);

  const clearSearch = () => {
    setSearchInput("");
    if (ref.current) {
      ref.current.focus();
    }
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      onChange(e);
    }
    setSearchInput(e.target.value);
  };

  return (
    <div className="relative w-full">
      <Input
        ref={ref}
        value={searchInput}
        name={name}
        onChange={handleChange}
        {...rest}
      />
      {!!searchInput && (
        <button
          onClick={clearSearch}
          type="button"
          className="flex-center absolute right-[2px] top-0 z-10 m-1.5 h-7 w-7  rounded-full transition-all duration-300 hover:bg-table-row-hover focus:outline-none focus:ring"
        >
          <XIcon className="w-4" />
        </button>
      )}
    </div>
  );
};
