import { Select, SelectProps } from "@chakra-ui/react";
import { ChangeEvent, useEffect, useRef } from "react";
import { useSearchParams } from "react-router-dom";

import { serializeData } from "~/utils";

type Props = SelectProps & {
  name: string;
};

export const SearchSelect: React.FC<Props> = ({ name, children, ...rest }) => {
  const [searchParams, setSearchParams] = useSearchParams();
  const ref = useRef<HTMLSelectElement>(null);

  useEffect(() => {
    if (ref.current) {
      ref.current.value = searchParams.get(name) ?? "";
    }
  }, [searchParams]);

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const params = serializeData(searchParams);
    setSearchParams({ ...params, [name]: event.target.value });
  };

  return (
    <Select ref={ref} name={name} {...rest} onChange={handleChange}>
      {children}
    </Select>
  );
};
