import React, { ReactChild, useState } from "react";

import { Wizard } from "./Wizard";

interface Props {
  children: ReactChild | ReactChild[];
}

export const useWizard = (maxSteps: number) => {
  const [currentStep, setCurrentStep] = useState(0);
  const next = () => {
    setCurrentStep((s) => Math.min(s + 1, maxSteps - 1));
  };
  const prev = () => {
    setCurrentStep((s) => Math.max(s - 1, 0));
  };
  const WizardWithStep: React.FC<Props> = ({ children }) =>
    Wizard({ currentStep, setCurrentStep, children });
  return { next, prev, Wizard: WizardWithStep };
};
