interface Props {
  title: string;
  subtitle: string;
  isHighlighted?: boolean;
  setIndex?: () => void;
  children?: React.ReactNode;
}

export const WizardStep: React.FC<Props> = ({
  title,
  subtitle,
  isHighlighted,
  setIndex,
}) => {
  const handleClick = () => {
    if (setIndex) {
      setIndex();
    }
  };
  return (
    <button
      onClick={handleClick}
      disabled={!isHighlighted}
      className={`text-left  underline-offset-2 w-full border-l-4 pl-2 sm:pl-0 sm:border-l-0 sm:border-t-4 py-2 ${
        isHighlighted && "border-accent hover:underline"
      }`}
    >
      <p className="font-semibold">{title}</p>
      <p className="text-sm text-muted-text">{subtitle}</p>
    </button>
  );
};
