import { Children, ReactChild, ReactElement } from "react";

import { WizardStep } from "./WizardStep";

interface Props {
  currentStep: number;
  setCurrentStep: (step: number) => void;
  children: ReactChild | ReactChild[];
}

export const Wizard: React.FC<Props> = ({
  currentStep,
  setCurrentStep,
  children,
}) => {
  let currentChild = <></>;
  return (
    <div>
      <div className="sm:flex sm:space-x-6 space-y-2 sm:space-y-0">
        {Children.map(children, (child, index) => {
          const component = child as ReactElement;
          const { title, subtitle, children } = component.props;
          if (index === currentStep) {
            currentChild = children;
          }
          return (
            <WizardStep
              title={title}
              subtitle={subtitle}
              isHighlighted={currentStep >= index}
              setIndex={() => setCurrentStep(index)}
            />
          );
        })}
      </div>
      {currentChild}
    </div>
  );
};

{
  /* <WizardStep
title="Step 1"
subtitle="Select import file"
isHighlighted={currentStep === 0}
/>
<WizardStep title="Step 2" subtitle="Select import file" />
<WizardStep title="Step 2" subtitle="Select import file" />
<WizardStep title="Step 2" subtitle="Select import file" /> */
}
