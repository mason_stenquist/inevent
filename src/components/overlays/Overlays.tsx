import React from "react";

import { Drawer, ReloadPrompt } from "~/components";
import { useDialogue, useModal } from "~/hooks";

export const OverlayComponents = () => {
  const { Dialogue } = useDialogue();
  const { Modal } = useModal();

  console.log("Rerender overlay");

  return (
    <>
      <ReloadPrompt />
      <Drawer />
      <Dialogue />
      <Modal />
    </>
  );
};

export const Overlays = React.memo(OverlayComponents);
