import {
  Drawer as ChakraDrawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  useColorMode,
} from "@chakra-ui/react";
import { useAtom } from "jotai";

import { drawerAtom, drawerDefaults } from "~/hooks";

export const Drawer = () => {
  const [state, setState] = useAtom(drawerAtom);
  const { colorMode } = useColorMode();
  const {
    isOpen,
    body,
    title,
    footer,
    closeButton,
    autoFocus,
    closeOnEsc,
    closeOnOverlayClick,
    placement,
    size,
  } = state;

  return (
    <ChakraDrawer
      isOpen={isOpen ?? false}
      placement={placement}
      onClose={() => setState(drawerDefaults)}
      autoFocus={autoFocus}
      closeOnEsc={closeOnEsc}
      closeOnOverlayClick={closeOnOverlayClick}
      size={size}
    >
      <DrawerOverlay />
      <DrawerContent className={colorMode}>
        {closeButton && <DrawerCloseButton />}
        {title && <DrawerHeader>{title}</DrawerHeader>}

        <DrawerBody>{body}</DrawerBody>

        {footer && <DrawerFooter>{footer}</DrawerFooter>}
      </DrawerContent>
    </ChakraDrawer>
  );
};
