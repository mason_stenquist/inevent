/* eslint-disable */
import { Button, Checkbox, Collapse } from "@chakra-ui/react";
import {
  ChevronDownIcon,
  ChevronUpIcon,
  SelectorIcon,
} from "@heroicons/react/outline";
import React from "react";
import { useFlexLayout, useRowSelect, useSortBy, useTable } from "react-table";

interface Props {
  columns: any;
  data: any;
}

const IndeterminateCheckbox = ({ indeterminate, checked, ...rest }: any) => {
  return (
    <Checkbox
      marginLeft={2.5}
      marginTop={1.5}
      isChecked={checked}
      isIndeterminate={indeterminate}
      {...rest}
    />
  );
};

export const Table = ({ columns, data }: Props) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    selectedFlatRows,
    toggleAllRowsSelected,
    state: { selectedRowIds },
  } = useTable(
    {
      columns,
      data,
    },
    useSortBy,
    useRowSelect,
    useFlexLayout,
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        // Let's make a column for selection
        {
          id: "selection",
          canSort: false,
          width: 15,
          // The header can use the table's getToggleAllRowsSelectedProps method
          // to render a checkbox
          Header: ({ getToggleAllRowsSelectedProps }) => (
            <div>
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
            </div>
          ),
          // The cell can use the individual row's getToggleRowSelectedProps method
          // to the render a checkbox
          Cell: ({ row }) => (
            <div>
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            </div>
          ),
        },
        ...columns,
      ]);

      hooks.rows;
    }
  );

  return (
    <div className="">
      <div className="w-full border-t" {...getTableProps()}>
        <div>
          {headerGroups.map((headerGroup) => (
            <div
              className="border-b text-sm bg-page"
              {...headerGroup.getHeaderGroupProps()}
            >
              {headerGroup.headers.map((column) => (
                <div
                  className="flex items-center text-left pr-4 py-1 select-none"
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                >
                  <div
                    className={`flex items-center space-x-1 ${
                      column.canSort && "hover:text-accent"
                    }`}
                  >
                    <div className="font-bold">
                      <>{column.render("Header")}</>
                    </div>
                    {column.canSort && (
                      <div className="w-3 mt-1">
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <ChevronDownIcon />
                          ) : (
                            <ChevronUpIcon />
                          )
                        ) : (
                          <SelectorIcon />
                        )}
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
          ))}
        </div>
        <Collapse in={selectedFlatRows.length > 0}>
          <div className="border-b p-2 flex justify-between items-center">
            <div className="space-x-2">
              <Button onClick={() => toggleAllRowsSelected(false)}>
                Cancel
              </Button>
              <Button>Send Message</Button>
              <Button className="btn-danger">Remove from activity</Button>
            </div>
            <div>{selectedFlatRows.length} items selected</div>
          </div>
        </Collapse>
        <div {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <div
                className={`border-b ${row.isSelected && "bg-table-row-hover"}`}
                {...row.getRowProps()}
              >
                {row.cells.map((cell) => {
                  return (
                    <div
                      className="pr-4 py-1.5 text-sm flex items-center"
                      {...cell.getCellProps()}
                    >
                      <>{cell.render("Cell")}</>
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
