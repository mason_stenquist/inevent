// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Activity, Agenda, Guest, Event, Filter } = initSchema(schema);

export {
  Activity,
  Agenda,
  Guest,
  Event,
  Filter
};