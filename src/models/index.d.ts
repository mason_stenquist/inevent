import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";





type ActivityMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type AgendaMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type GuestMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type EventMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type FilterMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

export declare class Activity {
  readonly id: string;
  readonly group: string;
  readonly name: string;
  readonly category?: string | null;
  readonly description?: string | null;
  readonly date?: string | null;
  readonly start?: string | null;
  readonly end?: string | null;
  readonly agenda?: (Agenda | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Activity, ActivityMetaData>);
  static copyOf(source: Activity, mutator: (draft: MutableModel<Activity, ActivityMetaData>) => MutableModel<Activity, ActivityMetaData> | void): Activity;
}

export declare class Agenda {
  readonly id: string;
  readonly group: string;
  readonly guest?: Guest | null;
  readonly activity?: Activity | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Agenda, AgendaMetaData>);
  static copyOf(source: Agenda, mutator: (draft: MutableModel<Agenda, AgendaMetaData>) => MutableModel<Agenda, AgendaMetaData> | void): Agenda;
}

export declare class Guest {
  readonly id: string;
  readonly group: string;
  readonly nameFirst: string;
  readonly _nameFirst?: string | null;
  readonly nameLast: string;
  readonly _nameLast?: string | null;
  readonly company?: string | null;
  readonly _company?: string | null;
  readonly position?: string | null;
  readonly _position?: string | null;
  readonly dateOfBirth?: string | null;
  readonly _keyPhoto?: string | null;
  readonly agenda?: (Agenda | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Guest, GuestMetaData>);
  static copyOf(source: Guest, mutator: (draft: MutableModel<Guest, GuestMetaData>) => MutableModel<Guest, GuestMetaData> | void): Guest;
}

export declare class Event {
  readonly id: string;
  readonly group: string;
  readonly eventName: string;
  readonly _eventName?: string | null;
  readonly startDate?: string | null;
  readonly endDate?: string | null;
  readonly _logo?: string | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Event, EventMetaData>);
  static copyOf(source: Event, mutator: (draft: MutableModel<Event, EventMetaData>) => MutableModel<Event, EventMetaData> | void): Event;
}

export declare class Filter {
  readonly id: string;
  readonly name: string;
  readonly modelName: string;
  readonly filter: string;
  readonly pinned?: boolean | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Filter, FilterMetaData>);
  static copyOf(source: Filter, mutator: (draft: MutableModel<Filter, FilterMetaData>) => MutableModel<Filter, FilterMetaData> | void): Filter;
}