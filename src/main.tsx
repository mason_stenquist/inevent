import { ChakraProvider } from "@chakra-ui/react";
import React from "react";
import { createRoot } from "react-dom/client";
import { QueryClient, QueryClientProvider, useQuery } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { BrowserRouter } from "react-router-dom";

import AppRouter from "./AppRouter";
// @ts-ignore
import awsconfig from "./aws-exports.js";
import "./styles/index.css";
import { theme } from "./theme";
import { configureDataStore } from "./utils";
import { Amplify } from "@aws-amplify/core";
import { PubSub } from "@aws-amplify/pubsub";
import { Authenticator } from "@aws-amplify/ui-react";
import "@aws-amplify/ui-react/styles.css";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      keepPreviousData: true,
      staleTime: 1000 * 60 * 5,
    },
  },
});

Amplify.configure(awsconfig);
PubSub.configure(awsconfig);

configureDataStore();

const container = document.getElementById("root");
const root = createRoot(container!);
root.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <ChakraProvider theme={theme}>
        <BrowserRouter>
          <Authenticator>{({ signOut, user }) => <AppRouter />}</Authenticator>
        </BrowserRouter>
      </ChakraProvider>
      {import.meta.env.DEV && <ReactQueryDevtools initialIsOpen={false} />}
    </QueryClientProvider>
  </React.StrictMode>
);
