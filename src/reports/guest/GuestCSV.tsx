import { Button, FormControl, FormLabel, useColorMode } from "@chakra-ui/react";

import Papa from "papaparse";
import { PageSpinner, SearchForm, SearchSelect } from "~/components";
import { useQuery } from "~/datastore";
import { Guest } from "~/models";

declare global {
  interface Window {
    $downloadCSV: () => void;
  }
}

function Options() {
  return (
    <SearchForm>
      <FormControl className="pb-2">
        <FormLabel>Company</FormLabel>
        <SearchSelect name="company">
          <option>DeveloperTown</option>
          <option>Test</option>
        </SearchSelect>
      </FormControl>
      <Button type="submit">Update</Button>
    </SearchForm>
  );
}

function Report() {
  const { colorMode } = useColorMode();
  const { data, loading } = useQuery({ model: Guest });
  if (loading) return <PageSpinner />;

  const columns = Object.keys(data[0]).filter(
    (column) => !column.startsWith("_") && !column.startsWith("key")
  );
  console.log(columns);

  const downloadCSV = () => {
    const formattedData = data.map((row: any) =>
      columns.reduce(
        (acc, column) => ({ ...acc, [t(column)]: row[column] }),
        {}
      )
    );

    const csv = Papa.unparse(formattedData);
    const csvData = new Blob([csv], { type: "text/csv;charset=utf-8;" });
    const csvURL = window.URL.createObjectURL(csvData);

    const downloadLink = document.createElement("a");
    downloadLink.href = csvURL;
    downloadLink.setAttribute("download", "export.csv");
    downloadLink.click();
  };

  window.$downloadCSV = downloadCSV;

  return (
    <div className={`text-xs w-[3000px] ${colorMode}`}>
      <table>
        <thead>
          <tr>
            <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
              First Name
            </td>
            <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
              Last Name
            </td>
            <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
              Company
            </td>
            <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
              Position
            </td>
          </tr>
        </thead>
        <tbody>
          {data.map((row) => (
            <tr key={row.id}>
              <td className="border p-0.5">{row.nameFirst}</td>
              <td className="border p-0.5">{row.nameLast}</td>
              <td className="border p-0.5">{row.company}</td>
              <td className="border p-0.5">{row.position}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default {
  category: "Guest" as const,
  name: "Guest CSV",
  path: "guest-csv",
  isCsv: true,
  report: <Report />,
  reportOptions: <Options />,
};
