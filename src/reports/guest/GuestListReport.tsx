import { Button, FormControl, FormLabel } from "@chakra-ui/react";
import { I18n } from "aws-amplify";

import { PageSpinner, SearchForm, SearchSelect } from "~/components";
import { useQuery } from "~/datastore";
import { Guest } from "~/models";

const t = I18n.get;

function Options() {
  return (
    <SearchForm>
      <FormControl className="pb-2">
        <FormLabel>{t`company`}</FormLabel>
        <SearchSelect name="company">
          <option>DeveloperTown</option>
          <option>Test</option>
        </SearchSelect>
      </FormControl>
      <Button type="submit">{t`update`}</Button>
    </SearchForm>
  );
}

function Report() {
  const { data: guests, loading } = useQuery({ model: Guest });
  if (loading) return <PageSpinner />;

  return (
    <div className="min-h-screen bg-white p-4 text-xs text-black print:p-0">
      <table className="w-full">
        <thead className="font-bold">
          <tr>
            <td>{t`nameFirst`}</td>
            <td>{t`nameLast`}</td>
            <td>{t`company`}</td>
            <td>{t`position`}</td>
          </tr>
        </thead>
        <tbody>
          {guests.map((guest) => (
            <tr key={guest.id}>
              <td>{guest.nameFirst}</td>
              <td>{guest.nameLast}</td>
              <td>{guest.company}</td>
              <td>{guest.position}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default {
  category: "Guest" as const,
  name: "Guest List",
  path: "guest-list",
  report: <Report />,
  reportOptions: <Options />,
};
