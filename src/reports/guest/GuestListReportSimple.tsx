import { Button, FormControl, FormLabel } from "@chakra-ui/react";

import { PageSpinner, SearchForm, SearchSelect } from "~/components";
import { useQuery } from "~/datastore";
import { Guest } from "~/models";

function Options() {
  return (
    <SearchForm>
      <FormControl className="pb-2">
        <FormLabel>Company</FormLabel>
        <SearchSelect name="company">
          <option>DeveloperTown</option>
          <option>Test</option>
        </SearchSelect>
      </FormControl>
      <Button type="submit">Update</Button>
    </SearchForm>
  );
}

function Report() {
  const { data: guests, loading } = useQuery({ model: Guest });
  if (loading) return <PageSpinner />;

  return (
    <div className="min-h-screen bg-white p-4 text-xs text-black print:p-0">
      <table>
        <thead>
          <tr>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Company</td>
            <td>Position</td>
          </tr>
        </thead>
        <tbody>
          {guests.map((guest) => (
            <tr key={guest.id}>
              <td>{guest.nameFirst}</td>
              <td>{guest.nameLast}</td>
              <td>{guest.company}</td>
              <td>{guest.position}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default {
  category: "Guest" as const,
  name: "Guest List Simple",
  path: "guest-list-simple",
  report: <Report />,
  reportOptions: <Options />,
};
