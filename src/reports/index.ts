import GuestReports from "./guest";


        import ActivityReports from "./activity";
        // PLOP IMPORT ENTRY

export interface Report {
  category:  "Activity" | "Guest" | "Flight";
  name: string;
  path: string;
  report: JSX.Element;
  reportOptions: JSX.Element;
  isCsv?: boolean;
}

export const reports: Report[] = [...ActivityReports,...GuestReports];
