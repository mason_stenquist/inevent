import { Button, FormControl, FormLabel } from "@chakra-ui/react";

import { PageSpinner, SearchForm, SearchSelect } from "~/components";
import { useQuery } from "~/datastore";
import { Activity } from "~/models";

function Options() {
  return (
    <SearchForm>
      <Button type="submit">{t`update`}</Button>
    </SearchForm>
  );
}

function Report() {
  const { data: activitys, loading } = useQuery({ model: Activity });
  if (loading) return <PageSpinner />;

  return (
    <div className="min-h-screen bg-white p-4 text-xs text-black print:p-0">
      <table className="w-full">
        <thead className="font-bold">
          <tr>
              <td>{t`name` }</td>
              <td>{t`category` }</td>
              <td>{t`description` }</td>
              <td>{t`date` }</td>
              <td>{t`start` }</td>
              <td>{t`end` }</td>
          </tr>
        </thead>
        <tbody>
          { activitys.map((activity) => (
            <tr key={ activity.id }>
                  <td>{ activity.name }</td>
                  <td>{ activity.category }</td>
                  <td>{ activity.description }</td>
                  <td>{ activity.date }</td>
                  <td>{ activity.start }</td>
                  <td>{ activity.end }</td>
            </tr>
          )) }
        </tbody>
      </table>
    </div>
  );
}

export default {
  category: "Activity" as const,
  name: "Activity List",
  path: "activity-list",
  report: <Report />,
  reportOptions: <Options />,
};
