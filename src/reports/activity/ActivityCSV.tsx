import { Button, FormControl, FormLabel, useColorMode } from "@chakra-ui/react";

import Papa from "papaparse";
import { PageSpinner, SearchForm, SearchSelect } from "~/components";
import { useQuery } from "~/datastore";
import { Activity } from "~/models";

declare global {
  interface Window {
    $downloadCSV: () => void;
  }
}

function Options() {
  return (
    <SearchForm>
      <Button type="submit">Update</Button>
    </SearchForm>
  );
}

function Report() {
  const { colorMode } = useColorMode();
  const { data, loading } = useQuery({ model: Activity });
  if (loading) return <PageSpinner />;

  const columns = Object.keys(data[0]).filter(
    (column) => !column.startsWith("_") && !column.startsWith("key")
  );
  console.log(columns);

  const downloadCSV = () => {
    const formattedData = data.map((row: any) =>
      columns.reduce(
        (acc, column) => ({ ...acc, [t(column)]: row[column] }),
        {}
      )
    );

    const csv = Papa.unparse(formattedData);
    const csvData = new Blob([csv], { type: "text/csv;charset=utf-8;" });
    const csvURL = window.URL.createObjectURL(csvData);

    const downloadLink = document.createElement("a");
    downloadLink.href = csvURL;
    downloadLink.setAttribute("download", "export.csv");
    downloadLink.click();
  };

  window.$downloadCSV = downloadCSV;

  return (
    <div className={`text-xs w-[3000px] ${colorMode}`}>
      <table>
        <thead>
          <tr>
              <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
                {t`name` }
              </td>
              <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
                {t`category` }
              </td>
              <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
                {t`description` }
              </td>
              <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
                {t`date` }
              </td>
              <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
                {t`start` }
              </td>
              <td className="border px-0.5 py-1 font-bold sticky top-0 bg-card">
                {t`end` }
              </td>
          </tr>
        </thead>
        <tbody>
          {data.map((row) => (
            <tr key={row.id}>
                <td className="border p-0.5">{row.name }</td>
                <td className="border p-0.5">{row.category }</td>
                <td className="border p-0.5">{row.description }</td>
                <td className="border p-0.5">{row.date }</td>
                <td className="border p-0.5">{row.start }</td>
                <td className="border p-0.5">{row.end }</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default {
  category: "Activity" as const,
  name: "Activity CSV",
  path: "activity-csv",
  isCsv: true,
  report: <Report />,
  reportOptions: <Options />,
};
