import { schema } from "~/models/schema";

export const getSchema = <Model extends { name: string }>(model: Model) => {
  const name = model.name;
  return schema.models[name];
};
