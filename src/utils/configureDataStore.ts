import { DataStore, syncExpression } from "aws-amplify";

import { getCurrentEvent } from "./getCurrentEvent";
import { Guest } from "~/models";

export const configureDataStore = () => {
  const selectedEvent = getCurrentEvent();
  console.log({ selectedEvent });
  DataStore.configure({
    syncExpressions: [
      syncExpression(Guest, () => {
        return (guest) => guest.group("eq", selectedEvent?.group ?? "");
      }),
    ],
  });
};

export const changeSync = async () => {
  await DataStore.clear();
  await DataStore.stop();
  await DataStore.start();
};
