import { getCurrentEvent } from "./getCurrentEvent";
import { getSchema } from "./getSchema";
import { ModelInit, PersistentModelMetaData } from "@aws-amplify/datastore";
import { Model } from "~/@types/aws/model";

export function newModel<T extends Model>(
  model: T,
  data: ModelInit<Model, PersistentModelMetaData>
) {
  const modelSchema = getSchema(model);
  const event = getCurrentEvent();

  const updatedData: any = Object.entries(data).reduce((acc, [key, value]) => {
    //Lowercase fields to make search fields
    const fieldLowerSchema = modelSchema.fields?.[`_${key}`];
    const fieldData = { [key]: value };
    if (fieldLowerSchema && typeof value === "string") {
      fieldData[`_${key}`] = value.toLowerCase();
    }
    return { ...acc, ...fieldData };
  }, {});

  if (event?.group && modelSchema.fields?.group) {
    updatedData.group = event.group;
  }

  let newModel;
  try {
    newModel = new model(updatedData);
  } catch (e: any) {
    throw e;
  }
  return newModel as any;
}
