export const serializeData = (data: FormData | URLSearchParams) => {
  let params: Record<string, string> = {};
  data.forEach((value, key) => {
    if (value) {
      params[key] = value as string;
    }
  });
  return params;
};
