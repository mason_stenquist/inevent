export const cleanObject = (obj: Record<string, any>) =>
  Object.entries(obj).reduce(
    (acc, [key, value]) => (value ? { ...acc, [key]: value } : acc),
    {}
  );
