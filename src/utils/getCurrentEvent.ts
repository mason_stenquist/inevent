import { Event } from "~/models";

export const getCurrentEvent = (): Event | null =>
  JSON.parse(localStorage.getItem("event") ?? "null");
