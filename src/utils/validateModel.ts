import { getSchema } from "./getSchema";
import { Model } from "~/@types/aws/model";

function isValidDate(date: any) {
  return (
    date &&
    Object.prototype.toString.call(date) === "[object Date]" &&
    !isNaN(date)
  );
}

export const validateModel = (
  model: Model,
  data: Record<string, string | number>
): [isValid: boolean, errorMessage?: string] => {
  const modelSchema = getSchema(model);

  let isValid = true;
  let errorMessage;

  const notValid = (error: string) => {
    isValid = false;
    errorMessage = error;
  };

  Object.entries(data).forEach(([key, value]) => {
    const fieldSchema = modelSchema.fields?.[key];
    const translatedKey = t(key);

    if (!value && !fieldSchema.isRequired) return;

    switch (fieldSchema.type) {
      case "String":
      case "ID":
        if (typeof value !== "string")
          notValid(`${translatedKey} is required to be a string`);
        break;
      case "Int":
      case "Flaot":
        if (typeof value !== "number")
          notValid(`${translatedKey} is required to be a number`);
        break;
      case "Boolean":
        if (typeof value !== "boolean")
          notValid(`${translatedKey} is required to be a boolean`);
        break;
      case "AWSDate":
      case "AWSTime":
      case "AWSTimestamp":
        if (!isValidDate(value))
          notValid(`${translatedKey} is required to be a date format`);
        break;
      default:
        break;
    }

    // if (
    //   (fieldSchema.type === "String" || fieldSchema.type === "ID") &&
    //   typeof value !== "string"
    // )
    //   return notValid(`${translatedKey} is required to be a string`);
    // if (
    //   (fieldSchema.type === "Int" || fieldSchema.type === "Float") &&
    //   typeof value !== "number"
    // )
    //  return notValid(`${translatedKey} is required to be a number`);
    // if (fieldSchema.type === "Boolean" && typeof value !== "boolean")
    //   return notValid(`${translatedKey} is required to be a number`);
    // if (
    //   (fieldSchema.type === "AWSDate" ||
    //     fieldSchema.type === "AWSTime" ||
    //     fieldSchema.type === "AWSTimestamp") &&
    //   !isValidDate(value)
    // )
    //   return notValid(`${translatedKey} is required to be a number`);
  });

  return [isValid, errorMessage];
};
