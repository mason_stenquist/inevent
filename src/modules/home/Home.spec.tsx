import React from "react";

import { Home } from "./Home";
import { mount } from "@cypress/react";

it("renders learn react link", () => {
  mount(<Home />);
  cy.get("a").contains("test");
});
