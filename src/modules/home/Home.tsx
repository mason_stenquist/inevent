import { Button } from "@chakra-ui/react";
import { Auth } from "aws-amplify";
import { useEffect } from "react";

import { MobileTitle } from "~/components";

export const Home = () => {
  const getSess = async () => {
    const sess = await Auth.currentSession();
    console.log(sess);
  };
  getSess();

  return (
    <div>
      Home
      <Button onClick={() => window.location.reload()}>Reload</Button>
      <MobileTitle
        rightButton={
          <Button
            className="btn-title"
            onClick={() => window.location.reload()}
          >
            Reload
          </Button>
        }
      />
    </div>
  );
};
