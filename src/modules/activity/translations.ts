export default {
  en: {
    activityDetail: "Activity Detail",
    addActivity: "Add Activity",
    createActivity: "Create Activity",
    deleteActivity: "Delete Activity",
    deleteActivitys: "Delete Activitys",      
    importActivity: "Import Activitys",
      name: "Name",
      category: "Category",
      description: "Description",
      date: "Date",
      start: "Start",
      end: "End",
  },
  es: {
    activityDetail: "Activity Detail",
    addActivity: "Add Activity",
    createActivity: "Create Activity",
    deleteActivity: "Delete Activity",
    deleteActivitys: "Delete Activitys",      
    importActivity: "Import Activitys",
      name: "Name",
      category: "Category",
      description: "Description",
      date: "Date",
      start: "Start",
      end: "End",
  },
};
