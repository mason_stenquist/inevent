import { Button, Tooltip } from "@chakra-ui/react";
import { ChevronRightIcon, ExternalLinkIcon } from "@heroicons/react/outline";
import { DataStore } from "aws-amplify";
import { useMemo } from "react";
import { Link, useParams } from "react-router-dom";
import { Column } from "react-table";

import { useGuestPicker } from "~/modules/guest";

import { listAgenda } from "./activityQueries";
import { Agenda } from "~/API";
import { Table } from "~/components";
import { useGQL, useRoutes } from "~/hooks";
import { newModel } from "~/utils";

export const ActivityGuestTab = () => {
  const { id: activityID } = useParams<{ id: string }>();
  const { routes } = useRoutes();
  const { data } = useGQL(listAgenda(activityID), {
    enabled: Boolean(activityID),
  });

  const { showPicker } = useGuestPicker({
    title: "Select Guests",
    multiple: true,
    size: "2xl",
  });

  // const selectGuests = async () => {
  //   const picked = await showPicker();
  //   if (picked.cancel) return;
  //   console.log(picked.results);
  //   picked.results.forEach((guest: Guest) => {
  //     const record = newModel(Agenda, {
  //       guestID: guest.id,
  //       activityID,
  //     }) as Agenda;
  //     DataStore.save(record);
  //   });
  // };

  const columns = useMemo(
    () => [
      {
        Header: "Guest",
        accessor: ({ guest }: Agenda) => (
          <div className="flex items-center space-x-1">
            <Link
              className="hover:text-accent hover:underline"
              to={routes.guestDetail.url({ id: guest?.id! })}
            >
              {`${guest?.nameFirst} ${guest?.nameLast}`}
            </Link>
            <Link
              className="mt-1 text-muted-text hover:text-accent hover:bg-table-row-hover block w-6 h-6 rounded-full grid place-items-center"
              to={routes.guestDetail.url({ id: guest?.id! })}
              target="new"
            >
              <ExternalLinkIcon className="w-3" />
            </Link>
          </div>
        ),
      },
      {
        Header: "Company",
        accessor: "guest.company",
      },
      {
        Header: "Position",
        accessor: "guest.position",
      },
    ],
    []
  );

  return (
    <div>
      <Button>Add Guests</Button>
      <div className="mt-4 -mx-4">
        <Table data={data?.items ?? []} columns={columns} />
      </div>
    </div>
  );
};
