import { Column } from "~/components";
import { Activity } from "~/models";

export const activityColumns: Column<Activity>[] = [
  {
    key: "name",
  },
  {
    key: "category",
  },
  {
    key: "description",
  },
  {
    key: "date",
  },
  {
    key: "start",
  },
  {
    key: "end",
  },
];
