import { Button } from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { useState } from "react";

import { ModelInit } from "@aws-amplify/datastore";
import * as Yup from "yup";
import { Input } from "~/components";
import { useDrawer } from "~/hooks";
import { Activity, ActivityMetaData } from "~/models";

export type ActivityFields = ModelInit<Activity, ActivityMetaData>;
type ParitalActivityFields = Partial<ActivityFields>;

interface Props {
  initValues: ParitalActivityFields;
  submitText: string;
  onSubmit: (values: ActivityFields) => Promise<void>;
}

const emptyActivity: ActivityFields = {
  name: "",
  category: "",
  description: "",
  date: "",
  start: "",
  end: "",
  group: "",
};

const schema = Yup.object().shape({
  name: Yup.string().label("name"),
  category: Yup.string().label("category"),
  description: Yup.string().label("description"),
  date: Yup.string().label("date"),
  start: Yup.string().label("start"),
  end: Yup.string().label("end"),
});

export const ActivityForm: React.FC<Props> = ({
  initValues,
  onSubmit,
  submitText,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const { closeDrawer } = useDrawer();
  const initialValues = { ...emptyActivity, ...initValues };

  const handleSubmit = async (values: ActivityFields) => {
    setIsLoading(true);
    try {
      await onSubmit(values);
      closeDrawer();
    } catch (e) {
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={schema}
      onSubmit={handleSubmit}
    >
      <Form>
        <Input label={t`name`} type="text" name="name" />
        <Input label={t`category`} type="text" name="category" />
        <Input label={t`description`} type="text" name="description" />
        <Input label={t`date`} type="text" name="date" />
        <Input label={t`start`} type="text" name="start" />
        <Input label={t`end`} type="text" name="end" />

        <div className="space-x-3 pt-4">
          <Button type="submit" className="btn-primary" isLoading={isLoading}>
            {submitText}
          </Button>
          <Button onClick={closeDrawer} isLoading={isLoading}>
            {t`cancel`}
          </Button>
        </div>
      </Form>
    </Formik>
  );
};
