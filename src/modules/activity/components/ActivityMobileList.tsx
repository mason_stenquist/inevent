import { ChevronRightIcon } from "@heroicons/react/outline";
import { Link, Params, generatePath } from "react-router-dom";

import { Activity } from "~/models";

export const ActivityMobileList = (item: Activity, rowTo: string) => {
  const url = generatePath(rowTo, item as unknown as Params<string>);

  return (
    <Link
      to={url}
      key={item.id}
      className="flex w-full items-center justify-between border-t py-3 px-4 text-left last:border-b hover:bg-table-row-hover"
    >
      <div>
        <p className="font-bold">{item?.name}</p>
        <p className="text-muted-text">
          {item?.date} at {item?.start}
        </p>
      </div>
      <div>
        <ChevronRightIcon className="w-5" />
      </div>
    </Link>
  );
};
