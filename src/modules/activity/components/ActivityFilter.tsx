import {
  Button,
  InputGroup,
  InputLeftAddon,
  ModalBody,
  ModalFooter,
  Select,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { useSearchParams } from "react-router-dom";

import { Input, PredicateFilterSelect } from "~/components";
import { cleanObject, serializeData } from "~/utils";

interface Props {
  closeModal: () => void;
}

/**
 * ban for does not contain
 * = for equals
 * ≠ for not equals
 * → for starts with
 * dots-circle-horizontal/save for contains
 */

export const ActivityFilter: React.FC<Props> = ({ closeModal }) => {
  const [searchParams, setSearchParams] = useSearchParams();
  const existingValues = serializeData(searchParams);
  const initValues = {
    nameFirst: "",
    nameFirstPredicate: "contains",
    nameLast: "",
    nameLastPredicate: "contains",
    company: "",
    position: "",
    ...existingValues,
  };

  return (
    <Formik
      initialValues={initValues}
      onSubmit={(values) => {
        setSearchParams(cleanObject(values));
        closeModal();
      }}
    >
      <Form>
        <Input
          type="text"
          name="nameFirst"
          label="First Name"
          leftAddon={
            <PredicateFilterSelect type="string" name="nameFirstPredicate" />
          }
        />
        <Input
          type="text"
          name="nameLast"
          label="Last Name"
          leftAddon={
            <PredicateFilterSelect type="string" name="nameLastPredicate" />
          }
        />

        <Input type="text" name="company" label="Company" />
        <Input type="text" name="position" label="Position" />
        <div className="space-x-3">
          <Button onClick={closeModal}>{t`cancel`}</Button>
          <Button className="btn-primary" type="submit">
            Filter
          </Button>
        </div>
      </Form>
    </Formik>
  );
};
