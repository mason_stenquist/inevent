import { Button, Hide } from "@chakra-ui/react";
import { PlusIcon, TrashIcon } from "@heroicons/react/outline";
import { DataStore } from "aws-amplify";

import { useAddActivity } from ".";
import { ActivityGuestTab } from "./ActivityGuestTab";
import {
  ActionBar,
  MobileTitle,
  ReportMenu,
  SearchForm,
  SearchInput,
  Sections,
  SkeletonList,
} from "~/components";
import { useDeleteConfirmation, useRecord } from "~/datastore";
import { useRoutes, useTrackRecent } from "~/hooks";
import { Activity } from "~/models";
import reports from "~/reports/activity";

export const ActivityDetail = () => {
  const { routes } = useRoutes();
  const { data: activity, loading } = useRecord(Activity);
  useTrackRecent(Activity, activity, loading);
  const deleteConfirmation = useDeleteConfirmation();
  const addActivity = useAddActivity();

  console.log(activity);

  if (loading || !activity) return <SkeletonList />;

  return (
    <div className="">
      <div className="mb-4 items-center justify-between space-y-4 sm:flex sm:space-y-0">
        <ActionBar>
          <Hide below="sm">
            <Button
              leftIcon={<PlusIcon className="w-4" />}
              onClick={addActivity}
            >
              {t`addActivity`}
            </Button>
          </Hide>
          <Button
            onClick={() =>
              deleteConfirmation({
                selected: activity,
                body: `${t("areYouSureYouWantToDelete")} this item?`,
                onDelete: () => routes.activityIndex.go(),
              })
            }
            className="hover:bg-danger hover:text-white"
            leftIcon={<TrashIcon className="w-4" />}
          >
            {t`deleteActivity`}
          </Button>
        </ActionBar>

        <div className="flex space-x-3 pl-0 md:pl-4">
          <SearchForm action={routes.activityIndex.url()} submitOnEnter>
            <SearchInput
              name="activitySearch"
              placeholder={t`search...`}
              className="bg-card"
            />
          </SearchForm>
          <ReportMenu reports={reports} />
        </div>
      </div>

      <div className="space-y-4 lg:flex lg:space-x-4 lg:space-y-0">
        <div className="card lg:w-1/4">
          <div className="space-y-3">
            <div>
              <p className="text-xs text-muted-text">{t`name`}</p>
              <p>{activity.name ?? "NA"}</p>
            </div>
            <div>
              <p className="text-xs text-muted-text">{t`category`}</p>
              <p>{activity.category ?? "NA"}</p>
            </div>
            <div>
              <p className="text-xs text-muted-text">{t`description`}</p>
              <p>{activity.description ?? "NA"}</p>
            </div>
            <div>
              <p className="text-xs text-muted-text">{t`date`}</p>
              <p>{activity.date ?? "NA"}</p>
            </div>
            <div>
              <p className="text-xs text-muted-text">{t`start`}</p>
              <p>{activity.start ?? "NA"}</p>
            </div>
            <div>
              <p className="text-xs text-muted-text">{t`end`}</p>
              <p>{activity.end ?? "NA"}</p>
            </div>
          </div>
        </div>
        <div className="lg:w-3/4">
          <Sections
            sections={[
              {
                label: t`guests`,
                element: <ActivityGuestTab />,
              },
              {
                label: t`tab2`,
                element: <div>Tab Content 2</div>,
              },
            ]}
          />
        </div>
      </div>
      <MobileTitle
        leftButtonText={routes.activityIndex.label}
        leftButtonAction={() => routes.activityIndex.go()}
        rightButton={
          <Button className="btn-title" onClick={addActivity}>
            Add Activity
          </Button>
        }
      />
    </div>
  );
};

export default ActivityDetail;
