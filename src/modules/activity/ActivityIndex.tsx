import { Button } from "@chakra-ui/react";
import {
  DownloadIcon,
  MenuAlt2Icon,
  PlusIcon,
  ViewGridIcon,
} from "@heroicons/react/outline";
import { Link } from "react-router-dom";

import {
  ActivityFilter,
  ActivityMobileList,
  activityColumns,
  useAddActivity,
} from ".";
import {
  FilterMenu,
  MobileTitle,
  RecentsBar,
  ReportMenu,
  SearchInput,
  SkeletonList,
  TableActionButton,
  TableList,
} from "~/components";
import { useDeleteConfirmation, useQuery } from "~/datastore";
import { useDrawer, useRoutes, useViewIndicator } from "~/hooks";
import { Activity } from "~/models";
import reports from "~/reports/activity";

export const ActivityIndex = () => {
  const { routes } = useRoutes();
  const { openDrawer, closeDrawer } = useDrawer();
  const { data: activitys, loading } = useQuery<Activity>({ model: Activity });
  const addActivity = useAddActivity();
  const deleteConfirmation = useDeleteConfirmation();
  const { ViewIndicator, getPanelProps } = useViewIndicator([
    { label: "List View", icon: <MenuAlt2Icon className="w-5" /> },
    { label: "Dashboard", icon: <ViewGridIcon className="w-5" /> },
  ]);
  const count = activitys.length;

  if (loading) return <SkeletonList />;
  return (
    <div className="">
      <div className="mb-4 items-center justify-between space-y-4 sm:flex sm:space-y-0">
        <div className="hidden items-center space-x-3 sm:flex">
          <Button leftIcon={<PlusIcon className="w-4" />} onClick={addActivity}>
            {t`addActivity`}
          </Button>
          <Button
            to={routes.activityImport.url()}
            as={Link}
            leftIcon={<DownloadIcon className="w-4" />}
          >
            {t`importActivity`}
          </Button>
          <p>
            {t`found`}: {count}
          </p>
        </div>
        <div className="flex space-x-3">
          <SearchInput
            autoFocus
            name="ActivitySearch"
            placeholder={t`search...`}
            className="bg-card"
          />

          <FilterMenu
            modelName={Activity.name}
            onAdvancedClick={() =>
              openDrawer({
                title: "Filter",
                size: "md",
                body: <ActivityFilter closeModal={closeDrawer} />,
              })
            }
          />

          <ReportMenu reports={reports} />
        </div>
      </div>

      <div className="flex items-start space-x-3 mb-4">
        <ViewIndicator />
        <RecentsBar model={Activity} />
      </div>

      <div className="card">
        <div {...getPanelProps("List View")}>
          <TableList
            data={activitys}
            globalSearchKey="activitySearch"
            columns={activityColumns}
            rowTo={routes.activityDetail.path}
            RenderMobile={ActivityMobileList}
          >
            <TableActionButton
              className="btn-danger"
              onClick={(selected) => deleteConfirmation({ selected })}
            >
              {(selectedCount) =>
                `${t("delete")} ${selectedCount} ${t("selected")}?`
              }
            </TableActionButton>
          </TableList>
        </div>
      </div>

      <MobileTitle
        subtitle={`${activitys.length} Activitys`}
        leftButtonText={routes.home.label}
        leftButtonAction={() => routes.home.go()}
        rightButton={
          <Button className="btn-title" onClick={addActivity}>
            {t`addActivity`}
          </Button>
        }
      />
    </div>
  );
};

export default ActivityIndex;
