import { API } from "aws-amplify";

import { GraphQLResult } from "@aws-amplify/api";
import { Agenda } from "~/API";
import { getCurrentEvent } from "~/utils";

export const listAgenda = (activityID?: string) => ({
  key: ["activity.agenda", activityID],
  fetch: async (limit?: number, nextToken?: string) => {
    const event = getCurrentEvent();
    const query = (await API.graphql({
      query: agendaByGroup,
      variables: {
        group: event?.group,
        filter: { activityID: { eq: activityID } },
        limit,
        nextToken,
      },
    })) as GraphQLResult<any>;

    if (!query.data) throw new Error("Could not retrieve data");

    return query.data.agendaByGroup as { items: Omit<Agenda, "activity">[] };
  },
});

const agendaByGroup = /* GraphQL */ `
  query AgendaByGroup(
    $group: String!
    $sortDirection: ModelSortDirection
    $filter: ModelAgendaFilterInput
    $limit: Int
    $nextToken: String
  ) {
    agendaByGroup(
      group: $group
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        guestID
        activityID
        createdAt
        updatedAt
        _version
        _deleted
        _lastChangedAt
        owner
        guest {
          id
          group
          nameFirst
          _nameFirst
          nameLast
          _nameLast
          company
          _company
          position
          _position
          dateOfBirth
          _keyPhoto
          createdAt
          updatedAt
          _version
          _deleted
          _lastChangedAt
          owner
        }
      }
      nextToken
      startedAt
    }
  }
`;
