import { DataStore, ProducerModelPredicate } from "@aws-amplify/datastore";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { Activity } from "~/models";

export const useActivity = (useId?: string) => {
  const { id } = useParams<{ id: string }>();
  const queryId = useId ?? id;
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<Activity>();

  useEffect(() => {
    if (!queryId) {
      return;
    }
    const sub = DataStore.observeQuery(Activity, (c) =>
      c.id("eq", queryId)
    ).subscribe((snapshot) => {
      setData(snapshot.items[0]);
      setLoading(false);
    });

    return () => sub.unsubscribe();
  }, [queryId]);

  return { loading, data };
};
