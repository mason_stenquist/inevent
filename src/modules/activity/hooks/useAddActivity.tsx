import { DataStore } from "aws-amplify";

import { ActivityFields, ActivityForm } from "..";
import { useDrawer, useRoutes } from "~/hooks";
import { Activity } from "~/models";
import { newModel } from "~/utils";

export const useAddActivity = () => {
  const { routes } = useRoutes();
  const { openDrawer, closeDrawer } = useDrawer();

  const saveActivity = async (values: ActivityFields) => {
    const activity = await DataStore.save(newModel(Activity, values));
    closeDrawer();
    routes.activityDetail.go({ id: activity.id });
  };

  const addActivity = () => {
    openDrawer({
      title: t`addActivity`,
      body: (
        <ActivityForm
          initValues={{}}
          submitText={t`createActivity`}
          onSubmit={saveActivity }
        />
      ),
    });
  };

  return addActivity;
};
