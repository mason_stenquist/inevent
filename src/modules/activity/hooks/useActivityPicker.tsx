import { Predicates, SortDirection } from "aws-amplify";

import { DefaultPickerRow, Picker } from "~/components";
import { useQuery } from "~/datastore";
import { ModalState, useFuseOptions, useModal } from "~/hooks";
import { Activity } from "~/models";

const fuseOptions: useFuseOptions<Activity> = {
  keys: ["name", "description", "date"],
};

export const useActivityPicker = (
  props: Omit<ModalState, "isOpen" | "body"> & { multiple?: boolean }
) => {
  const { multiple, ...modalProps } = props;
  const { showModal, closeModal } = useModal();
  const { data, loading } = useQuery({
    model: Activity,
    query: Predicates.ALL,
    sort: (s) => s.name(SortDirection.ASCENDING),
  });

  const showPicker = async () =>
    showModal({
      ...modalProps,
      body: (
        <Picker
          closeModal={() => closeModal()}
          multiple={multiple}
          data={data}
          loading={loading}
          fuseOptions={fuseOptions}
        >
          {(rowProps) => (
            <DefaultPickerRow {...rowProps}>
              <div>
                <div>{rowProps.item.name}</div>
                <div className="text-sm text-muted-text">
                  {rowProps.item.date} at {rowProps.item.start}
                </div>
              </div>
            </DefaultPickerRow>
          )}
        </Picker>
      ),
    });

  return { closePicker: closeModal, showPicker };
};
