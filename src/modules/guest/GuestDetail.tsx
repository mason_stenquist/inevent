import {
  Button,
  Hide,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import {
  BanIcon,
  CameraIcon,
  ChatIcon,
  DocumentReportIcon,
  PlusIcon,
  TrashIcon,
  UsersIcon,
} from "@heroicons/react/outline";
import { DataStore } from "aws-amplify";

import { useAddGuest } from ".";
import { useGuestPicker } from "./hooks/useGuestPicker";
import {
  ActionBar,
  Date,
  MobileTitle,
  S3Image,
  SearchForm,
  SearchInput,
  Sections,
  SkeletonList,
} from "~/components";
import { useDeleteConfirmation, useRecord } from "~/datastore";
import { useRoutes, useTrackRecent } from "~/hooks";
import { Guest } from "~/models";

export const GuestDetail = () => {
  const { routes } = useRoutes();
  const { data: guest, loading } = useRecord(Guest);
  useTrackRecent(Guest, guest, loading);
  const deleteConfirmation = useDeleteConfirmation();
  const addGuest = useAddGuest();
  const { showPicker } = useGuestPicker({
    title: "Select Guests",
    multiple: true,
    size: "2xl",
  });

  const test = async () => {
    const picked = await showPicker();
    console.log({ picked });
  };

  const handlePhotoChange = (key: string) => {
    console.log(key);
    if (!guest) return;
    DataStore.save(
      Guest.copyOf(guest, (updated) => {
        updated._keyPhoto = key;
      })
    );
  };

  if (loading || !guest) return <SkeletonList />;

  return (
    <div className="">
      <div className="mb-4 items-center justify-between space-y-4 sm:flex sm:space-y-0">
        <ActionBar>
          <Hide below="sm">
            <Button leftIcon={<PlusIcon className="w-4" />} onClick={addGuest}>
              {t`addGuest`}
            </Button>
          </Hide>
          <Button
            onClick={() => console.log(routes.guestDetail.go({ id: "123" }))}
            leftIcon={<ChatIcon className="w-4" />}
          >
            {t`text`}
          </Button>
          <Button
            leftIcon={<UsersIcon className="w-4" />}
          >{t`consolidate`}</Button>
          <Button
            className="hover:bg-warning"
            leftIcon={<BanIcon className="w-4" />}
          >
            {t`voidFromEvent`}
          </Button>
          <Button
            onClick={() =>
              deleteConfirmation({
                selected: guest,
                body: `${t("areYouSureYouWantToDelete")} ${guest.nameFirst} ${
                  guest.nameLast
                }?`,
                onDelete: () => routes.guestIndex.go(),
              })
            }
            className="hover:bg-danger hover:text-white"
            leftIcon={<TrashIcon className="w-4" />}
          >
            {t`deleteGuest`}
          </Button>
        </ActionBar>

        <div className="flex space-x-3 pl-0 md:pl-4">
          <SearchForm action={routes.guestIndex.url()} submitOnEnter>
            <SearchInput
              name="guestSearch"
              placeholder={t`search...`}
              className="bg-card"
            />
          </SearchForm>
          <Menu>
            <MenuButton as={Button} colorScheme="gray">
              <DocumentReportIcon className="h-6 w-6" />
            </MenuButton>
            <MenuList>
              <MenuGroup title="PDF/Print Reports">
                <MenuItem>Guest List Master</MenuItem>
                <MenuItem>Guest Agenda </MenuItem>
                <MenuItem>Simple Guest List </MenuItem>
                <MenuItem>Passport Information </MenuItem>
                <MenuItem>Guest Depature Summary </MenuItem>
                <MenuItem>Guest Depature Notice </MenuItem>
                <MenuItem>Guest Amenities </MenuItem>
                <MenuItem>Guest Diet Needs </MenuItem>
              </MenuGroup>
              <MenuDivider />
              <MenuGroup title="Excel">
                <MenuItem>Guest List</MenuItem>
              </MenuGroup>
            </MenuList>
          </Menu>
        </div>
      </div>

      <div className="space-y-4 lg:flex lg:space-x-4 lg:space-y-0">
        <div className="card lg:w-1/4">
          <div className="space-y-3">
            <div className="flex">
              <div className="w-full space-y-3">
                <div>
                  <p className="text-xs text-muted-text">{t`name`}</p>
                  <p>
                    {guest.nameFirst} {guest.nameLast}
                  </p>
                </div>
                <div>
                  <p className="text-xs text-muted-text">{t`company`}</p>
                  <p>{guest.company}</p>
                </div>
              </div>
              <div>
                <div className="w-20 h-20 overflow-hidden rounded-lg">
                  <S3Image
                    className="object-cover w-20 h-20"
                    onKeyChange={handlePhotoChange}
                    src={guest._keyPhoto}
                    uploadPath={`${Guest.name}/${guest.id}`}
                  >
                    <button className="w-full h-full text-muted-text hover:text-accent bg-page  flex justify-center items-center">
                      <div className="">
                        <CameraIcon className="w-6 mx-auto block" />
                        <span className="block mt-1 text-xs text-center w-full">
                          {t`upload`}
                        </span>
                      </div>
                    </button>
                  </S3Image>
                </div>
              </div>
            </div>
            <div>
              <p className="text-xs text-muted-text">{t`position`}</p>
              <p>{guest.position ?? "NA"}</p>
            </div>
            <div>
              <p className="text-xs text-muted-text">{t`dateOfBirth`}</p>
              <p>
                <Date>{guest.dateOfBirth}</Date>
              </p>
            </div>
            <Button onClick={test}>{t`addPeopleToParty`}</Button>
          </div>
        </div>
        <div className="lg:w-3/4">
          <Sections
            sections={[
              {
                label: t`agenda`,
                element: <div>Agend Element</div>,
              },
              {
                label: t`registration`,
                element: <div>Registration Element</div>,
              },
              {
                label: t`amenities`,
                element: <div>Amenities Element</div>,
              },
              {
                label: t`notes`,
                element: <div>Notes Element</div>,
              },
            ]}
          />
        </div>
      </div>
      <MobileTitle
        subtitle={`${guest.nameFirst} ${guest.nameLast}`}
        leftButtonText={routes.guestIndex.label}
        leftButtonAction={() => routes.guestIndex.go()}
        rightButton={
          <Button className="btn-title" onClick={addGuest}>
            Add Guest
          </Button>
        }
      />
    </div>
  );
};

export default GuestDetail;
