import { Button } from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { useState } from "react";

import { ModelInit } from "@aws-amplify/datastore";
import * as Yup from "yup";
import { Input } from "~/components";
import { useDrawer } from "~/hooks";
import { Guest, GuestMetaData } from "~/models";

export type GuestFields = ModelInit<Guest, GuestMetaData>;
type ParitalGuestFields = Partial<GuestFields>;

interface Props {
  initValues: ParitalGuestFields;
  submitText: string;
  onSubmit: (values: GuestFields) => Promise<void>;
}

const emptyGuest: GuestFields = {
  group: "",
  nameFirst: "",
  nameLast: "",
  company: "",
  position: "",
  dateOfBirth: "",
  agenda: [],
};

const schema = Yup.object().shape({
  nameFirst: Yup.string().required().label("First Name"),
  nameLast: Yup.string().required().label("Last Name"),
  company: Yup.string().label("Company"),
  position: Yup.string().label("Position"),
  dateOfBirth: Yup.date().required().label("Date of Birth"),
});

export const GuestForm: React.FC<Props> = ({
  initValues,
  onSubmit,
  submitText,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const { closeDrawer } = useDrawer();
  const initialValues = { ...emptyGuest, ...initValues };

  const handleSubmit = async (values: GuestFields) => {
    setIsLoading(true);
    try {
      await onSubmit(values);
      closeDrawer();
    } catch (e) {
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={schema}
      onSubmit={handleSubmit}
    >
      <Form>
        <Input label={t`nameFirst`} type="text" name="nameFirst" />
        <Input label={t`nameLast`} type="text" name="nameLast" />
        <Input label={t`company`} type="text" name="company" />
        <Input label={t`position`} type="text" name="position" />
        <Input label={t`dateOfBirth`} name="dateOfBirth" type="date" />
        <div className="space-x-3 pt-4">
          <Button type="submit" className="btn-primary" isLoading={isLoading}>
            {submitText}
          </Button>
          <Button onClick={closeDrawer} isLoading={isLoading}>
            {t`cancel`}
          </Button>
        </div>
      </Form>
    </Formik>
  );
};
