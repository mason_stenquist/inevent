import { Column } from "~/components";
import { Guest } from "~/models";

export const guestColumns: Column<Guest>[] = [
  {
    key: "nameFirst",
  },
  {
    key: "nameLast",
  },
  {
    key: "company",
  },
  {
    key: "position",
  },
];
