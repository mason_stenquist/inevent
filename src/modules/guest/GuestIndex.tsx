import { Button } from "@chakra-ui/react";
import {
  DownloadIcon,
  MenuAlt2Icon,
  PlusIcon,
  ViewGridIcon,
} from "@heroicons/react/outline";
import { Link } from "react-router-dom";

import {
  GuestFilter,
  GuestMobileList,
  guestColumns,
  useAddGuest,
} from "./index";
import {
  FilterMenu,
  MobileTitle,
  RecentsBar,
  ReportMenu,
  SearchInput,
  SkeletonList,
  TableActionButton,
  TableList,
} from "~/components";
import { useDeleteConfirmation, useQuery } from "~/datastore";
import { useDrawer, useRoutes, useViewIndicator } from "~/hooks";
import { Guest } from "~/models";
import reports from "~/reports/guest";

export const GuestIndex = () => {
  const { routes } = useRoutes();
  const { openDrawer, closeDrawer } = useDrawer();
  const { data: guests, loading } = useQuery<Guest>({ model: Guest });
  const addGuest = useAddGuest();
  const deleteConfirmation = useDeleteConfirmation();
  const { ViewIndicator, getPanelProps } = useViewIndicator([
    { label: "List View", icon: <MenuAlt2Icon className="w-5" /> },
    { label: "Dashboard", icon: <ViewGridIcon className="w-5" /> },
  ]);
  const count = guests.length;

  if (loading) return <SkeletonList />;
  return (
    <div className="">
      <div className="mb-4 items-center justify-between space-y-4 sm:flex sm:space-y-0">
        <div className="hidden items-center space-x-3 sm:flex">
          <Button leftIcon={<PlusIcon className="w-4" />} onClick={addGuest}>
            {t`addGuest`}
          </Button>
          <Button
            to={routes.guestImport.url()}
            as={Link}
            leftIcon={<DownloadIcon className="w-4" />}
          >
            {t`importGuest`}
          </Button>
          <p>
            {t`found`}: {count}
          </p>
        </div>
        <div className="flex space-x-3">
          <SearchInput
            autoFocus
            name="GuestSearch"
            placeholder={t`search...`}
            className="bg-card"
          />

          <FilterMenu
            modelName={Guest.name}
            onAdvancedClick={() =>
              openDrawer({
                title: "Filter",
                size: "md",
                body: <GuestFilter closeModal={closeDrawer} />,
              })
            }
          />

          <ReportMenu reports={reports} />
        </div>
      </div>

      <div className="flex items-start space-x-3 mb-4">
        <ViewIndicator />
        <RecentsBar model={Guest} />
      </div>

      <div className="card">
        <div {...getPanelProps("List View")}>
          <TableList
            data={guests}
            globalSearchKey="guestSearch"
            columns={guestColumns}
            rowTo={routes.guestDetail.path}
            RenderMobile={GuestMobileList}
          >
            <TableActionButton
              className="btn-danger"
              onClick={(selected) => deleteConfirmation({ selected })}
            >
              {(selectedCount) =>
                `${t("delete")} ${selectedCount} ${t("selected")}?`
              }
            </TableActionButton>
          </TableList>
        </div>
      </div>

      <MobileTitle
        subtitle={`${guests.length} Guests`}
        leftButtonText={routes.home.label}
        leftButtonAction={() => routes.home.go()}
        rightButton={
          <Button className="btn-title" onClick={addGuest}>
            {t`addGuest`}
          </Button>
        }
      />
    </div>
  );
};

export default GuestIndex;
