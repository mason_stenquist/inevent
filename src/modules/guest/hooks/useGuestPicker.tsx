import { Predicates, SortDirection } from "aws-amplify";

import { DefaultPickerRow, Picker } from "~/components";
import { useQuery } from "~/datastore";
import { ModalState, useFuseOptions, useModal } from "~/hooks";
import { Guest } from "~/models";

const fuseOptions: useFuseOptions<Guest> = {
  keys: ["nameFirst", "company", "position"],
  getFn: (item: Guest, [path]: any) => {
    if (path === "nameFirst") return `${item?.nameFirst} ${item?.nameLast}`;
    const key = path as keyof Omit<Guest, "agenda">;
    return item?.[key] ?? "";
  },
};

export const useGuestPicker = (
  props: Omit<ModalState, "isOpen" | "body"> & { multiple?: boolean }
) => {
  const { multiple, ...modalProps } = props;
  const { showModal, closeModal } = useModal();
  const { data, loading } = useQuery({
    model: Guest,
    query: Predicates.ALL,
    sort: (s) =>
      s.nameFirst(SortDirection.ASCENDING).nameLast(SortDirection.ASCENDING),
  });

  const showPicker = async () =>
    showModal({
      ...modalProps,
      body: (
        <Picker
          closeModal={() => closeModal()}
          multiple={multiple}
          data={data}
          loading={loading}
          fuseOptions={fuseOptions}
        >
          {(rowProps) => (
            <DefaultPickerRow {...rowProps}>
              <div>
                <div>
                  {rowProps.item.nameFirst} {rowProps.item.nameLast}
                </div>
                <div className="text-sm text-muted-text">
                  {rowProps.item.position} at {rowProps.item.company}
                </div>
              </div>
            </DefaultPickerRow>
          )}
        </Picker>
      ),
    });

  return { closePicker: closeModal, showPicker };
};
