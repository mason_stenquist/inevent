import { DataStore } from "aws-amplify";

import { GuestFields, GuestForm } from "..";
import { useDrawer, useRoutes } from "~/hooks";
import { Guest } from "~/models";
import { newModel } from "~/utils";

export const useAddGuest = () => {
  const { routes } = useRoutes();
  const { openDrawer, closeDrawer } = useDrawer();

  const saveGuest = async (values: GuestFields) => {
    const guest = await DataStore.save(newModel(Guest, values));
    closeDrawer();
    routes.guestDetail.go({ id: guest.id });
  };

  const addGuest = () => {
    openDrawer({
      title: t`addGuest`,
      body: (
        <GuestForm
          initValues={{}}
          submitText={t`createGuest`}
          onSubmit={saveGuest}
        />
      ),
    });
  };

  return addGuest;
};
