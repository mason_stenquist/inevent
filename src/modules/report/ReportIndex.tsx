import { FormControl, FormLabel } from "@chakra-ui/react";
import { useMemo } from "react";
import { NavLink, Outlet, useOutlet, useSearchParams } from "react-router-dom";

import ReportPreview from "./ReportPreview";
import { MobileTitle, SearchSelect } from "~/components";
import { useRoutes } from "~/hooks";
import { reports } from "~/reports";

const REPORT_KEY = "report-category";

export default function ReportIndex() {
  const { routes } = useRoutes();
  const outlet = useOutlet();
  const [searchParams] = useSearchParams();
  const category = searchParams.get(REPORT_KEY) ?? "";
  const categories = reports.map(({ category }) => category);
  const uniqueCategories = [...new Set(categories)];

  const filteredReports = useMemo(() => {
    if (!category) return reports;
    return reports.filter((report) => report.category === category);
  }, [reports, category]);

  return (
    <>
      <div className="flex flex-col lg:flex-row gap-8">
        <div className="card w-full">
          <h1 className="mb-4">{t`reports`}</h1>
          <FormControl className="pb-2">
            <FormLabel>{t`reportType`}</FormLabel>
            <SearchSelect name={REPORT_KEY}>
              <option>TEST</option>
              {uniqueCategories.map((category) => (
                <option key={category}>{category}</option>
              ))}
            </SearchSelect>
          </FormControl>

          {filteredReports.map((report) => (
            <NavLink
              key={report.path}
              to={`${report.path}?${REPORT_KEY}=${category}`}
              className={({ isActive }) =>
                `block rounded px-4  py-2 hover:bg-table-row-hover ${
                  isActive ? " bg-table-row-hover " : ""
                }`
              }
            >
              {t(report.name)}
            </NavLink>
          ))}
        </div>
        {!!outlet && (
          <>
            <ReportPreview />
            <div className="w-1/2">
              <div className="card ">
                <p className="mb-4">Report Settings</p>
                <Outlet />
              </div>
            </div>
          </>
        )}
      </div>

      <MobileTitle
        leftButtonText="Home"
        leftButtonAction={() => routes.home.go()}
      />
    </>
  );
}
