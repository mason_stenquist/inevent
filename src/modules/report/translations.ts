export default {
  en: {
    reports: "Reports",
    reportType: "Report Type",
    reportPreview: "Report Preview",
    printExport: "Print/Export",
  },
  es: {
    reports: "Informes",
    reportType: "Tipo de informe",
    reportPreview: "Vista previa del informe",
    printExport: "Imprimir/Exportar",
  },
};
