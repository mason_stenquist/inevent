import { Button } from "@chakra-ui/react";
import { useRef } from "react";
import { useLocation } from "react-router-dom";

export default function ReportPreview() {
  const { pathname } = useLocation();
  const pathArray = pathname.split("/");
  const name = pathArray[pathArray.length - 1];
  const iframe = useRef<HTMLIFrameElement>(null);
  const url = `/r/${name}${window.location.search}`;
  const print = () => {
    if (iframe.current && iframe.current.contentWindow) {
      if (iframe.current.contentWindow.$downloadCSV) {
        iframe.current.contentWindow.$downloadCSV();
      } else {
        iframe.current.contentWindow.print();
      }
    }
  };
  return (
    <>
      <div className="card">
        <div className="flex justify-between">
          <p>{t`reportPreview`}</p>
          <Button onClick={print}>{t`printExport`}</Button>
        </div>
        <div></div>
        <iframe
          ref={iframe}
          id="preview"
          src={url}
          className="mt-4 h-[65vh] w-full lg:w-[595px] border bg-white"
        />
      </div>
    </>
  );
}
