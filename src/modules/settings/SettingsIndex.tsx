import {
  Link,
  NavLink,
  NavLinkProps,
  Outlet,
  useLocation,
  useNavigate,
} from "react-router-dom";

import { useRoutes } from "~/hooks";

const SettingsNav = ({ children, ...restProps }: NavLinkProps) => (
  <NavLink {...restProps} className="settings-nav">
    {children}
  </NavLink>
);

export const SettingsIndex = () => {
  const { routes } = useRoutes();
  const { pathname } = useLocation();

  //Don't allow root index settings access
  if (routes.settingsIndex.path === pathname) {
    routes.settingsMyAccount.go();
  }

  return (
    <div className="flex space-x-8">
      <div className="flex flex-col space-y-2 card min-w-[200px]">
        <SettingsNav to={routes.settingsMyAccount.url()}>
          My Account
        </SettingsNav>
        <SettingsNav to={routes.settingsBilling.url()}>Billing</SettingsNav>
        <SettingsNav to={routes.settingsEvents.url()}>
          Event Management
        </SettingsNav>
        <SettingsNav to={routes.settingsUsers.url()}>
          User Management
        </SettingsNav>
      </div>
      <div className="w-full card">
        <Outlet />
      </div>
    </div>
  );
};

export default SettingsIndex;
