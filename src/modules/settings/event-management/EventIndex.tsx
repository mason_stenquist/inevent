import { Button } from "@chakra-ui/react";

import { eventColumns } from "./eventConfig";
import { TableList } from "~/components";
import { useQuery } from "~/datastore";
import { useRoutes } from "~/hooks";
import { Event } from "~/models";

export const EventIndex = () => {
  const { routes } = useRoutes();
  const { data, loading } = useQuery<Event>({ model: Event });
  return (
    <div className="">
      <div className="flex justify-between">
        <h1>Event Management</h1>
        <div>
          <Button onClick={() => routes.settingsEventsNew.go()}>
            Add Event
          </Button>
        </div>
      </div>
      <div>
        <TableList
          columns={eventColumns}
          data={data}
          globalSearchKey="eventSearch"
          rowTo={routes.settingsEventsDetail.path}
        ></TableList>
      </div>
    </div>
  );
};

export default EventIndex;
