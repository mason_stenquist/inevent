import { Predicates, SortDirection } from "aws-amplify";

import { DefaultPickerRow, Picker } from "~/components";
import { useQuery } from "~/datastore";
import { ModalState, useFuseOptions, useModal } from "~/hooks";
import { Event } from "~/models";
import { useEvent } from "~/state/eventAtom";
import { changeSync, configureDataStore } from "~/utils";

const fuseOptions: useFuseOptions<Event> = {
  keys: ["eventName"],
};

export const useEventPicker = (
  props: Omit<ModalState, "isOpen" | "body"> & {
    multiple?: boolean;
    initSelected?: Event[];
  }
) => {
  const { multiple, initSelected, ...modalProps } = props;
  const { showModal, closeModal } = useModal();
  const { data, loading } = useQuery({
    model: Event,
    query: Predicates.ALL,
    sort: (s) => s.eventName(SortDirection.ASCENDING),
  });

  const showPicker = async () =>
    showModal({
      ...modalProps,
      body: (
        <Picker
          closeModal={() => closeModal()}
          multiple={multiple}
          data={data}
          loading={loading}
          fuseOptions={fuseOptions}
          initSelected={initSelected}
        >
          {(rowProps) => (
            <DefaultPickerRow {...rowProps}>
              <div>
                <div>{rowProps.item.eventName}</div>
                <div className="text-sm text-muted-text">
                  {rowProps.item.startDate} to {rowProps.item.endDate}
                </div>
              </div>
            </DefaultPickerRow>
          )}
        </Picker>
      ),
    });

  return { closePicker: closeModal, showPicker };
};

export const useChangeEvent = () => {
  const [event, setEvent] = useEvent();
  const { showPicker } = useEventPicker({
    title: "Select Event",
    multiple: false,
    initSelected: event ? [event] : [],
  });
  const changeEvent = async () => {
    const picker = await showPicker();
    if (picker.cancel) return;
    setEvent(picker.results);
    configureDataStore();
    changeSync();
    window.location.reload();
  };
  return changeEvent;
};
