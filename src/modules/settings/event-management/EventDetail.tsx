import { Button } from "@chakra-ui/react";
import { useParams } from "react-router-dom";

import { useRecord } from "~/datastore";
import { useRoutes } from "~/hooks";
import { Event } from "~/models";

export const EventIndex = () => {
  const { routes } = useRoutes();
  const { id } = useParams();
  const { data, loading } = useRecord(Event);

  return (
    <div className="">
      <div className="flex justify-between">
        <h1>Event Management</h1>
        <div>
          <Button onClick={() => routes.settingsEventsNew.go()}>
            Add Event
          </Button>
        </div>
      </div>
      <div>{JSON.stringify(data)}</div>
    </div>
  );
};

export default EventIndex;
