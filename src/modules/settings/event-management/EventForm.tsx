import { Button } from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { ModelInit } from "@aws-amplify/datastore";
import * as Yup from "yup";
import { Input } from "~/components";
import { Event, EventMetaData } from "~/models";

export type EventFields = ModelInit<Event, EventMetaData>;
type ParitalEventFields = Partial<EventFields>;

interface Props {
  initValues: ParitalEventFields;
  submitText?: string;
  onSubmit: (values: EventFields) => void;
}

const emptyEvent: EventFields = {
  group: "",
  eventName: "",
  startDate: "",
  endDate: "",
};

const schema = Yup.object().shape({
  eventName: Yup.string().required().label("Event Name"),
  startDate: Yup.date().required().label("Start Date"),
  endDate: Yup.date().required().label("End Date"),
});

export const EventForm: React.FC<Props> = ({
  initValues,
  onSubmit,
  submitText = "Submit",
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const initialValues = { ...emptyEvent, ...initValues };

  const handleSubmit = async (values: EventFields) => {
    setIsLoading(true);
    try {
      await onSubmit(values);
    } catch (e) {
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={schema}
      onSubmit={handleSubmit}
    >
      <Form>
        <Input label={t`eventName`} type="text" name="eventName" />
        <Input label={t`startDate`} name="startDate" type="date" />
        <Input label={t`endDate`} name="endDate" type="date" />
        <div className="space-x-3 pt-4">
          <Button type="submit" className="btn-primary" isLoading={isLoading}>
            {submitText}
          </Button>
          <Button onClick={() => navigate(-1)} isLoading={isLoading}>
            {t`cancel`}
          </Button>
        </div>
      </Form>
    </Formik>
  );
};
