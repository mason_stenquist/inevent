import { DataStore } from "aws-amplify";

import { EventFields, EventForm } from "./EventForm";
import { useRoutes } from "~/hooks";
import { Event } from "~/models";
import { newModel, slugify } from "~/utils";

export const EventNew = () => {
  const { routes } = useRoutes();

  const addEvent = async (values: EventFields) => {
    const randNumber: number = Math.floor(
      parseFloat(Math.random().toFixed(2)) * 100
    );
    const { eventName } = values;
    const groupId = slugify(eventName) + `-${randNumber}`;
    console.log(groupId);
    const event = await DataStore.save(
      newModel(Event, { ...values, group: groupId })
    );
    routes.settingsEvents.go();
  };

  return (
    <div className="">
      <div className="flex justify-between">
        <h1>Add New Event</h1>
      </div>
      <div>
        <EventForm initValues={{}} onSubmit={addEvent} />
      </div>
    </div>
  );
};

export default EventNew;
