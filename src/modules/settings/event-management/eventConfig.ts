import { Column } from "~/components";
import { Event } from "~/models";

export const eventColumns: Column<Event>[] = [
  {
    key: "eventName",
  },
  {
    key: "startDate",
  },
  {
    key: "endDate",
  },
];
