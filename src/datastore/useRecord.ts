import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import {
  DataStore,
  PersistentModel,
  PersistentModelConstructor,
} from "@aws-amplify/datastore";

export const useRecord = <Model extends PersistentModel>(
  model: PersistentModelConstructor<Model>,
  useId?: string
) => {
  const { id } = useParams<{ id: string }>();
  const queryId = useId ?? id;
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<Model>();

  useEffect(() => {
    if (!queryId) {
      return;
    }
    const sub = DataStore.observeQuery(model, (c) =>
      // @ts-ignore
      c.id("eq", queryId)
    ).subscribe((snapshot) => {
      setData(snapshot.items[0]);
      setLoading(false);
    });

    return () => sub.unsubscribe();
  }, [queryId]);

  return { loading, data };
};
