import { useEffect, useMemo, useState } from "react";
import { useSearchParams } from "react-router-dom";

import { useQuerySort } from "./useQuerySort";
import {
  DataStore,
  PersistentModel,
  PersistentModelConstructor,
  ProducerModelPredicate,
  ProducerSortPredicate,
} from "@aws-amplify/datastore";
import { PredicateAll } from "@aws-amplify/datastore/lib-esm/predicates";
import { useQueryBuilder } from "~/datastore/useQueryBuilder";
import { useEvent } from "~/state/eventAtom";
import { getSchema } from "~/utils";

const predicateMapping: Record<string, string> = {
  ID: "eq",
  String: "contains",
  AWSDate: "eq",
  AWSTime: "eq",
  AWSDateTime: "eq",
  AWSDateTimestamp: "eq",
  Int: "eq",
  Float: "eq",
  Boolean: "eq",
  List: "contains",
  AWSEmail: "contains",
  AWSJSON: "contains",
  AWSURL: "contains",
  AWSPhone: "contains",
  AWSIPAddress: "contains",
};

interface Props<Model extends PersistentModel> {
  model: PersistentModelConstructor<Model>;
  query?: ProducerModelPredicate<Model> | typeof PredicateAll;
  sort?: ProducerSortPredicate<Model>;
}

export const useQuery = <Model extends PersistentModel>({
  model,
  query,
  sort,
}: Props<Model>) => {
  const [event] = useEvent();
  const [searchParams] = useSearchParams();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<Model[]>([]);

  const schema = getSchema(model as any);
  const getPredicate = (name: string, type: string) => {
    const urlPredicate = searchParams.get(`${name}Predicate`);
    return urlPredicate ?? predicateMapping?.[type] ?? "contains";
  };

  const allowedFields = Object.values(schema.fields)
    .filter(({ name }) => !name.startsWith("_"))
    .map(({ name, type }) => ({
      field: name,
      queryField: schema.fields[`_${name}`]?.name ?? name,
      predicate: getPredicate(name, type as string),
    }));

  const defaultQuery = useQueryBuilder(allowedFields, `${model.name}Search`);
  const defaultSort = useQuerySort<any>(allowedFields);

  const criteria = useMemo(() => {
    if (query) return query;
    return defaultQuery() as unknown as ProducerModelPredicate<Model>;
  }, [query, searchParams]);

  const s = useMemo(() => {
    if (sort) return sort;
    return defaultSort();
  }, [sort, searchParams]);

  useEffect(() => {
    console.log("Event changed, refresh query", event);
    const observe = DataStore.observeQuery(model as any, criteria as any, {
      sort: s as any,
    }).subscribe((snapshot) => {
      setData(snapshot.items as unknown as Model[]);
      setLoading(false);
    });

    return () => observe.unsubscribe();
  }, [searchParams, event]);

  return { loading, data };
};
