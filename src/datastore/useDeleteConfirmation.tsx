import { DataStore } from "aws-amplify";

import { PersistentModel } from "@aws-amplify/datastore";
import { useDialogue } from "~/hooks";

interface Props {
  selected: PersistentModel | PersistentModel[];
  title?: string;
  body?: string;
  onDelete?: () => void;
}

export const useDeleteConfirmation = () => {
  const { showDialogue } = useDialogue();

  const deleteConfirmation = async ({
    selected,
    title,
    body,
    onDelete,
  }: Props) => {
    const toDelete = Array.isArray(selected) ? selected : [selected];

    //Configure display text
    const displayTitle = title ? title : "Delete";
    let displayBody = body;
    if (!displayBody && toDelete.length > 1)
      displayBody = `${t("areYouSureYouWantToDeleteThese")} ${
        toDelete.length
      } ${t("items")}?`;
    if (!displayBody) displayBody = `${t("areYouSureYouWantToDeleteThisItem")}`;

    //Confirm deletion
    const shouldDelete = await showDialogue({
      title: displayTitle,
      body: displayBody,
      showCancel: true,
      buttons: [{ label: t`delete`, value: "delete" }],
    });
    if (shouldDelete === "cancel") return;

    //Delete all models
    const deletePromises = toDelete.map((model: PersistentModel) =>
      DataStore.delete(model)
    );
    await Promise.all(deletePromises);

    //Handle callback
    if (onDelete) onDelete();
  };

  return deleteConfirmation;
};
