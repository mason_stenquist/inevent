import {
  PersistentModel,
  PersistentModelConstructor,
  SortDirection,
  SortPredicate,
} from "@aws-amplify/datastore";
import { FilterOption } from "~/@types/FilterOption";
import { useSearchParamKey } from "~/hooks";

export const useQuerySort = <T extends PersistentModelConstructor<any>>(
  allowedFields: any[]
) => {
  const [sortField] = useSearchParamKey("sortField");
  const [sortDirection] = useSearchParamKey("sortDirection");

  const sort = () => {
    let sort;
    const selectedSortField = allowedFields.find(
      ({ field }) => field === sortField
    );
    sort = (s: SortPredicate<PersistentModel>) => {
      if (selectedSortField)
        s[selectedSortField.queryField](
          sortDirection === "ascending"
            ? SortDirection.ASCENDING
            : SortDirection.DESCENDING
        );
      return s;
    };

    return sort;
  };

  return sort;
};
