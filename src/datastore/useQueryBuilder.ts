import { useSearchParams } from "react-router-dom";

import { ModelPredicate, PersistentModel } from "@aws-amplify/datastore";
import { cleanObject, serializeData } from "~/utils";

export const useQueryBuilder = (
  allowedFields: any[],
  globalSearchKey: string,
  globalSearchValue?: string
) => {
  const [searchParams, setSearchParams] = useSearchParams();

  const globalSearch = globalSearchValue ?? searchParams.get(globalSearchKey);

  const clearFilters = () => {
    const clearedSearchParams = allowedFields.reduce(
      (acc, field) => ({
        ...acc,
        [field.field]: "",
        [`${field.field}Predicate`]: "",
      }),
      {}
    );
    const existingSearchParams = serializeData(searchParams);
    setSearchParams(
      cleanObject({ ...existingSearchParams, ...clearedSearchParams })
    );
  };

  const query = () => {
    let criteria;

    if (globalSearch) {
      criteria = (c: ModelPredicate<PersistentModel>) =>
        c.or((c) => {
          allowedFields
            .filter((field) => !Boolean(field?.noGlobalSearch))
            .forEach(({ queryField }) => {
              if (globalSearch)
                //@ts-ignore
                c[queryField]("contains", globalSearch.toLowerCase());
            });
          return c;
        });
      if (!globalSearchValue) clearFilters();
    } else {
      criteria = (c: ModelPredicate<PersistentModel>) => {
        allowedFields.forEach(({ field, queryField, predicate }) => {
          const filterValue = searchParams.get(field);
          if (filterValue)
            //@ts-ignore
            c[queryField](predicate, filterValue.toLowerCase());
        });
        return c;
      };
    }
    return criteria;
  };

  return query;
};
