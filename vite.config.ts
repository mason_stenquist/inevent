import postcss from "./postcss.config.js";
import replace from "@rollup/plugin-replace";
import react from "@vitejs/plugin-react";
import path from "path";
import { PluginOption, defineConfig } from "vite";
import legacyPlugin from "vite-plugin-legacy";
import { VitePWA, VitePWAOptions } from "vite-plugin-pwa";

const legacyOptions = {
  // The browsers that must be supported by your legacy bundle.
  // https://babeljs.io/docs/en/babel-preset-env#targets
  targets: ["> 0.5%", "last 2 versions", "Firefox ESR", "not dead"],
  // Define which polyfills your legacy bundle needs. They will be loaded
  // from the Polyfill.io server. See the "Polyfills" section for more info.
  polyfills: [
    // Empty by default
  ],
  // Toggles whether or not browserslist config sources are used.
  // https://babeljs.io/docs/en/babel-preset-env#ignorebrowserslistconfig
  ignoreBrowserslistConfig: false,
  // When true, core-js@3 modules are inlined based on usage.
  // When false, global namespace APIs (eg: Object.entries) are loaded
  // from the Polyfill.io server.
  corejs: false,
};

const pwaOptions: Partial<VitePWAOptions> = {
  mode: "production",
  base: "/",
  registerType: "autoUpdate",
  workbox: {
    sourcemap: true,
    maximumFileSizeToCacheInBytes: 4000000,
  },
  includeAssets: [
    "favicon.svg",
    "favicon.ico",
    "robots.txt",
    "apple-touch-icon.png",
  ],
  manifest: {
    name: "inEvent Manager",
    short_name: "inEvent Manager",
    description: "inEvent Manager for event destination management",
    start_url: `/event/?v=${new Date().toISOString()}`,
    theme_color: "#FBF9F6",
    icons: [
      {
        src: "pwa-192x192.png",
        sizes: "192x192",
        type: "image/png",
      },
      {
        src: "pwa-512x512.png",
        sizes: "512x512",
        type: "image/png",
      },
      {
        src: "pwa-512x512.png",
        sizes: "512x512",
        type: "image/png",
        purpose: "any maskable",
      },
    ],
  },
};

const process = { env: { SW: "false", CLAIMS: "false", RELOAD_SW: "true" } };

const replaceOptions = { __DATE__: new Date().toISOString() };
const claims = process.env.CLAIMS === "true";
const reload = process.env.RELOAD_SW === "true";

if (process.env.SW === "true") {
  pwaOptions.srcDir = "src";
  pwaOptions.filename = claims ? "claims-sw.ts" : "prompt-sw.ts";
  pwaOptions.strategies = "injectManifest";
}

if (claims) pwaOptions.registerType = "autoUpdate";

if (reload) {
  // @ts-ignore
  replaceOptions.__RELOAD_SW__ = "true";
}

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    sourcemap: false,
  },
  esbuild: {
    target: "es2020",
  },
  minify: true,
  resolve: {
    alias: {
      "~": path.resolve(__dirname, "./src"),
      "./runtimeConfig": "./runtimeConfig.browser",
    },
  },
  plugins: [
    react(),
    VitePWA(pwaOptions),
    replace(replaceOptions) as unknown as PluginOption,
    legacyPlugin(legacyOptions),
  ],
  css: {
    // @ts-ignore
    postcss,
  },
});
